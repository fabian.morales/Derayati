<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {
        
    use UserTrait, RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sis_usuario';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password', 'remember_token');
    
    protected $fillable = array('id', 'nombre', 'apellido', 'login', 'email', 'tipo', 'telefono', 'ciudad', 'pais_residencia', 'sexo', 'nacionalidad', 'experiencia', 'profesion');

    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->password;
    }

    /**
     * Get the e-mail address where password reminders are sent.
     *
     * @return string
     */
    public function getReminderEmail()
    {
        return $this->email;
    }
    
    public function pais(){
        return $this->belongsTo('Pais', 'pais_residencia', 'id');
    }
    
    public function idiomas(){
        return $this->hasMany('Idioma', 'id_profesor');
    }
    
    public function certificados(){
        return $this->hasMany('Certificado', 'id_profesor');
    }
    
    public function cursos(){
        return $this->hasMany('CursoProfesor', 'id_profesor');
    }
    
    public function inscripciones(){
        return $this->hasMany('CursoAlumno', 'id_alumno');
    }
    
    public function calificaciones(){
        return $this->hasMany('Calificacion', 'id_profesor');
    }
    
    public function obtenerCalificacion(){
        $ret = $this->calificaciones()->avg('calificacion');
        if (!$ret){
            $ret = 0;
        }
        return $ret;
    }
    
    public function obtenerIdiomasConcat(){
        $ret = array();
        foreach ($this->idiomas()->get() as $i){
            $ret[] = $i->idioma_en;
        }
        
        return implode(", ", $ret);
    }
}
