<?php

class Idioma extends Eloquent {    
    protected $table = 'sis_profesor_idioma';
    protected $fillable = array('id', 'id_profesor', 'idioma_en', 'idioma_ar');
}