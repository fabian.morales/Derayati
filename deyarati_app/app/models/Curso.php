<?php

class Curso extends Eloquent {    
    protected $table = 'sis_curso';
    
    public function tema(){
        return $this->belongsTo("Tema", "id_tema", "id");
    }
}