<?php

class CursoProfesor extends Eloquent {    
    protected $table = 'sis_profesor_curso';
    protected $fillable = array('id', 'id_curso', 'id_profesor', 'descripcion_corta', 'descripcion', 'nivel', 'ciudad', 'modalidad', 'idioma', 'h_lunes', 'h_martes', 'h_miercoles', 'h_jueves', 'h_viernes', 'h_sabado', 'h_domingo', 'costo', 'notas');
    
    public function profesor(){
        return $this->belongsTo('User', 'id_profesor');
    }
    
    public function curso(){
        return $this->belongsTo('Curso', 'id_curso');
    }
    
    public function ciudadObj(){
        return $this->belongsTo('Ciudad', 'ciudad');
    }
    
    public function inscripciones(){
        return $this->hasMany('CursoAlumno', 'id_curso');
    }
    
    public function obtenerCalificacion(){
        $ret = $this->inscripciones()->avg('calificacion');
        if (!$ret){
            $ret = 0;
        }
        return $ret;
    }
}