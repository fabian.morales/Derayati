<?php

class Estado extends Eloquent {    
    protected $table = 'sis_estado';
    
    public function ciudades(){
        return $this->hasMany('Ciudad', 'id_estado');
    }
}