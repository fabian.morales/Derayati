<?php

class Ciudad extends Eloquent {    
    protected $table = 'sis_ciudad';
    
    public function pais(){
        return $this->belongsTo('Pais', 'id_pais');
    }
}