<?php

class CursoAlumno extends Eloquent {    
    protected $table = 'sis_alumno_curso';
    protected $fillable = array('id', 'id_curso', 'id_profesor_curso', 'h_lunes', 'h_martes', 'h_miercoles', 'h_jueves', 'h_viernes', 'h_sabado', 'h_domingo', 'horas', 'notas');
    
    public function alumno(){
        return $this->belongsTo('User', 'id_alumno');
    }
    
    public function profesor(){
        return $this->belongsTo('User', 'id_profesor_curso');
    }
    
    public function curso(){
        return $this->belongsTo('CursoProfesor', 'id_curso');
    }
}