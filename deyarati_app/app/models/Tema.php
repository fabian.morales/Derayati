<?php

class Tema extends Eloquent {    
    protected $table = 'sis_tema';
    
    public function cursos(){
        return $this->hasMany('Curso', 'id_tema');
    }
}