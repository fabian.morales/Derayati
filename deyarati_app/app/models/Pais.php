<?php

class Pais extends Eloquent {    
    protected $table = 'sis_pais';
    
    /*public function estados(){
        return $this->hasMany('Estado', 'id_pais');
    }*/
    
    public function ciudades(){
        return $this->hasMany('Ciudad', 'id_pais');
    }
}