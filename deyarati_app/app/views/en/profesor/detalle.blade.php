@extends($lang.'.master')

@section('js_header')
@stop

@section('content')                    
<div class="container">    
    <div class="contact-info">
        <div class="row">            
            <div class="col-sm-6">
                <div class="tittle">
            	<h3> Teacher Information </h3>
      		<hr>
       	   </div>
                <ul class="row lista_espacio">
                    @if (!Auth::check())
                    <li class="col-md-12"><span class="azul">If you want see more information, please<a href="{{ url('/'.$lang.'/sesion/formLogin') }}"> register</a></span></li>
                    @else
                    <li class="col-md-5">
                        @if (is_file(public_path('avatares/'.$profesor->id.'.jpg')))
                        <img class='img-responsive' src='{{ asset('avatares/'.$profesor->id.'.jpg') }}' />
                        @else

                        @if ($profesor->sexo == 'M')
                        <img class='img-responsive' src='{{ asset('images/avatar_male.png') }}' />
                        @else
                        <img class='img-responsive' src='{{ asset('images/avatar_female.png') }}' />
                        @endif

                        @endif
                    </li>
                    @endif
                    
                    <li class="col-md-12">
                        <strong>{{ $profesor->nombre }} {{ $profesor->apellido }}</strong>
                        @include("rate", array("valor" => $profesor->obtenerCalificacion()))
                        <hr />
                    </li>
                    <li class="col-md-12">
                        <strong>Profession:</strong> {{ $profesor->profesion }}
                    </li>                        
                    <li class="col-md-6">
                        <strong>Country:</strong> {{ $profesor->pais->nombre_en }}
                    </li>
                    <li class="col-md-6">
                        <strong>Nationality:</strong> {{ $profesor->nacionalidad }}
                    </li>
                    @if (Auth::check())
                    <li class="col-md-12">
                        <strong>Gender:</strong> 
                        @if ($profesor->sexo == "M") Male @else Female @endif
                    </li>
                    @endif
                    <li class="col-md-12">                            
                        <strong>Languages:</strong> {{ $profesor->obtenerIdiomasConcat() }}
                    </li>
                    @if (Auth::check())
                    <li class="col-md-12">
                        <strong>Experience:</strong> <br /> {{ $profesor->experiencia }}
                    </li>
                    <li class="col-md-12">
                        <strong>Certificates:</strong>
                        <ul class="row">
                        @foreach ($profesor->certificados as $c)
                            <li class="col-sm-12 col-md-3">
                                <a href="{{ url($lang.'/profesor/certificado/'.$c->id) }}" title="Download"><i class='fa fa-download'></i>&nbsp;{{ $c->descripcion }}</a>
                            </li>
                        @endforeach
                        </ul>
                    </li>                    
                    @endif
                </ul>
            </div>
        </div>
        @if (Auth::check())
        <section class="products">
            <div class="container"> 
                <!--======= TITTLE =========-->
                <div class="tittle">
                    <h3>Teacher's Courses</h3>
                    <hr>
                </div>
                @if(count($profesor->cursos))
                @include($lang.'.curso.listaCursos', array("cursos" => $profesor->cursos))
                @else
                There are not courses to show
                @endif
            </div>
        </section>
        @endif
    </div>
</div>
@stop