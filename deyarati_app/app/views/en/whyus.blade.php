@extends($lang.'.master')

@section('content')
<div class="container">
    <div class="contact-info">
        <div class="row">
            <div class="col-sm-12">
                <h3>About us</h3>
                <p>Derayati.com is an electronic platform works as a mediator between 
                tutors and students. It can be used to book an appointment with a teacher 
                from all over the world for higher education subjects.</p>
               <p>&nbsp;</p>
                <h3>Vision:</h3>
                <p>To become the favorite leading electronic platform in the field of Assistant Education.</p>
               <p>&nbsp;</p>
                <h3>The message:</h3>
                <p>To provide high quality and reliable assistant educational services through our core values.</p>
               <p>&nbsp;</p>
                <h3>Values:</h3>
                    <p>Reliability</p>
                    <p>High and special customer service</p>
                    <p>Diversity</p>
                    <p>Achievement</p>
                    <p>Continuous improvement and development</p>
                    <p>Strong leadership</p>
                    <p>Commitment to excellence through creativity, speed and quality</p>
                    <p>Deep respect for each individual contributes to the success of the groups work through:
                     <blockquote>

                            <p>Integrity and commitment</p>
                            <p>Mutual respect and teamwork</p>
                            <p>Strong sense of social responsibility</p>
                                           </blockquote>

            </div>
        </div>
    </div>
</div>
@stop