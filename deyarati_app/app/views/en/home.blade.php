@extends($lang.'.master')

@section('js_header')
<script>
(function ($){
    function scrollTo(target) {
        var wheight = $(window).height() / 2;
        var ooo = $(target).offset().top - wheight;
        $('html, body').animate({scrollTop:ooo}, 600);
    }
    
    $(document).ready(function() {
        $("select#id_tema1, select#id_tema2").change(function(e){
            var $target = $(this).attr("data-target");
            $.ajax({
                url: $(this).attr("data-url") + "/" + $(this).val(),
                success: function (json) {                    
                    var $select = $("select" + $target);
                    $select.empty();
                    $select.append("<option>Course name</option>");
                    $.each(json, function(i, o) {
                        $select.append("<option value='" + o.id + "'>" + o.nombre_en + "</div>");
                    });
                }
            });
        });
        
        $("#btn_buscar1, #btn_buscar2").click(function(e) {
            e.preventDefault();
            $('.work-in-progress').fadeIn(100);
            var $form = $($(this).attr("data-form"));
            $.ajax({
                url: $form.attr("action"),
                method: "post",
                data: $form.serialize(),
                success: function($html) {
                    $("#lista_cursos").html($html);
                    $('.work-in-progress').fadeOut(100);
                    scrollTo("#lista_cursos");
                }
            });
        });
    });
})(jQuery);
</script>
@stop

@section('content')
<!--======= BANNER =========-->
<div id="banner">
    <div class="flexslider">
        <ul class="slides">

            <!--======= SLIDE 1 =========-->
            <li>
                <img draggable="false" src="{{ asset('images/slide-1.jpg') }}" />
                <div class="container">
                    <div class="text-slider">
                        <div class="col-sm-7">
                            <h3>Save Your Money Book Your Lesson <i class="fa fa-long-arrow-right"></i></h3>                            
                            <a class="btn" href="{{ url('/'.$lang.'/curso') }}">Book lesson</a>
                        </div>
                        <div class="col-sm-5 col-md-4 pull-right hidden-xs">
                            <div class="find-drive sec-form">
                                <h5>Find Course <i class="fa fa-leanpub"></i></h5>
                                <div class="drive-form">
                                    <div class="intres-lesson"> 

                                        <!--======= FORM =========-->
                                        <form id="form_buscar1" method="post" action="{{ url('/'.$lang.'/buscar') }}">
                                            <ul class="row">
                                                <!--======= INPUT SELECT =========-->
                                                <li class="col-sm-6">
                                                    <div class="form-group">
                                                        <select id="tipo_curso" name="tipo_curso">
                                                            <option value="">Course Type</option>
                                                            <option value="P">Class</option>
                                                            <option value="V">Virtual</option>
                                                            <option value="M">Both</option>
                                                        </select>
                                                        <span class="fa fa-book"></span> 

                                                    </div>
                                                </li>
                                                
                                                <!--======= INPUT SELECT =========-->
                                                <li class="col-sm-6">
                                                    <div class="form-group">
                                                        <select name="id_ciudad">
                                                            <option value="">City</option>
                                                            @foreach($ciudades as $c)
                                                            <option value="{{ $c->id }}">{{ $c->nombre_en }}</option>
                                                            @endforeach
                                                        </select>
                                                        <span class="fa fa-building"></span> 
                                                    </div>
                                                </li>
                                                
                                                <!--======= INPUT SELECT =========-->
                                                <li class="col-sm-6">
                                                    <div class="form-group">
                                                        <select name="idioma">
                                                            <option value="">Language</option>
                                                            <option value="english">English</option>
                                                            <option value="arabic">Arabic</option>
                                                        </select>
                                                        <span class="fa fa-language"></span> 
                                                    </div>
                                                </li>
                                                
                                                <!--======= INPUT SELECT =========-->
                                                <li class="col-sm-6">
                                                    <div class="form-group">
                                                        <select id="id_tema1" name="id_tema" data-target="#id_curso1" data-url="{{ url('/'.$lang.'/sesion/obtenerCursos') }}">
                                                            <option value="">Course Subject</option>
                                                            @foreach($temas as $t)
                                                            <option value="{{ $t->id }}">{{ $t->nombre_en }}</option>
                                                            @endforeach
                                                        </select>
                                                        <span class="fa fa-file-text-o"></span> </div>
                                                </li>
                                                
                                                <li class="col-sm-12">
                                                    <div class="form-group">
                                                        <select id="id_curso1" name="id_curso">
                                                            <option value="">Course name</option>
                                                        </select>
                                                        <span class="fa fa-file-text-o"></span> 
                                                    </div>
                                                </li>
                                            </ul>

                                            <!--======= BUTTON =========-->
                                            <div class="text-center">
                                                <button class="btn" id="btn_buscar1" data-form="#form_buscar1">Find My Course</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</div>
<div class="content">
    <!--======= RESPONSIVE FORM =========-->
    <div class="visible-xs" id="find-course">
        <div class="container">
            <div class="col-xs-12">
                <div class="find-drive">
                    <h5>Find Course <i class="fa fa-road"></i></h5>
                    <div class="drive-form">
                        <div class="intres-lesson"> 

                            <!--======= FORM =========-->
                            <form role="form" id="find_course" method="post" action="{{ url('/'.$lang.'/buscar') }}">
                                <ul class="row">
                                    <!--======= INPUT SELECT =========-->
                                    <li class="col-sm-6">
                                        <div class="form-group">
                                            <select id="tipo_curso" name="tipo_curso">
                                                <option value="">Course Type</option>
                                                <option value="P">Class</option>
                                                <option value="V">Virtual</option>
                                                <!--option value="M">Mixed</option-->
                                            </select>
                                            <span class="fa fa-file-text-o"></span> 
                                        </div>                                        
                                    </li>
                                    
                                    <!--======= INPUT SELECT =========-->
                                    <li class="col-sm-6">
                                        <div class="form-group">
                                            <select name="id_ciudad">
                                                <option value="">City</option>
                                                @foreach($ciudades as $c)
                                                <option value="{{ $c->id }}">{{ $c->nombre_en }}</option>
                                                @endforeach
                                            </select>
                                            <span class="fa fa-building"></span> 
                                        </div>
                                    </li>
                                    
                                    <!--======= INPUT SELECT =========-->
                                    <li class="col-sm-6">
                                        <div class="form-group">
                                            <select name="idioma">
                                                <option value="">Language</option>
                                                <option value="english">English</option>
                                                <option value="arabic">Arabic</option>
                                            </select>
                                            <span class="fa fa-language"></span> 
                                        </div>
                                    </li>
                                    
                                    <li class="col-sm-6">
                                        <div class="form-group">
                                            <select id="id_tema2" name="id_tema" data-target="#id_curso2" data-url="{{ url('/'.$lang.'/sesion/obtenerCursos') }}">
                                                <option value="">Course Subject</option>
                                                @foreach($temas as $t)
                                                <option value="{{ $t->id }}">{{ $t->nombre_en }}</option>
                                                @endforeach
                                            </select>
                                            <span class="fa fa-file-text-o"></span> 
                                        </div>
                                    </li>
                                    
                                    <li class="col-sm-12">
                                        <div class="form-group">
                                            <select id="id_curso2" name="id_curso">
                                                <option value="">Course name</option>
                                            </select>
                                            <span class="fa fa-file-text-o"></span> 
                                        </div>
                                    </li>
                                </ul>

                                <!--======= BUTTON =========-->
                                <div class="text-center">
                                    <button class="btn" id="btn_buscar1" data-form="#find_course">Find My Course</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!--======= RODUCTS / ITEMS =========-->
    <section id="products" class="products">
        <div class="container"> 
            <!--======= TITTLE =========-->
            <div class="tittle">
                <h3>Our Courses</h3>
                <hr>
            </div>

            <!--======= PRODUCTS ROW =========-->
            <div id="lista_cursos">
                @include($lang.'.curso.listaCursos', array("cursos" => $cursos))            
                <div class="text-center margin-t-40"> <a href="{{ url('/'.$lang.'/curso') }}" class="btn"> See all courses </a> 
                </div>
            </div>
        </div>
    </section>    

    <!--======= NEWS / FAQS =========-->
    <section class="news faqs-with-bg" data-stellar-background-ratio="0.5">
        <div class="container"> 

            <!--======= TITTLE =========-->
            <div class="tittle">
                <h3>Faqs</h3>
                <hr>
            </div>
            <div class="row"> 
                <!--======= ACCORDING =========-->
                <div class="col-sm-6">                    
                    <div class="panel-group" id="accordion">
                        <!--======= ACCORDING 1=========-->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">How does Derayati.com work?</a> </h4>
                            </div>

                            <!--======= ADD INFO HERE =========-->
                            <div id="collapseOne" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <p> Derayati.com works as a mediator between tutors and students. Students can arrange their schedules and reserve an appointment with a tutor in any subject.</p>
                                </div>
                            </div>
                        </div>

                        <!--======= ACCORDING 2 =========-->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="collapsed"> What can I get help with?</a> </h4>
                            </div>

                            <!--======= ADD INFO HERE =========-->
                            <div id="collapseTwo" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p> Our tutors can assist with studying, test prep and homework help. <a href="{{ url('/'.$lang.'/curso') }}"> Take a look at a complete list of the subjects.</a></p>
                                </div>
                            </div>
                        </div>

                        <!--======= ACCORDING 3 =========-->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="collapsed"> How do I get a tutor?</a> </h4>
                            </div>

                            <!--======= ADD INFO HERE =========-->
                            <div id="collapseThree" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p> You need an active Derayati.com account to connect to a tutor. If you don't have an account, <a href="{{ url('/'.$lang.'/sesion/formLogin') }}">click here to get started now.</a>  If you already have an account, just sign in and reserve an appointment. </p>
                                </div>
                            </div>
                        </div>

                        <!--======= ACCORDING 4 =========-->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapsefour" class="collapsed">When is Derayati.com available?</a> </h4>
                            </div>

                            <!--======= ADD INFO HERE =========-->
                            <div id="collapsefour" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p> 24 hours a day, seven days a week, 361 days a year.</p>
                                </div>
                            </div>
                        </div>

                                                            <!--======= ACCORDING 5 =========-->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse5" class="collapsed"> Can I evaluate my tutor?</a> </h4>
                            </div>

                            <!--======= ADD INFO HERE =========-->
                            <div id="collapse5" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p> YES! After the session is completed you can evaluate your tutor by selecting the deserved number of stars.</a></p>
                                </div>
                            </div>
                        </div>


                                                            <!--======= ACCORDING 6 =========-->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse6" class="collapsed"> What if my tutor can't help me?</a> </h4>
                            </div>

                            <!--======= ADD INFO HERE =========-->
                            <div id="collapse6" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p> If there are no tutors for a specific subject, Let us know and we will do our best to add the subject in our <a href="{{ url('/'.$lang.'/curso') }}"> subjects list </a>and find a tutor to satisfy your expectation. </a></p>
                                </div>
                            </div>
                        </div>

                                                    <!--======= ACCORDING 7 =========-->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse7" class="collapsed"> Schedule a session with a tutor?</a> </h4>
                            </div>

                            <!--======= ADD INFO HERE =========-->
                            <div id="collapse7" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p> Students can schedule an appointment with a specific tutor. To find a tutor and set up an appointment:<a href="{{ url('/'.$lang.'/sesion/formLogin') }}">Sign in to your account,</a> Type the name of the subject and choose a tutor from the search result.</a></p>
                                </div>
                            </div>
                        </div>

                        <!--======= ACCORDING 5 =========-->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse8" class="collapsed"> Contact Customer Support?</a> </h4>
                            </div>

                            <!--======= ADD INFO HERE =========-->
                            <div id="collapse8" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>Send us an email (info@derayati.com). We're available Sunday through Thursday from 5 PM to 10:00 PM, Friday and Saturday 9:00 AM to 6:00 PM (Saudi Arabia +3)</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!--======= NEWS ARTICALS =========-->
                <div class="col-md-6"> </div>
            </div>
        </div>
    </section>
</div>
@stop