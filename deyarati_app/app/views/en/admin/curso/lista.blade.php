@extends($lang.'.master')

@section('js_header')
@stop

@section('content')                    
<div class="container">
    <div class="contact-info">
        <div class="row">
            <div class="col-md-12">
                <h3>Courses</h3>
                <a class="btn" href="{{ url('/'.$lang.'/admin/curso/crear') }}">Create new course</a>
                &nbsp;
                <a href="{{ url('/'.$lang.'/admin/curso/crear') }}" class="btn">New <i class="fa fa-plus"></i></a>
                <br />
                <br />
            </div>
        </div>
        <div class="row">
            <div class="col-md-3"><strong>Subject</strong></div>
            <div class="col-md-3"><strong>Course name</strong></div>
            <div class="col-md-3"><strong>Teacher</strong></div>
            <div class="col-md-2"><strong>Edit</strong></div>
            <div class="col-md-1"><strong>Disapprove</strong></div>
            <br />
            <br />
        </div>
        @foreach($cursos as $c)
        <div class="row">
            <div class="col-md-3">{{ $c->curso->tema->nombre_en }}</div>
            <div class="col-md-3">{{ $c->curso->nombre_en }}</div>
            <div class="col-md-3">{{ $c->profesor->nombre }} {{ $c->profesor->apellido }}</div>
            <div class="col-md-2"><a href="{{ url('/'.$lang.'/admin/curso/editar/'.$c->id) }}"><i class="fa fa-pencil"></i></a></div>
            <div class="col-md-1">@if ($c->estado == 'A')<a href="{{ url('/'.$lang.'/admin/curso/desaprobar/'.$c->id) }}"><i class="fa fa-remove"></i></a>@endif</div>
            <br />
            <br />
        </div>
        @endforeach
        <div class="row">
            <div class="col-sm-12 text-center">
                {{ $cursos->links() }}
            </div>
        </div>
    </div>
</div>
@stop