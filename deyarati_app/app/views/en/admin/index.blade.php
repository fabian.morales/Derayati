@extends($lang.'.master')

@section('content')
<div class="container">
    <div class="contact-info">
        <div class="row">
            <div class="col-sm-6">
            	<h3> Administrative Tools </h3>
      		<hr>
                <ul>
                    <li><a href="{{ url('/'.$lang.'/administradores') }}">Administrative Users</a></li>
                    <li><a href="{{ url('/'.$lang.'/profesores') }}">Teachers</a></li>
                    <li><a href="{{ url('/'.$lang.'/estudiantes') }}">Students</a></li>
                    <li><a href="{{ url('/'.$lang.'/admin/curso') }}">Courses</a></li>
                    <li><a href="{{ url('/'.$lang.'/admin/curso/pendientes') }}">Approve Courses</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
@stop


