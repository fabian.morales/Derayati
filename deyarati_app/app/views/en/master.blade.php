<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>::Derayati::</title>
        <meta name="keywords" content="Derayati, Courses, Teachers, KSA" >
        <meta name="description" content="Courses, Teachers,">
        <meta name="author" content="">

        <!-- FONTS ONLINE -->
        <link href='http://fonts.googleapis.com/css?family=Roboto:500,900,300,700,400' rel='stylesheet' type='text/css'>

        <!--MAIN STYLE-->
        <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/main.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/animate.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/responsive.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
        <!--======= COLOR STYLE CSS =========-->
        <link rel="stylesheet" id="color" href="{{ asset('css/color/default.css') }}">
        @section ('css_header')
        @show

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <!--======= PRELOADER =========-->
        <div class="work-in-progress">
            <div id="preloader">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>        

        <!--======= WRAPPER =========-->
        <div id="wrap"> 

            <!--======= TOP BAR =========-->
            <div class="top-bar">
                <div class="container">
                    <ul class="left-bar-side">
                        <li> <a href="{{ url('/'.$lang.'/acerca') }}" title="About Us">Why Us</a> - </li>                        
                        <li> <a href="#"><i class="fa fa-facebook"></i></a> - </li>
                        <li> <a href="https://twitter.com/_derayati_"><i class="fa fa-twitter"></i></a></li>                        
                    </ul>
                    <ul class="right-bar-side">
                        <li> <a href="mailto:info@derayati.com"><i class="fa fa-envelope"></i> info@deyarati.com </a></li>
                        @if (Auth::check())
                        <li> <a href="{{ url('/'.$lang.'/sesion/perfil') }}"><i class="fa fa-user"></i> My profile </a></li>
                        <li> <a href="{{ url('/'.$lang.'/sesion/logout') }}"><i class="fa fa-power-off"></i> Logout </a></li>
                        
                        @if (Auth::user()->tipo == 'A')
                        <li> <a href="{{ url('/'.$lang.'/admin') }}"><i class="fa fa-briefcase"></i> Administrator </a></li>
                        @endif
                        
                        @else
                        <li> <a href="{{ url('/'.$lang.'/sesion/formLogin') }}"><i class="fa fa-user"></i> Login/Register </a></li>
                        @endif
                        <li> <a href="{{ url('/ar/') }}"><i class="fa fa-check"></i> Arabic </a></li>
                        <li> <a href="{{ url('/en/') }}"><i class="fa fa-check"></i> English </a></li>
                    </ul>
                </div>
            </div>

            <!--======= HEADER =========-->
            <header class="sticky">
                <div class="container">
                    <nav class="navbar navbar-default">
                        <div class="row">
                            <div class="navbar-header col-md-3 col-sm-3">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nav-res"> <span class="sr-only">Toggle navigation</span> <span class="fa fa-navicon"></span> </button>
                                <!--======= LOGO =========-->
                                <div class="logo"> <a href="{{ url('/'.$lang) }}"><img src="{{ asset('images/logo_home.png') }}" /></a></div>
                            </div>

                            <!--======= MENU =========-->
                            <div class="col-md-9 col-sm-9">
                                <div class="collapse navbar-collapse" id="nav-res">
                                    <ul class="nav navbar-nav">                         
                                        <li><a href="{{ url('/'.$lang) }}">Home</a></li>
                                        <li><a href="{{ url('/'.$lang.'/acerca') }}">Why Us</a></li>
                                        <li><a href="{{ url('/'.$lang.'/curso') }}">Courses</a></li>
                                        <li><a href="{{ url('/'.$lang.'/profesor') }}">Teachers</a></li>
                                        <li><a href="{{ url('/'.$lang.'/contacto') }}">Contact Us</a></li>
                                        @if (!Auth::check())
                                        <li class="hidden-md hidden-lg"><a href="{{ url('/'.$lang.'/sesion/formLogin') }}">Login / Register</a></li>                                        
                                        @else
                                        <li class="hidden-md hidden-lg"><a href="{{ url('/'.$lang.'/sesion/perfil') }}">My profile</a></li>
                                         @if (Auth::user()->tipo == 'A')
                                        <li class="hidden-md hidden-lg"><a href="{{ url('/'.$lang.'/admin') }}">Administrator</a></li>
                                        @endif
                                        <li class="hidden-md hidden-lg"><a href="{{ url('/'.$lang.'/sesion/logout') }}">Logout</a></li>
                                        @endif
                                        <li class="hidden-md hidden-lg"><a href="{{ url('/ar/') }}"><i class="fa fa-check"></i> Arabic </a></li>
                                        <li class="hidden-md hidden-lg"><a href="{{ url('/en/') }}"><i class="fa fa-check"></i> English </a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </nav>
                </div>
            </header>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="mensajes">
                            @if (Session::has('mensajeError'))            
                            <div class="alert alert-danger alert-dismissable" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{ Session::get('mensajeError') }}
                            </div>            
                            @endif
                            @if (Session::has('mensaje'))
                            <div class="alert alert-success alert-dismissable" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{ Session::get('mensaje') }}
                            </div>
                            @endif 
                            @if (Session::has('mensajeExt'))            
                                {{ Session::get('mensajeExt') }}            
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            @yield('content')
            <!--======= CONTENT START =========-->
            <div class="content">
                <!--======= QUOTE =========-->
                <section class="quote">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-10">
                                <h1>Are You Looking For a Course in KSA</h1>                                
                        </div>
                    </div>
                </section>

                <!--======= FOOTER =========-->
                <footer>
                    <!--======= RIGHTS =========-->
                    <div class="rights">
                        <div class="container">
                            <ul class="row">
                                <li class="col-md-8">
                                    <p>All Rights Reserved ©</p>
                                </li>
                                <!--======= SOCIAL ICON =========-->
                                <li class="col-md-4">
                                    <ul class="social_icons">
                                        <li class="facebook"><a href="#."><i class="fa fa-facebook"></i></a></li>
                                        <li class="twitter"><a href="https://twitter.com/_derayati_"><i class="fa fa-twitter"></i></a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="{{ asset('js/jquery-1.11.0.min.js') }}"></script> 
        <script src="{{ asset('js/wow.min.js') }}"></script> 
        <script src="{{ asset('js/bootstrap.min.js') }}"></script> 
        <script src="{{ asset('js/drive-me-plugin.js') }}"></script> 
        <script src="{{ asset('js/jquery.isotope.min.js') }}"></script> 
        <script src="{{ asset('js/jquery.flexslider-min.js') }}"></script> 
        <script src="{{ asset('js/mapmarker.js') }}"></script> 
        <script src="{{ asset('js/color-switcher.js') }}"></script> 
        <script src="{{ asset('js/jquery.magnific-popup.min.js') }}"></script> 
        <script src="{{ asset('js/jquery.validate.min.js') }}"></script> 
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
        <script src="{{ asset('js/main.js') }}"></script>
        @section ('js_header')
        @show
    </body>
</html>