<div class="row">
    <div class="small-12 medium-8 large-6 end columns">        
        <div class="row">
            <div class="small-12 columns">
                <a href="{{ url('usuarios/crear') }}" class="button rojo">Nuevo <i class="fi-plus"></i></a>
            </div>
        </div>
        <div class="row titulo lista">
            <div class="small-12 columns">Lista de usuarios</div>
        </div>
        <div class="row item lista">
            <div class="small-2 columns">Consecutivo</div>
            <div class="small-4 columns">Nombre</div>            
            <div class="small-4 columns">Correo</div>
            <div class="small-2 columns">Editar</div>
        </div>
        @foreach($usuarios as $u)
        <div class="row item lista">
            <div class="small-2 columns">{{ $u->id }}</div>
            <div class="small-4 columns">{{ $u->nombre }}</div>            
            <div class="small-4 columns">{{ $u->email }}</div>
            <div class="small-2 columns"><a href="{{ url('/usuarios/editar/'.$u->id) }}"><i class="fi-pencil"></i></a></div>
        </div>                
        <!--a href="{{ url('/usuarios/permisos/'.$u->id) }}"><i class="fi-checkbox"></i></a-->
        @endforeach
    </div>
</div>
<div class="row">
    <div class="small-12 columns text-center">
        {{ $usuarios->links() }}
    </div>
</div>