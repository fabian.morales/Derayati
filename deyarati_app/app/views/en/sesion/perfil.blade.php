@extends($lang.'.master')

@section('js_header')
<script>
(function ($){
    $(document).ready(function() {        
        $("#btn_submit_lang").click(function(e) {
            e.preventDefault();
            var $idiomaEn = $('#idiomas option:selected').attr('data-en');
            var $idiomaAr = $('#idiomas option:selected').attr('data-ar');
            $.ajax({
                url: "{{ url($lang.'/sesion/adicionarIdioma') }}",
                method: 'post',
                data: {'idioma_en': $idiomaEn, 'idioma_ar': $idiomaAr, 'id_profesor': '{{ $usuario->id }}'},
                success: function(){
                    window.location.reload();
                }
            });
        });
        
        $("#perfil_form, #clave_form, #form_certificado").validate({
            submitHandler: function(form) {
                form.submit();
            }
        });
    });
})(jQuery);
</script>
@stop

@section('content')

@if ($usuario->tipo == "E")
<div class="container">
    <div class="contact-info">
        <div class="row">
            <div class="col-sm-12">
                <h3>My Profile</h3>
                <p class="azul">{{ $usuario->nombre }} {{ $usuario->apellido }} </p>
                <hr>
            </div>
            <div class="col-sm-6">
                <p>Change your password</p>                
                <form role="form" id="clave_form" method="post" action="{{ url('/'.$lang.'/sesion/cambiarClave') }}">
                    <ul class="row">
                        <li class="col-md-6">
                            <input type="password" class="form-control"
                                   name="_password" id="_password" placeholder="Password"
                                   data-toggle="tooltip" title="Password is required" required
                                   />
                        </li>                        
                        <li class="col-md-12">
                            <button type="submit" value="submit" class="btn f_right" id="btn_submit_clave">Change</button>
                        </li>
                    </ul>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6"><h3>Courses</h3></div>
            @foreach($usuario->inscripciones as $i)
            <div class="col-md-6 box">
                <strong>{{ $i->curso->curso->tema->nombre_en }}</strong> 
                <br /> 
                <a href="{{ url('/'.$lang.'/curso/inscripcion/'.$i->id) }}">{{ $i->curso->curso->nombre_en }}</a>
                <br />
                Teacher: {{ $i->curso->profesor->nombre }} {{ $i->curso->profesor->apellido }}
            </div>
            @endforeach
        </div>
    </div>
</div>
@endif

@if ($usuario->tipo != 'E')
<div class="container">
    <div class="contact-info">
        <div class="row"> 
            <div class="col-sm-12 col-md-6">
                <h3>My Profile</h3>
                <p class="azul">{{ $usuario->nombre }} {{ $usuario->apellido }} </p>
                <hr>
                <form role="form" id="perfil_form" method="post" enctype="multipart/form-data" action="{{ url('/'.$lang.'/sesion/actualizar') }}">
                    <input type="hidden" name="login" value="{{ $usuario->login }}" />
                    <input type="hidden" name="ciudad" value="{{ $usuario->ciudad }}" />
                    <input type="hidden" name="tipo" value="{{ $usuario->tipo }}" />
                    <ul class="row">                        
                        <li class='col-md-5'>
                            @if (is_file(public_path('avatares/'.$usuario->id.'.jpg')))
                            <img class='img-responsive' src='{{ asset('avatares/'.$usuario->id.'.jpg') }}' />
                            @else
                            
                            @if ($usuario->sexo == 'M')
                            <img class='img-responsive' src='{{ asset('images/avatar_male.png') }}' />
                            @else
                            <img class='img-responsive' src='{{ asset('images/avatar_female.png') }}' />
                            @endif
                            
                            @endif
                        </li>
                        <li class='col-md-7'>
                            <p>Change your avatar</p>
                            <input type='file' name='avatar' value='Select your avatar' />
                        </li>
                    </ul>
                    <ul class="row">
                        <li class="col-md-6">
                            <p>Name</p>
                            <input type="text" class="form-control"
                                   name="nombre" id="nombre" placeholder="Name"
                                   data-toggle="tooltip" title="Name is required" value='{{ $usuario->nombre }}' required
                                   />
                        </li>
                        <li class="col-md-6">
                            <p>Last name</p>
                            <input type="text" class="form-control"
                                   name="apellido" id="apellido" placeholder="Last Name"
                                   data-toggle="tooltip" title="Last Name is required" value='{{ $usuario->apellido }}' required
                                   />
                        </li>                        
                        <li class="col-md-6">
                            <p>Password</p>
                            <input type="password" class="form-control"
                                   name="password" id="clave" placeholder="Password"
                                   data-toggle="tooltip" title="Password is required" required
                                   />
                        </li>
                        <li class="col-md-6">
                            <p>Email</p>
                            <input type="email" class="form-control"
                                   name="email" id="email" placeholder="Email" value='{{ $usuario->email }}' required
                                   />
                        </li>
                        <li class="col-md-6">
                            <p>Phone number</p>
                            <input type="text" class="form-control"
                                   name="telefono" id="telefono" placeholder="Phone number" value='{{ $usuario->telefono }}' required
                                   />
                        </li>
                        <li class="col-md-12">
                            <p>Country</p>
                            <select name='pais_residencia' id='pais_residencia' required>
                                <option value="">Select your country</option>
                                @foreach($paises as $p)
                                <option value='{{ $p->id }}' @if($p->id == $usuario->pais_residencia) selected @endif>{{ $p->nombre_en }}</option>
                                @endforeach
                            </select>
                        </li>                        
                        <li class="col-md-6" id='col_sexo'>
                            <p>Gender</p>
                            <select id='sexo' name='sexo' required>
                                <option value="">Gender</option>
                                <option value='M' @if($usuario->sexo == "M") selected @endif>Male</option>
                                <option value='F' @if($usuario->sexo == "F") selected @endif>Female</option>
                            </select>
                        </li>
                        <li class="col-md-6" id='col_nacionalidad'>
                            <p>Nacionality</p>
                            <input type="text" class="form-control"
                                   name="nacionalidad" id="nacionalidad" placeholder="Nacionality" value='{{ $usuario->nacionalidad }}' required
                                   />
                        </li>
                        <li class="col-md-12" id='col_profesion'>
                            <input type="text" class="form-control"
                                   name="profesion" id="profesion" placeholder="Profession" value='{{ $usuario->profesion }}' required
                                   />
                        </li>                        
                        <li class="col-md-12">
                            <p>Experience</p>
                            <textarea class="form-control"
                                   name="experiencia" id="experiencia" placeholder="Experience" required
                                   >{{ $usuario->experiencia }}</textarea>
                        </li>
                        <li class="col-md-12">
                            <button type="submit" value="submit" class="btn f_right" id="btn_submit_update">Update</button>
                        </li>
                    </ul>
                </form>
            </div>
            <div class="col-md-6 col-sm-12">                
                <a href="{{ url('/'.$lang.'/sesion/cursosp') }}" class="btn">My courses</a>
            </div>
        </div>
        <div class='row'>
            <div class="col-sm-12 col-md-6 box">
                <h3>My languages</h3>
                <form>
                    <ul>
                        @foreach ($usuario->idiomas()->get() as $i)
                        <li>
                            <a href='{{ url($lang.'/sesion/quitarIdioma/'.$i->id) }}' title="Delete"><i class='fa fa-remove'></i><a>&nbsp;{{ $i->idioma_en }}
                        </li>
                        @endforeach
                        <li>
                            <p>Add a new language</p>
                            <select name='idiomas' id='idiomas'>
                                <option data-en='English' data-ar='Ingles'>English</option>
                                <option data-en='Arabic' data-ar='Arabe'>Arabic</option>
                                <option data-en='Spanish' data-ar='Español'>Spanish</option>
                            </select>
                        </li>
                        <li>
                            <button type="button" class="btn f_right" id="btn_submit_lang">Add</button>
                        </li>
                    </ul>
                </form>
            </div>
        </div>
        <hr />
        <div class='row'>
            <div class="col-sm-12 col-md-6 box">
                <h3>My certificates</h3>                
                <form id="form_certficado" name="form_certificado" enctype="multipart/form-data" method="post" action="{{ url($lang.'/sesion/adicionarCertificado') }}">
                    <ul>
                        @foreach ($usuario->certificados()->get() as $c)
                        <li>
                            <a href="{{ url($lang.'/sesion/certificado/'.$c->id) }}" title="Download"><i class='fa fa-download'></i>&nbsp;{{ $c->descripcion }}</a>
                        </li>
                        @endforeach
                        <li>
                            <p>Add a new certificate</p>
                            <input type='text' id='certificado' name='certificado' class="form-control" placeholder="Certificate name" required />
                        </li>
                        <li>
                            <input type='file' id='archivo_cert' name='archivo_cert' required />
                        </li>                        
                        <li>
                            <input type="submit" class="btn f_right" id="btn_submit_cert" value="Add" />
                        </li>
                    </ul>
                </form>
            </div>
        </div>
    </div>
</div>
@endif
@stop