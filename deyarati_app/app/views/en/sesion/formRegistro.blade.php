@extends($lang.'.master')

@section('js_header')
<script>
(function ($){
    $(document).ready(function() {        
        $("select#pais_residencia").change(function(e){
            $.ajax({
                url: $(this).attr("data-url") + "/" + $(this).val(),
                success: function (json) {                    
                    var $select = $("select#ciudad");
                    $select.empty();
                    $select.append("<option>Select your city</option>");
                    $.each(json, function(i, o) {
                        $select.append("<option value='" + o.id + "'>" + o.nombre_en + "</div>");
                    });
                }
            });
        });
        
        /*$("select#estado").change(function(e){
            $.ajax({
                url: $(this).attr("data-url") + "/" + $(this).val(),
                success: function (json) {                    
                    var $select = $("select#ciudad");
                    $select.empty();
                    $select.append("<option>Select your city</option>");
                    $.each(json, function(i, o) {
                        $select.append("<option value='" + o.id + "'>" + o.nombre_en + "</div>");
                    });
                }
            });
        });*/
        
        $("select#tipo").change(function(e){
            if ($(this).val() === 'P'){
                $("#col_sexo").removeClass("hide");
                $("#col_nacionalidad").removeClass("hide");
                $("#col_profesion").removeClass("hide");
            }
            else{
                $("#col_sexo").addClass("hide");
                $("#col_nacionalidad").addClass("hide");
                $("#col_profesion").addClass("hide");
            }
        });
        
        $("#login_form").validate({
            submitHandler: function(form) {
                form.submit();
            }
        });
        
        $("#registro_form").validate({
            submitHandler: function(form) {
                form.submit();
            }
        });
    });
})(jQuery);
</script>
@stop

@section('content')                    
<div class="container">
    <!--======= CONTACT INFORMATION =========-->
    <div class="contact-info">
        <div class="row"> 

            <!--======= CONTACT FORM =========-->
            <div class="col-sm-6">
                <h3>Login</h3>                
                <form role="form" id="login_form" method="post" action="{{ url('/'.$lang.'/sesion/login') }}">
                    <ul class="row">
                        <li class="col-md-6">
                            <input type="text" class="form-control"
                                   name="_login" id="_login" placeholder="User Name"
                                   data-toggle="tooltip" title="User Name is required" required
                                   />
                        </li>
                        <li class="col-md-6">
                            <input type="password" class="form-control"
                                   name="_password" id="_password" placeholder="Password"
                                   data-toggle="tooltip" title="Password is required" required
                                   />
                        </li>
                        <li class="col-md-12"><a href="{{ url('/'.$lang.'/sesion/password/requestReset') }}">Forgot your password?</a></li>
                        <li class="col-md-12">
                            <button type="submit" value="submit" class="btn f_right" id="btn_submit_login">Login</button>
                        </li>
                    </ul>
                </form>
            </div>
            <div class="col-sm-6">
                <h3>Sign up</h3>                
                <form role="form" id="registro_form" method="post" action="{{ url('/'.$lang.'/sesion/registrar') }}">
                    <ul class="row">
                        <li class="col-md-12">
                            <select id="tipo" name="tipo" required>
                                <option value="">User type</option>
                                <option value="E">Student</option>
                                <option value="P">Teacher</option>
                            </select>
                        </li>                        
                        <li class="col-md-6">
                            <input type="text" class="form-control"
                                   name="nombre" id="nombre" placeholder="Name"
                                   data-toggle="tooltip" title="Name is required" required
                                   />
                        </li>
                        <li class="col-md-6">
                            <input type="text" class="form-control"
                                   name="apellido" id="apellido" placeholder="Last Name"
                                   data-toggle="tooltip" title="Last Name is required" required
                                   />
                        </li>
                        <li class="col-md-6">
                            <input type="text" class="form-control"
                                   name="login" id="login" placeholder="User Name"
                                   data-toggle="tooltip" title="User Name is required" required
                                   />
                        </li>
                        <li class="col-md-6">
                            <input type="password" class="form-control"
                                   name="password" id="clave" placeholder="Password"
                                   data-toggle="tooltip" title="Password is required" required
                                   />
                        </li>
                        <li class="col-md-6">
                            <input type="text" class="form-control"
                                   name="email" id="email" placeholder="Email"
                                   data-toggle="tooltip" title="Email is required" required
                                   />
                        </li>
                        <li class="col-md-6">
                            <input type="text" class="form-control"
                                   name="telefono" id="telefono" placeholder="Phone number"
                                   />
                        </li>
                        <li class="col-md-6">
                            <select name='pais_residencia' id='pais_residencia' data-url='{{ url('/ciudades') }}' required>
                                <option value="">Select your country</option>
                                @foreach($paises as $p)
                                <option value='{{ $p->id }}'>{{ $p->nombre_en }}</option>
                                @endforeach
                            </select>
                        </li>
                        <!--li class="col-md-6">
                            <select name='estado' id='estado' data-url='{{ url('/ciudades') }}'>
                                <option>Select your state</option>                                
                            </select>
                        </li-->
                        <li class="col-md-6">
                            <select name='ciudad' id='ciudad' required>
                                <option value="">Select your city</option>                                
                            </select>
                        </li>
                        <li class="col-md-6 hide" id='col_sexo'>
                            <select id='sexo' name='sexo' required>
                                <option value="">Gender</option>
                                <option value='M'>Male</option>
                                <option value='F'>Female</option>
                            </select>
                        </li>
                        <li class="col-md-6 hide" id='col_nacionalidad'>
                            <input type="text" class="form-control"
                                   name="nacionalidad" id="nacionalidad" placeholder="Nationality" required
                                   />
                        </li>
                        <li class="col-md-6 hide" id='col_profesion'>
                            <input type="text" class="form-control"
                                   name="profesion" id="profesion" placeholder="Profession" required
                                   />
                        </li>
                        <li>
                            <button type="submit" value="submit" class="btn f_right" id="btn_registrar">Sign up</button>
                        </li>
                    </ul>
                </form>
            </div>
        </div>
    </div>
</div>
@stop