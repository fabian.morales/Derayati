@extends($lang.'.master')

@section('js_header')
<script>
(function ($){
    $(document).ready(function() {        
        $("select#id_tema").change(function(e){
            $.ajax({
                url: $(this).attr("data-url") + "/" + $(this).val(),
                success: function (json) {                    
                    var $select = $("select#id_curso");
                    $select.empty();
                    $select.append("<option>Course name</option>");
                    $.each(json, function(i, o) {
                        $select.append("<option value='" + o.id + "'>" + o.nombre_en + "</div>");
                    });
                }
            });
        });
        
        $("input[type='checkbox']").change(function() {
            var $target = "#" + $(this).attr("data-target");            
            if($(this).is(":checked")){
                $($target).val('');
                $($target).prop('readonly', true);
            }
            else{                
                $($target).prop('readonly', false);
            }
        });
        
        $("div[rel='time-select']").slider({
            range: true,
            min: 0,
            max: 48,
            step: 1,
            values: [0, 5],
            slide: function (e, ui) {
                var $val1 = ui.values[0]  * 30;
                var hours1 = Math.floor($val1 / 60);
                var minutes1 = $val1 - (hours1 * 60);

                if (hours1.length === 1) hours1 = '0' + hours1;
                if (minutes1.length === 1) minutes1 = '0' + minutes1;
                if (minutes1 === 0) minutes1 = '00';
                if (hours1 >= 12) {
                    if (hours1 === 12) {
                        hours1 === hours1;
                        minutes1 = minutes1 + " PM";
                    } else {
                        hours1 = hours1 - 12;
                        minutes1 = minutes1 + " PM";
                    }
                } else {
                    //hours1 = hours1;
                    minutes1 = minutes1 + " AM";
                }
                if (hours1 === 0) {
                    hours1 = 12;
                    //minutes1 = minutes1;
                }
                
                var $val2 = ui.values[1] * 30;
                var hours2 = Math.floor($val2 / 60);
                var minutes2 = $val2 - (hours2 * 60);

                if (hours2.length === 1) hours2 = '0' + hours2;
                if (minutes2.length === 1) minutes2 = '0' + minutes2;
                if (minutes2 === 0) minutes2 = '00';
                if (hours2 >= 12) {
                    if (hours2 === 12) {
                        //hours2 = hours2;
                        minutes2 = minutes2 + " PM";
                    } else if (hours2 === 24) {
                        hours2 = 11;
                        minutes2 = "59 PM";
                    } else {
                        hours2 = hours2 - 12;
                        minutes2 = minutes2 + " PM";
                    }
                } else {
                    //hours2 = hours2;
                    minutes2 = minutes2 + " AM";
                }

                $($(this).attr("data-target")).val(hours1 + ':' + minutes1 + ' to ' + hours2 + ':' + minutes2);
            }
        });
        
        $("#curso_form").validate({
            submitHandler: function(form) {
                form.submit();
            }
        });
    });
})(jQuery);
</script>
@stop

@section('content')                    
<div class="container">    
    <div class="contact-info">
        <div class="row">            
            <div class="col-sm-6">
                <h3>Course information</h3>                
                <form role="form" id="curso_form" enctype="multipart/form-data" method="post" action="{{ url('/'.$lang.'/sesion/guardarCurso') }}">
                    <input type="hidden" name="id" value="{{ $curso->id }}" />
                    <ul class="row">
                        <li class="col-md-6">
                            <select id="id_tema" name="id_tema" data-url="{{ url('/'.$lang.'/sesion/obtenerCursos') }}" required>
                                <option value="">Subject</option>
                                @foreach($temas as $t)
                                <option value="{{ $t->id }}" @if(sizeof($tema) && $t->id == $tema->id) selected @endif>{{ $t->nombre_en }}</option>
                                @endforeach
                            </select>
                        </li>                        
                        <li class="col-md-6">
                            <select id="id_curso" name="id_curso" required>
                                <option value="">Course name</option>
                                @if(sizeof($tema))
                                @foreach($tema->cursos()->get() as $c)
                                <option value="{{ $c->id }}" @if($c->id == $curso->id_curso) selected @endif>{{ $c->nombre_en }}</option>
                                @endforeach
                                @endif
                            </select>
                        </li>
                        <li class="col-md-12">
                            <textarea class="form-control"
                                   name="descripcion_corta" id="descripcion_corta" placeholder="Short description"
                                   data-toggle="tooltip" required
                                   >{{ $curso->descripcion_corta }}</textarea>
                        </li>
                        <li class="col-md-12">
                            <textarea class="form-control"
                                   name="descripcion" id="descripcion" placeholder="Description"
                                   data-toggle="tooltip" required
                                   >{{ $curso->descripcion }}</textarea>
                        </li>
                        <li class="col-md-6">
                            <select id="nivel" name="nivel" required>
                                <option value="">Level</option>                                
                                <option value="B" @if($curso->nivel == "B") selected @endif>Low</option>
                                <option value="M" @if($curso->nivel == "M") selected @endif>Medium</option>
                                <option value="A" @if($curso->nivel == "A") selected @endif>High</option>
                            </select>
                        </li>
                        <li class="col-md-6">
                            <!--input type="text" class="form-control"
                               name="ciudad" id="ciudad" placeholder="City" value="{{ $curso->ciudad }}"
                               /-->
                            <select name="ciudad" id="ciudad" required>
                                <option value="">Select the city</option>
                                @foreach($ciudades as $c)
                                <option value="{{ $c->id }}" @if($c->id == $curso->ciudad) selected @endif>{{ $c->nombre_en }} - {{ $c->pais->nombre_en }}</option>
                                @endforeach
                            </select>
                        </li>
                        <li class="col-sm-12 clearfix" style="height: 1px; margin: 0;">&nbsp;</li>
                        <li class="col-md-6">
                            <select id="modalidad" name="modalidad" required>
                                <option value="">Mode</option>                                
                                <option value="P" @if($curso->modalidad == "P") selected @endif>Classroom</option>
                                <option value="V" @if($curso->modalidad == "V") selected @endif>Virtual</option>
                                <!--option value="M" @if($curso->modalidad == "M") selected @endif>Mixed</option-->
                            </select>
                        </li>
                        <li class="col-md-6">
                            <input type="text" class="form-control"
                               name="idioma" id="idioma" placeholder="Languages" value="{{ $curso->idioma }}" required
                               />
                        </li>
                        <li class="col-md-12 box">
                            <h4>Schedule</h4>
                            <ul class="row">
                                <li class="col-md-12">Please, use the slider to select the time range.</li>
                                <li class="col-md-3">Monday</li>
                                <li class="col-md-5">
                                    <div id="slider-lunes" data-target="#h_lunes" rel="time-select"></div>
                                </li>
                                <li class="col-md-3">
                                    <input type="text" class="form-control" name="h_lunes" id="h_lunes" value="{{ $curso->h_lunes }}" @if($curso->id && empty($curso->h_lunes)) readonly @endif />
                                </li>
                                <li class="col-md-1">
                                    <input type="checkbox" @if($curso->id && empty($curso->h_lunes)) checked @endif id="h_lunes_off" data-target="h_lunes" />
                                    <label for="h_lunes_off">Off</label>
                                </li>
                                
                                <li class="col-md-3">Tuesday</li>
                                <li class="col-md-5">
                                    <div id="slider-martes" data-target="#h_martes" rel="time-select"></div>
                                </li>
                                <li class="col-md-3">
                                    <input type="text" class="form-control" name="h_martes" id="h_martes" value="{{ $curso->h_martes }}" @if($curso->id && empty($curso->h_martes)) readonly @endif  />
                                </li>
                                <li class="col-md-1">
                                    <input type="checkbox" @if($curso->id && empty($curso->h_martes)) checked @endif id="h_martes_off" data-target="h_martes" />
                                    <label for="h_martes_off">Off</label>
                                </li>
                                
                                <li class="col-md-3">Wednesday</li>
                                <li class="col-md-5">
                                    <div id="slider-miercoles" data-target="#h_miercoles" rel="time-select"></div>
                                </li>
                                <li class="col-md-3"><input type="text" class="form-control" name="h_miercoles" id="h_miercoles" value="{{ $curso->h_miercoles }}" @if($curso->id && empty($curso->h_miercoles)) readonly @endif /></li>
                                <li class="col-md-1">
                                    <input type="checkbox" @if($curso->id && empty($curso->h_miercoles)) checked @endif id="h_miercoles_off" data-target="h_miercoles" />
                                    <label for="h_miercoles_off">Off</label>
                                </li>
                                
                                <li class="col-md-3">Thursday</li>
                                <li class="col-md-5">
                                    <div id="slider-jueves" data-target="#h_jueves" rel="time-select"></div>
                                </li>
                                <li class="col-md-3"><input type="text" class="form-control" name="h_jueves" id="h_jueves" value="{{ $curso->h_jueves }}" @if($curso->id && empty($curso->h_jueves)) readonly @endif /></li>
                                <li class="col-md-1">
                                    <input type="checkbox" @if($curso->id && empty($curso->h_jueves)) checked @endif id="h_jueves_off" data-target="h_jueves" />
                                    <label for="h_jueves_off">Off</label>
                                </li>                                
                                
                                <li class="col-md-3">Friday</li>
                                <li class="col-md-5">
                                    <div id="slider-viernes" data-target="#h_viernes" rel="time-select"></div>
                                </li>
                                <li class="col-md-3"><input type="text" class="form-control" name="h_viernes" id="h_viernes" value="{{ $curso->h_viernes }}" @if($curso->id && empty($curso->h_viernes)) readonly @endif /></li>
                                <li class="col-md-1">
                                    <input type="checkbox" @if($curso->id && empty($curso->h_viernes)) checked @endif id="h_viernes_off" data-target="h_viernes" />
                                    <label for="h_viernes_off">Off</label>
                                </li>
                                
                                <li class="col-md-3">Saturday</li>
                                <li class="col-md-5">
                                    <div id="slider-sabado" data-target="#h_sabado" rel="time-select"></div>
                                </li>
                                <li class="col-md-3"><input type="text" class="form-control" name="h_sabado" id="h_sabado" value="{{ $curso->h_sabado }}" @if($curso->id && empty($curso->h_sabado)) readonly @endif /></li>
                                <li class="col-md-1">
                                    <input type="checkbox" @if($curso->id && empty($curso->h_sabado)) checked @endif id="h_sabado_off" data-target="h_sabado" />
                                    <label for="h_sabado_off">Off</label>
                                </li>
                                
                                <li class="col-md-3">Sunday</li>
                                <li class="col-md-5">
                                    <div id="slider-domingo" data-target="#h_domingo" rel="time-select"></div>
                                </li>
                                <li class="col-md-3"><input type="text" class="form-control" name="h_domingo" id="h_domingo" value="{{ $curso->h_domingo }}" @if($curso->id && empty($curso->h_domingo)) readonly @endif /></li>
                                <li class="col-md-1">
                                    <input type="checkbox" @if($curso->id && empty($curso->h_domingo)) checked @endif id="h_domingo_off" data-target="h_domingo" />
                                    <label for="h_domingo_off">Off</label>
                                </li>
                            </ul>
                        </li>
                        <li class="col-md-6">
                            Certificate
                            <input type="file"
                               name="certificado" id="certificado" @if(!$curso->id) required @endif
                               />
                        </li>
                        <li class="col-md-6">
                            <input type="text" class="form-control"
                                   name="costo" id="costo" placeholder="Cost per hour (SAR)" value="{{ $curso->costo }}" required
                               />
                        </li>
                        <li class="col-md-12">
                            <textarea class="form-control" name="notas" id="notas" placeholder="Notes" required>{{ $curso->notas }}</textarea>
                        </li>
                        <li>
                            <button type="submit" value="submit" class="btn f_right" id="btn_guardar">Save</button>
                        </li>
                    </ul>
                </form>
            </div>
        </div>
    </div>
</div>
@stop