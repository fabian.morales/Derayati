@extends($lang.'.master')

@section('js_header')
@stop

@section('content')                    
<div class="container">
    <div class="contact-info">
        <div class="row">Password recovery</h3>
            <div style="clear: both"></div>
            <br />
            <p>We've sent a message to your email with instructions to recover your password</p>            
        </div>
    </div>
</div>
@stop
