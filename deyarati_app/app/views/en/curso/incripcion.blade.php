@extends($lang.'.master')

@section('js_header')
<script>
(function ($){
    $(document).ready(function() {        
        
    });
})(jQuery);
</script>
@stop

@section('content')                    
<div class="container">    
    <div class="contact-info">
        <div class="row">            
            <div class="col-sm-6">
                <h3>Course information</h3>                
                <form role="form" id="curso_form" enctype="multipart/form-data" method="post" action="{{ url('/'.$lang.'/sesion/guardarCurso') }}">
                    <input type="hidden" name="id" value="{{ $curso->id }}" />
                    <ul class="row">
                        <li class="col-md-6">
                            <strong>Subject:</strong> {{ $curso->curso->tema->nombre_en }}
                        </li>                        
                        <li class="col-md-6">
                            <strong>Course name:</strong> {{ $curso->curso->nombre_en }}
                        </li>
                        <li class="col-md-12">
                            <strong>Description</strong>
                            <hr />
                            {{ $curso->descripcion }}
                        </li>
                        <li class="col-md-6">                            
                            <strong>Level:</strong>
                            @if($curso->nivel == "B") Low @endif>
                            @if($curso->nivel == "M") Medium @endif
                            @if($curso->nivel == "A") High @endif                           
                        </li>
                        <li class="col-md-6">
                            <strong>Ciudad</strong>: {{ $curso->ciudad }}
                        </li>
                        <li class="col-md-6">
                            <strong>Mode:</strong>                                
                            @if($curso->nivel == "P") Classroom @endif
                            @if($curso->nivel == "V") Virtual @endif
                            @if($curso->nivel == "M") Mixed @endif
                            </select>
                        </li>
                        <li class="col-md-6">
                            <strong>Laguages:</strong> {{ $curso->idioma }}
                        </li>
                        <li class="col-md-12 box">
                            <h4>Schedule</h4>
                            <ul class="row">
                                <li class="col-md-6"><label for="h_lunes">Monday</label></li>
                                <li class="col-md-6">
                                    @if (!empty($curso->h_lunes) && trim($curso->h_lunes))
                                    <input type="checkbox" name="h_lunes" id="h_lunes" value="Y" />
                                    @else
                                    Off
                                    @endif
                                </li>
                                
                                <li class="col-md-6"><label for="h_martes">Tuesday</label></li>
                                <li class="col-md-6">
                                    @if (!empty($curso->h_martes) && trim($curso->h_martes))
                                    <input type="checkbox" name="h_martes" id="h_martes" value="Y" />
                                    @else
                                    Off
                                    @endif
                                </li>
                                
                                <li class="col-md-6"><label for="h_martes">Wednesday</label></li>
                                <li class="col-md-6">
                                    @if (!empty($curso->h_miercoles) && trim($curso->h_miercoles))
                                    <input type="checkbox" name="h_miercoles" id="h_miercoles" value="Y" />
                                    @else
                                    Off
                                    @endif
                                </li>
                                
                                <li class="col-md-6"><label for="h_martes">Thursday</label></li>
                                <li class="col-md-6">
                                    @if (!empty($curso->h_jueves) && trim($curso->h_jueves))
                                    <input type="checkbox" name="h_jueves" id="h_jueves" value="Y" />
                                    @else
                                    Off
                                    @endif
                                </li>
                                
                                <li class="col-md-6"><label for="h_martes">Friday</label></li>
                                <li class="col-md-6">
                                    @if (!empty($curso->h_viernes) && trim($curso->h_viernes))
                                    <input type="checkbox" name="h_viernes" id="h_viernes" value="Y" />
                                    @else
                                    Off
                                    @endif
                                </li>
                                
                                <li class="col-md-6"><label for="h_martes">Saturday</label></li>
                                <li class="col-md-6">
                                    @if (!empty($curso->h_sabado) && trim($curso->h_sabado))
                                    <input type="checkbox" name="h_sabado" id="h_sabado" value="Y" />
                                    @else
                                    Off
                                    @endif
                                </li>
                                
                                <li class="col-md-6"><label for="h_martes">Sunday</label></li>
                                <li class="col-md-6">
                                    @if (!empty($curso->h_domingo) && trim($curso->h_domingo))
                                    <input type="checkbox" name="h_domingo" id="h_domingo" value="Y" />
                                    @else
                                    Off
                                    @endif
                                </li>
                            </ul>
                        </li>
                        <li class="col-md-6">
                            <a href=""><i class="fa fa-download"></i>&nbsp; Download Certificate</a>
                        </li>
                        <li class="col-md-6">
                            <strong>Cost:</strong> ${{ $curso->costo }}
                        </li>
                        <li class="col-md-12">
                            <p><strong>Notes:</strong></p>
                            {{ $curso->notas }}                            
                        </li>
                        <li>
                            <button type="submit" value="submit" class="btn f_right" id="btn_inscribirse">Book</button>
                        </li>
                    </ul>
                </form>
            </div>
        </div>
    </div>
</div>
@stop