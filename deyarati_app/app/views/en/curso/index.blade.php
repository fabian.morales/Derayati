@extends($lang.'.master')

@section('js_header')
<script>
    (function ($){
        $(document).ready(function() {
            $("#btn_filter").click(function() {
                var $url = '{{ url("/".$lang."/curso") }}?id_tema=' + $("select#id_tema").val();
                window.location.href = $url;                
            });
        });
    })(jQuery);
</script>
@stop

@section('content')
<!--======= RODUCTS / ITEMS =========-->
<section id="products" class="products">
    <div class="container"> 
        <!--======= TITTLE =========-->
        <div class="tittle">
            <h3>Our Courses</h3>
      
            <hr>
            <p> Get a tutor anytime in any math, science, social studies and English subject. We cover many subjects. Advanced courses and test prep, too. </p>
            <hr>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <select id="id_tema">
                    <option value="">All subjects</option>
                    @foreach($temas as $t)
                    <option value="{{ $t->id }}" @if(!empty($idTema) && $t->id == $idTema) selected @endif>{{ $t->nombre_en }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-sm-2">
                <button class="btn" id="btn_filter">Filter</button>
            </div>
        </div>
        <hr />
        @include($lang.'.curso.listaCursos', array("cursos" => $cursos))
    </div>
</section>
@stop