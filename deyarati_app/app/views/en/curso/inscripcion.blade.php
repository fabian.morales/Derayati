@extends($lang.'.master')

@section('js_header')
<script>
(function ($){
    $(document).ready(function() {        
        
    });
})(jQuery);
</script>
@stop

@section('content')                    
<div class="container">    
    <div class="contact-info">
        <div class="row">            
            <div class="col-sm-6">
                <h3>Course Booking</h3>                
                <form role="form" id="curso_form" enctype="multipart/form-data" method="post" action="{{ url('/'.$lang.'/curso/guardarInscripcion') }}">
                    <input type="hidden" name="id_curso" value="{{ $curso->id }}" />
                    <input type="hidden" name="id_profesor_curso" value="{{ $curso->id_profesor }}" />
                    <ul class="row">
                        <li class="col-md-12">
                            <strong>Teacher: </strong>{{ $curso->profesor->nombre }} {{ $curso->profesor->apellido }}
                        </li>
                        <li class="col-md-6">
                            <strong>Subject:</strong> {{ $curso->curso->tema->nombre_en }}
                        </li>                        
                        <li class="col-md-6">
                            <strong>Course name:</strong> {{ $curso->curso->nombre_en }}
                        </li>
                        <li class="col-md-12">
                            <strong>Description:</strong>
                            <br />
                            {{ $curso->descripcion }}
                            <hr />
                        </li>                        
                        <li class="col-md-6">                            
                            <strong>Level:</strong>
                            @if($curso->nivel == "B") Low @endif
                            @if($curso->nivel == "M") Medium @endif
                            @if($curso->nivel == "A") High @endif                           
                        </li>
                        <li class="col-md-6">
                            <strong>Ciudad</strong>: {{ $curso->ciudad }}
                        </li>
                        <li class="col-md-6">
                            <strong>Mode:</strong>                                
                            @if($curso->modalidad == "P") Classroom @endif
                            @if($curso->modalidad == "V") Virtual @endif
                            @if($curso->modalidad == "M") Mixed @endif
                            </select>
                        </li>
                        <li class="col-md-6">
                            <strong>Languages:</strong> {{ $curso->idioma }}
                        </li>
                        <li class="col-md-12 box">
                            <h4>Schedule</h4>
                            <ul class="row">
                                <li class="col-md-9"><label for="h_lunes">Monday: </label> {{ $curso->h_lunes }}</li>
                                <li class="col-md-3">
                                    @if (!empty($curso->h_lunes) && trim($curso->h_lunes))
                                    <input type="checkbox" name="h_lunes" id="h_lunes" value="Y" />
                                    @else
                                    Off
                                    @endif
                                </li>
                                <li class="col-sm-12 clearfix" style="height: 1px; margin: 0;">&nbsp;</li>
                                <li class="col-md-9"><label for="h_martes">Tuesday: </label> {{ $curso->h_martes }}</li>
                                <li class="col-md-3">
                                    @if (!empty($curso->h_martes) && trim($curso->h_martes))
                                    <input type="checkbox" name="h_martes" id="h_martes" value="Y" />
                                    @else
                                    Off
                                    @endif
                                </li>
                                <li class="col-sm-12 clearfix" style="height: 1px; margin: 0;">&nbsp;</li>
                                <li class="col-md-9"><label for="h_martes">Wednesday: </label> {{ $curso->h_miercoles }}</li>
                                <li class="col-md-3">
                                    @if (!empty($curso->h_miercoles) && trim($curso->h_miercoles))
                                    <input type="checkbox" name="h_miercoles" id="h_miercoles" value="Y" />
                                    @else
                                    Off
                                    @endif
                                </li>
                                <li class="col-sm-12 clearfix" style="height: 1px; margin: 0;">&nbsp;</li>
                                <li class="col-md-9"><label for="h_martes">Thursday: </label> {{ $curso->h_jueves }}</li>
                                <li class="col-md-3">
                                    @if (!empty($curso->h_jueves) && trim($curso->h_jueves))
                                    <input type="checkbox" name="h_jueves" id="h_jueves" value="Y" />
                                    @else
                                    Off
                                    @endif
                                </li>
                                <li class="col-sm-12 clearfix" style="height: 1px; margin: 0;">&nbsp;</li>
                                <li class="col-md-9"><label for="h_martes">Friday: </label> {{ $curso->h_viernes }}</li>
                                <li class="col-md-3">
                                    @if (!empty($curso->h_viernes) && trim($curso->h_viernes))
                                    <input type="checkbox" name="h_viernes" id="h_viernes" value="Y" />
                                    @else
                                    Off
                                    @endif
                                </li>
                                <li class="col-sm-12 clearfix" style="height: 1px; margin: 0;">&nbsp;</li>
                                <li class="col-md-9"><label for="h_martes">Saturday: </label> {{ $curso->h_sabado }}</li>
                                <li class="col-md-3">
                                    @if (!empty($curso->h_sabado) && trim($curso->h_sabado))
                                    <input type="checkbox" name="h_sabado" id="h_sabado" value="Y" />
                                    @else
                                    Off
                                    @endif
                                </li>
                                <li class="col-sm-12 clearfix" style="height: 1px; margin: 0;">&nbsp;</li>
                                <li class="col-md-9"><label for="h_martes">Sunday: </label> {{ $curso->h_domingo }}</li>
                                <li class="col-md-3">
                                    @if (!empty($curso->h_domingo) && trim($curso->h_domingo))
                                    <input type="checkbox" name="h_domingo" id="h_domingo" value="Y" />
                                    @else
                                    Off
                                    @endif
                                </li>
                            </ul>
                        </li>
                        <li class="col-md-2">
                            <strong>Hours:</strong>
                        </li>
                        <li class="col-md-3">
                            <input type="text" class="form-control" id="horas" name="horas" />
                        </li>
                        <li class="col-sm-12 clearfix" style="height: 1px; margin: 0;">&nbsp;</li>
                        <li class="col-md-6">
                            @if (is_file(public_path("cursos")."/".$curso->id_profesor."/".$curso->id.".".$curso->certificado))
                            <a href="{{ url('/'.$lang.'/curso/certificado/'.$curso->id) }}"><i class="fa fa-download"></i>&nbsp; Download Certificate</a>
                            @endif
                        </li>
                        <li class="col-md-6">
                            <strong>Cost per hour:</strong> {{ Lang::get("messages.moneda") }} {{ $curso->costo }}
                        </li>
                        <li class="col-md-12">
                            <p><strong>Notes:</strong></p>
                            {{ $curso->notas }}                            
                        </li>
                        <li class="col-md-12">
                            <p><strong>Notes for teacher:</strong></p>
                            <textarea class="form-control"
                                name="notas" id="notas" placeholder="Notes"></textarea>
                        </li>
                        
                        <li>
                            <button type="submit" value="submit" class="btn f_right" id="btn_inscribirse">Book</button>
                        </li>
                    </ul>
                </form>
            </div>
        </div>
    </div>
</div>
@stop