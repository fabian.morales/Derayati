@extends($lang.'.master')

@section('js_header')
<script>
    (function($) {
        $(document).ready(function() {
            $("#btn_rate").click(function(e) {
                e.preventDefault();
                var $url = "{{ url('/'.$lang.'/curso/rate') }}?id_curso={{ $curso->id }}&rate_profesor=" + $("#rate_profesor").val() + "&rate_curso=" + $("#rate_curso").val();
                window.location.href = $url;
            });
        });
    })(jQuery);
</script>
@stop

@section('content')                    
<div class="container">    
    <div class="contact-info">
        <div class="row">            
            <div class="col-sm-6">
                <h3>Course Booking</h3>
                <ul class="row lista_espacio">
                    <li class="col-md-6">
                        <strong>Subject:</strong> {{ $curso->curso->tema->nombre_en }}
                    </li>                        
                    <li class="col-md-6">
                        <strong>Course name:</strong> {{ $curso->curso->nombre_en }}
                    </li>
                    <li class="col-md-12">
                        <strong>Description:</strong>
                        <br />
                        {{ $curso->descripcion }}
                        <hr />
                    </li>
                    <li class="col-md-6">                            
                        <strong>Level:</strong>
                        @if($curso->nivel == "B") Low @endif
                        @if($curso->nivel == "M") Medium @endif
                        @if($curso->nivel == "A") High @endif                           
                    </li>
                    <li class="col-md-6">
                        <strong>City</strong>: {{ $curso->ciudadObj->nombre_en }}
                    </li>
                    <li class="col-md-6">
                        <strong>Mode:</strong>                                
                        @if($curso->modalidad == "P") Classroom @endif
                        @if($curso->modalidad == "V") Virtual @endif
                        @if($curso->modalidad == "M") Mixed @endif
                        </select>
                    </li>
                    <li class="col-md-6">
                        <strong>Laguages:</strong> {{ $curso->idioma }}
                    </li>
                    <li class="col-md-12 box">
                        <h4>Schedule</h4>
                        <ul class="row">
                            <li class="col-md-12">Monday:
                                @if (!empty($curso->h_lunes) && trim($curso->h_lunes) && $inscripcion->h_lunes == "Y") 
                                {{ $curso->h_lunes }}
                                @else
                                Off
                                @endif
                            </li>
                            <li class="col-md-12">Tuesday:
                                @if (!empty($curso->h_martes) && trim($curso->h_martes) && $inscripcion->h_martes == "Y") 
                                {{ $curso->h_martes }}
                                @else
                                Off
                                @endif
                            </li>
                            <li class="col-md-12">Wednesday:
                                @if (!empty($curso->h_miercoles) && trim($curso->h_miercoles) && $inscripcion->h_miercoles == "Y") 
                                {{ $curso->h_miercoles }}
                                @else
                                Off
                                @endif
                            </li>
                            <li class="col-md-12">Thursday:
                                @if (!empty($curso->h_jueves) && trim($curso->h_jueves) && $inscripcion->h_jueves == "Y" && $inscripcion->h_jueves == "Y") 
                                {{ $curso->h_jueves }}
                                @else
                                Off
                                @endif
                            </li>
                            <li class="col-md-12">Friday:
                                @if (!empty($curso->h_viernes) && trim($curso->h_viernes) && $inscripcion->h_viernes == "Y") 
                                {{ $curso->h_viernes }}
                                @else
                                Off
                                @endif
                            </li>
                            <li class="col-md-12">Saturday:
                                @if (!empty($curso->h_sabado) && trim($curso->h_sabado) && $inscripcion->h_sabado == "Y") 
                                {{ $curso->h_sabado }}
                                @else
                                Off
                                @endif
                            </li>
                            <li class="col-md-12">Sunday:
                                @if (!empty($curso->h_domingo) && trim($curso->h_domingo) && $inscripcion->h_domingo == "Y") 
                                {{ $curso->h_domingo }}
                                @else
                                Off
                                @endif
                            </li>
                        </ul>
                    </li>                       
                    <li class="col-md-6">
                        @if (is_file(public_path("cursos")."/".$curso->id_profesor."/".$curso->id.".".$curso->certificado))
                        <a href="{{ url('/'.$lang.'/curso/certificado/'.$curso->id) }}"><i class="fa fa-download"></i>&nbsp; Download Certificate</a>
                        @endif
                    </li>            
                    <li class="col-md-6">
                        <strong>Cost per hour:</strong> {{ Lang::get("messages.moneda") }} {{ $curso->costo }}
                    </li>            
                    <li class="col-md-12">
                        <p><strong>Notes:</strong></p>
                        {{ $curso->notas }}                            
                    </li>
                    <li class="col-md-12">
                        <p><strong>Message from student:</strong></p>
                        {{ $inscripcion->notas }}                            
                    </li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-12">
                <h4>Rate this course (1-5)</h4>
                <select id="rate_curso">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                </select>
                <h4>Rate the teacher (1-5)</h4>
                <select id="rate_profesor">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                </select>
                <br />
                <br />
                <button class="btn pull-right" id="btn_rate">Rate</button>
            </div>
        </div>
    </div>
</div>
@stop