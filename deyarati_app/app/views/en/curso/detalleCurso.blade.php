@extends($lang.'.master')

@section('js_header')
<script>
    (function($) {
        $(document).ready(function() {
            $("#btn_rate").click(function(e) {
                e.preventDefault();
                var $url = "{{ url('/'.$lang.'/curso/rate') }}?id_curso={{ $curso->id }}&rate_profesor=" + $("#rate_profesor").val() + "&rate_curso=" + $("#rate_curso").val();
                window.location.href = $url;
            });
        });
    })(jQuery);
</script>
@stop

@section('content')                    
<div class="container">    
    <div class="contact-info">
        <div class="row">            
            <div class="col-sm-6">
            	<h3> Courses Details</h3>
      		<hr>
       	 
                <ul class="row lista_espacio">
                    @if (!Auth::check())
                    <li class="col-md-12"><span class="azul">If you want see more information or book your course, please<a href="{{ url('/'.$lang.'/sesion/formLogin') }}"> register</a></span></li>
                    @endif
                    <li class="col-md-12">
                        <strong>Teacher: </strong>{{ $curso->profesor->nombre }} {{ $curso->profesor->apellido }}
                        @include("rate", array("valor" => $curso->profesor->obtenerCalificacion()))
                    </li>
                    <li class="col-md-6">
                        <strong>Subject:</strong> {{ $curso->curso->tema->nombre_en }}
                    </li>                        
                    <li class="col-md-6">
                        <strong>Course name:</strong> {{ $curso->curso->nombre_en }}
                        @include("rate", array("valor" => $curso->obtenerCalificacion()))
                    </li>
                    <li class="col-md-12">
                        <strong>Description:</strong>
                        <br />
                        @if (Auth::check())
                        {{ $curso->descripcion }}
                        @else
                        {{ $curso->descripcion_corta }}
                        @endif
                        <hr />
                    </li>
                    <li class="col-md-6">                            
                        <strong>Level:</strong>
                        @if($curso->nivel == "B") Low @endif
                        @if($curso->nivel == "M") Medium @endif
                        @if($curso->nivel == "A") High @endif                           
                    </li>
                    <li class="col-md-6">
                        <strong>City</strong>: {{ $curso->ciudadObj->nombre_en }}
                    </li>
                    <li class="col-md-6">
                        <strong>Mode:</strong>                                
                        @if($curso->modalidad == "P") Class @endif
                        @if($curso->modalidad == "V") Virtual @endif
                        @if($curso->modalidad == "M") Mixed @endif
                        </select>
                    </li>
                    <li class="col-md-6">
                        <strong>Languages:</strong> {{ $curso->idioma }}
                    </li>
                    @if (Auth::check())
                    <li class="col-md-12 box">
                        <h4>Schedule</h4>
                        <ul class="row">
                            <li class="col-md-12">Monday:
                                @if (!empty($curso->h_lunes) && trim($curso->h_lunes)) 
                                {{ $curso->h_lunes }}
                                @else
                                Off
                                @endif
                            </li>
                            <li class="col-md-12">Tuesday:
                                @if (!empty($curso->h_martes) && trim($curso->h_martes)) 
                                {{ $curso->h_martes }}
                                @else
                                Off
                                @endif
                            </li>
                            <li class="col-md-12">Wednesday:
                                @if (!empty($curso->h_miercoles) && trim($curso->h_miercoles)) 
                                {{ $curso->h_miercoles }}
                                @else
                                Off
                                @endif
                            </li>
                            <li class="col-md-12">Thursday:
                                @if (!empty($curso->h_jueves) && trim($curso->h_jueves)) 
                                {{ $curso->h_jueves }}
                                @else
                                Off
                                @endif
                            </li>
                            <li class="col-md-12">Friday:
                                @if (!empty($curso->h_viernes) && trim($curso->h_viernes)) 
                                {{ $curso->h_viernes }}
                                @else
                                Off
                                @endif
                            </li>
                            <li class="col-md-12">Saturday:
                                @if (!empty($curso->h_sabado) && trim($curso->h_sabado)) 
                                {{ $curso->h_sabado }}
                                @else
                                Off
                                @endif
                            </li>
                            <li class="col-md-12">Sunday:
                                @if (!empty($curso->h_domingo) && trim($curso->h_domingo)) 
                                {{ $curso->h_domingo }}
                                @else
                                Off
                                @endif
                            </li>
                        </ul>
                    </li>
                    @endif
                    <li class="col-sm-12 clearfix" style="height: 1px; margin: 0;">&nbsp;</li>
                    @if (Auth::check())
                    <li class="col-md-6">
                        @if (is_file(public_path("cursos")."/".$curso->id_profesor."/".$curso->id.".".$curso->certificado))
                        <a href="{{ url('/'.$lang.'/curso/certificado/'.$curso->id) }}"><i class="fa fa-download"></i>&nbsp; Download Certificate</a>
                        @endif
                        <p> </p>
                        <a href="{{ url('/'.$lang.'/curso/inscribirse/'.$curso->id) }}" class="btn">BOOK NOW</a> 
                    </li>
                    @endif
                    <li class="col-md-6">
                        <strong>Cost per hour:</strong> {{ Lang::get("messages.moneda") }} {{ $curso->costo }}
                    </li>
                    @if (Auth::check())
                    <li class="col-md-12">
                        <p><strong>Notes:</strong></p>
                        {{ $curso->notas }}                            
                    </li>
                    @endif
                </ul>
            </div>
            @if (Auth::check() && Auth::user()->tipo == "E" && $inscrito == "Y")
            <div class="col-md-3 col-sm-12">
                <h4>Rate this course (1-5)</h4>
                <select id="rate_curso">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                </select>
                <h4>Rate the teacher (1-5)</h4>
                <select id="rate_profesor">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                </select>
                <br />
                <br />
                <button class="btn pull-right" id="btn_rate">Rate</button>
            </div>
            @endif
        </div>
    </div>
</div>
@stop