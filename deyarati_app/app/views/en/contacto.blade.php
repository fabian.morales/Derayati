@extends($lang.'.master')

@section('content')
<div class="container">
    <div class="contact-info">
        <div class="row">
            <div class="col-sm-6">
                <h3>Contact us</h3>
                <form method="post" action="{{ url('/'.$lang.'/contacto/enviar') }}">
                    <ul class="row">
                        <li class="col-md-6">
                            <input type="text" name="nombre" id="nombre" placeholder="Name" class="form-control" />
                        </li>
                        <li class="col-md-6">
                            <input type="text" name="email" id="email" placeholder="Email" class="form-control" />
                        </li>
                        <li class="col-md-12">
                            <input type="text" name="tema" id="tema" placeholder="Subject" class="form-control" />
                        </li>
                        <li class="col-md-12">
                            <textarea name="notas" id="notas" placeholder="Notes" class="form-control"></textarea>
                        </li>
                        <li class="col-md-12">
                            <input type="submit" value="Submit" class="btn" />
                        </li>
                    </ul>
                </form>                
            </div>
        </div>
    </div>
</div>
@stop