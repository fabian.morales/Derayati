<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <table>
            <tr>
                <td><img src="{{ asset('/images/logo_home.png') }}" /></td>
                <td><h1>Derayati</h1></td>
            </tr>
        </table>
        <h2>Password Reset</h2>
        <div>
            To reset your password, complete this form: 
            <a href="{{ URL::to('/'.BaseController::$lang.'/sesion/password/reset', array($token)) }}">{{ URL::to('/'.BaseController::$lang.'/sesion/password/reset', array($token)) }}</a><br/>
            This link will expire in {{ Config::get('auth.reminder.expire', 60) }} minutes.
        </div>
    </body>
</html>
