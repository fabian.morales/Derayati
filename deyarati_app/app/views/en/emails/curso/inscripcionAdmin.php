<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <h2>Course book</h2>
        <p>The student <strong>{{ $alumno->nombre }} {{ $alumno->apellido }}</strong> was enrolled in the course
           <strong>{{ $curso->curso->tema->nombre_en }} {{ $curso->curso->nombre_en }}</strong>. The teacher for
           this course is <strong>{{ $curso->profesor->nombre }} {{ $curso->profesor->apellido }}</strong>.
        <h3>Course information</h3>
        <ul class="row lista_espacio">
            <li class="col-md-6">
                <strong>Student email:</strong> {{ $alumno->email }}
            </li>            
            <li class="col-md-6">
                <strong>Subject:</strong> {{ $curso->curso->tema->nombre_en }}
            </li>
            <li class="col-md-6">
                <strong>Course name:</strong> {{ $curso->curso->nombre_en }}
            </li>
            <li class="col-md-12">
                <strong>Description:</strong>
                <br />
                {{ $curso->descripcion }}
                <hr />
            </li>
            <li class="col-md-6">                            
                <strong>Level:</strong>
                @if($curso->nivel == "B") Low @endif
                @if($curso->nivel == "M") Medium @endif
                @if($curso->nivel == "A") High @endif                           
            </li>
            <li class="col-md-6">
                <strong>City</strong>: {{ $curso->ciudad }}
            </li>
            <li class="col-md-6">
                <strong>Mode:</strong>                                
                @if($curso->modalidad == "P") Classroom @endif
                @if($curso->modalidad == "V") Virtual @endif
                @if($curso->modalidad == "M") Mixed @endif
                </select>
            </li>
            <li class="col-md-6">
                <strong>Laguages:</strong> {{ $curso->idioma }}
            </li>
            <li class="col-md-12 box">
                <h4>Schedule</h4>
                <ul class="row">
                    <li class="col-md-12">Monday:
                        @if (!empty($curso->h_lunes) && trim($curso->h_lunes) && $inscripcion->h_lunes == "Y") 
                        {{ $curso->h_lunes }}
                        @endif
                    </li>
                    <li class="col-md-12">Tuesday:
                        @if (!empty($curso->h_martes) && trim($curso->h_martes) && $inscripcion->h_martes == "Y") 
                        {{ $curso->h_martes }}
                        @endif
                    </li>
                    <li class="col-md-12">Wednesday:
                        @if (!empty($curso->h_miercoles) && trim($curso->h_miercoles) && $inscripcion->h_miercoles == "Y") 
                        {{ $curso->h_miercoles }}
                        @endif
                    </li>
                    <li class="col-md-12">Thursday:
                        @if (!empty($curso->h_jueves) && trim($curso->h_jueves) && $inscripcion->h_jueves == "Y" && $inscripcion->h_jueves == "Y") 
                        {{ $curso->h_jueves }}
                        @endif
                    </li>
                    <li class="col-md-12">Friday:
                        @if (!empty($curso->h_viernes) && trim($curso->h_viernes) && $inscripcion->h_viernes == "Y") 
                        {{ $curso->h_viernes }}
                        @endif
                    </li>
                    <li class="col-md-12">Saturday:
                        @if (!empty($curso->h_sabado) && trim($curso->h_sabado) && $inscripcion->h_sabado == "Y") 
                        {{ $curso->h_sabado }}
                        @endif
                    </li>
                    <li class="col-md-12">Sunday:
                        @if (!empty($curso->h_domingo) && trim($curso->h_domingo) && $inscripcion->h_domingo == "Y") 
                        {{ $curso->h_domingo }}                        
                        @endif
                    </li>
                </ul>
            </li>                       
            <li class="col-md-6">
                <a href="{{ url('/'.$lang.'/curso/certificado/'.$curso->id) }}"><i class="fa fa-download"></i>&nbsp; Download Certificate</a>
            </li>            
            <li class="col-md-6">
                <strong>Cost per hour:</strong> ${{ $curso->costo }}
            </li>            
            <li class="col-md-12">
                <p><strong>Notes:</strong></p>
                {{ $curso->notas }}                            
            </li>
            <li class="col-md-12">
                <p><strong>Message from student:</strong></p>
                {{ $inscripcion->notas }}                            
            </li>
        </ul>
    </body>
</html>