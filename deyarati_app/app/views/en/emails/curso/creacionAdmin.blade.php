<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <table>
            <tr>
                <td><img src="{{ asset('/images/logo_home.png') }}" /></td>
                <td><h1>Derayati</h1></td>
            </tr>
        </table>
        <h2>New course</h2>
        <p>The Teacher <strong>{{ $curso->profesor->nombre }} {{ $curso->profesor->apellido }}</strong>
        created a new course <strong>{{ $curso->curso->tema->nombre_en }} - {{ $curso->curso->nombre_en }}</strong>.
        Please, check it to approve it.</p>
    </body>
</html>
