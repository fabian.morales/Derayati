<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <table>
            <tr>
                <td><img src="{{ asset('/images/logo_home.png') }}" /></td>
                <td><h1>Derayati</h1></td>
            </tr>
        </table>
        <hr />        
        <h2>Your course was approved</h2>
        <p>Your course <strong>{{ $curso->curso->tema->nombre_en }} - {{ $curso->curso->nombre_en }}</strong>
        was approved</p>
    </body>
</html>