<!DOCTYPE html>
<html lang="es-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <table>
            <tr>
                <td><img src="{{ asset('/images/logo_home.png') }}" /></td>
                <td><h1>Derayati</h1></td>
            </tr>
        </table>
        <h2 Your new account</h2>
        <p>Welcome {{ $usuario->nombre }} to Derayati. Your data:
        <ul>
            <li><strong>Username:</strong>{{ $usuario->login }}</li>
            <li><strong>Password: </strong>{{ $clave }}</li>
        </ul>
    </body>
</html>