<!DOCTYPE html>
<html lang="es-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <table>
            <tr>
                <td><img src="{{ asset('/images/logo_home.png') }}" /></td>
                <td><h1>Derayati</h1></td>
            </tr>
        </table>
        <h2>Contact request</h2>
        <ul>
            <li><strong>Name:</strong>{{ $nombre }}</li>
            <li><strong>Subject:</strong>{{ $tema }}</li>
            <li><strong> Email: </strong>{{ $email }}</li>
            <li><strong> Notes: </strong>{{ $notas }}</li>
        </ul>
    </body>
</html>