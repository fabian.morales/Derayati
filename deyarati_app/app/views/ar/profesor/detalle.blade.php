@extends($lang.'.master')

@section('js_header')
@stop

@section('content')                    
<div class="container">    
    <div class="contact-info">
        <div class="row">            
            <div class="col-sm-6">
                <h3>معلومات المعلم</h3>
                <ul class="row lista_espacio">
                    @if (!Auth::check())
                   <li class="col-md-12"><span class="azul">إذا كنت تريد معرفة المزيد من المعلومات أو الحجز دراستك، يرجى<a href="{{ url('/'.$lang.'/sesion/formLogin') }}"> تسجيل</a></span></li>
                    @else
                    <li class="col-md-5">
                        @if (is_file(public_path('avatares/'.$profesor->id.'.jpg')))
                        <img class='img-responsive' src='{{ asset('avatares/'.$profesor->id.'.jpg') }}' />
                        @else

                        @if ($profesor->sexo == 'M')
                        <img class='img-responsive' src='{{ asset('images/avatar_male.png') }}' />
                        @else
                        <img class='img-responsive' src='{{ asset('images/avatar_female.png') }}' />
                        @endif

                        @endif
                    </li>
                    @endif
                    
                    <li class="col-md-12">
                        <strong>{{ $profesor->nombre }} {{ $profesor->apellido }}</strong>
                        @include("rate", array("valor" => $profesor->obtenerCalificacion()))
                        <hr />
                    </li>
                    <li class="col-md-12">
                        <strong>المهنة:</strong> {{ $profesor->profesion }}
                    </li>                        
                    <li class="col-md-6">
                        <strong>البلد:</strong> {{ $profesor->pais->nombre_ar }}
                    </li>
                    <li class="col-md-6">
                        <strong>الجنسية:</strong> {{ $profesor->nacionalidad }}
                    </li>
                    @if (Auth::check())
                    <li class="col-md-12">
                        <strong>الجنس:</strong> 
                        @if ($profesor->sexo == "M") ذكر @else أنثى @endif
                    </li>
                    @endif
                    <li class="col-md-12">                            
                        <strong>اللغات:</strong> {{ $profesor->obtenerIdiomasConcat() }}
                    </li>
                    @if (Auth::check())
                    <li class="col-md-12">
                        <strong>الخبرة: </strong> <br /> {{ $profesor->experiencia }}
                    </li>
                    <li class="col-md-12">
                        <strong>شهادات:</strong>
                        <ul class="row">
                        @foreach ($profesor->certificados as $c)
                            <li class="col-sm-12 col-md-3">
                                <a href="{{ url($lang.'/profesor/certificado/'.$c->id) }}" title="Download"><i class='fa fa-download'></i>&nbsp;{{ $c->descripcion }}</a>
                            </li>
                        @endforeach
                        </ul>
                    </li>                    
                    @endif
                </ul>
            </div>
        </div>
        @if (Auth::check())
        <section class="products">
            <div class="container"> 
                <!--======= TITTLE =========-->
                <div class="tittle">
                    <h3>مقررات المعلم</h3>
                    <hr>
                </div>
                @if(count($profesor->cursos))
                @include($lang.'.curso.listaCursos', array("cursos" => $profesor->cursos))
                @else
                ليست هناك دورات لاظهار
                @endif
            </div>
        </section>
        @endif
    </div>
</div>
@stop