@extends($lang.'.master')

@section('js_header')
<script>
    (function($) {
        $(document).ready(function() {
            $('#btn_buscar').click(function(e) {
                e.preventDefault();
                var $url = "{{ url('/'.$lang.'/profesor/') }}?id_tema=" + $("#id_tema").val() + "&nombre=" + $("#nombre").val() + "&apellido=" + $("#apellido").val();
                window.location.href = $url;
            });
        });
    })(jQuery);
</script>
@stop

@section('content')                    
<div class="container">    
    <div class="contact-info">
        <div class="row">            
            <div class="col-sm-12">
                <div class="tittle">
                    <h3>المعلمون</h3>
                    <hr>
                    <p> احجز موعد لدرس مساعد مع أحد المعلمين في أي وقت ولأي مادة من مواد التعليم العالي واختبارات التهيئة لدخول الجامعة وما بعد الجامعة والاختبارات العالمية</p>
                    <hr>
                </div>
                <div class='row'>                    
                    <div class='col-md-4'>
                        <select name='id_tema' id='id_tema'>
                            <option value=''>التخصص</option>
                            @foreach($temas as $t)
                            <option value='{{ $t->id }}' @if($t->id == $idTema) selected @endif>{{ $t->nombre_en }}</option>
                            @endforeach
                        </select>
                    </div>                    
                    <div class='col-md-3'>
                        <input type='text' name='nombre' id='nombre' class='form-control' placeholder="اسم الأستاذ الأول" value='{{ $nombre }}' />
                    </div>
                    <div class='col-md-3'>
                        <input type='text' name='apellido' id='apellido' class='form-control' placeholder="اسم الأستاذ الأخير" value='{{ $apellido }}' />
                    </div>
                    <div class='col-md-2'>
                        <input type='button' id='btn_buscar' value='ابحث' class='btn' />
                    </div>
                </div>
                <hr />
                <ul class="row lista_espacio">
                    @foreach($profesores as $p)
                    <li class="col-md-6">
                        <div class="box">                            
                            <div class="row">
                                @if (Auth::check())
                                <div class="col-md-3">
                                    @if (is_file(public_path('avatares/'.$p->id.'.jpg')))
                                    <img class='img-responsive' src='{{ asset('avatares/'.$p->id.'.jpg') }}' />
                                    @else

                                    @if ($p->sexo == 'M')
                                    <img class='img-responsive' src='{{ asset('images/avatar_male.png') }}' />
                                    @else
                                    <img class='img-responsive' src='{{ asset('images/avatar_female.png') }}' />
                                    @endif

                                    @endif
                                </div>
                                @endif
                                <div class="@if(Auth::check()) col-md-9 @else col-md-12 @endif">
                                    <div>
                                        <a href="{{ url('/'.$lang.'/profesor/detalle/'.$p->id) }}"><strong>{{ $p->nombre }} {{ $p->apellido }}</strong></a>
                                        @include("rate", array("valor" => $p->obtenerCalificacion()))
                                    </div>
                                    <div><a href="{{ url('/'.$lang.'/profesor/detalle/'.$p->id) }}">{{ $p->profesion }}</a></div>
                                    <hr />
                                    <p class="azul"> الخبرات </p>
                                    <div>{{ $p->experiencia }}</div>
                                    <div><a href="{{ url('/'.$lang.'/profesor/detalle/'.$p->id) }}"> المزيد >>> </a></div>
                                </div>
                            </div>
                        </div>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {{ $profesores->links() }}
            </div>
        </div>
    </div>
</div>
@stop