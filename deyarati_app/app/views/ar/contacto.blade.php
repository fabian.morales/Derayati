@extends($lang.'.master')

@section('content')
<div class="container">
    <div class="contact-info">
        <div class="row">
            <div class="col-sm-6">
                <h3>اتصل بنا</h3>
                <form method="post" action="{{ url('/'.$lang.'/contacto/enviar') }}">
                    <ul class="row">
                        <li class="col-md-6">
                            <input type="text" name="nombre" id="nombre" placeholder="الاسم" class="form-control" />
                        </li>
                        <li class="col-md-6">
                            <input type="text" name="email" id="email" placeholder="البريد الإلكتروني" class="form-control" />
                        </li>
                        <li class="col-md-12">
                            <input type="text" name="tema" id="tema" placeholder="عنوان الرسالة" class="form-control" />
                        </li>
                        <li class="col-md-12">
                            <textarea name="notas" id="notas" placeholder="موضوع الرسالة" class="form-control"></textarea>
                        </li>
                        <li class="col-md-12">
                            <input type="submit" value="إرسال" class="btn" />
                        </li>
                    </ul>
                </form>                
            </div>
        </div>
    </div>
</div>
@stop