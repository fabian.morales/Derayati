@extends($lang.'.master')

@section('js_header')
<script>
    (function ($){
        $(document).ready(function() {
            $("#btn_filter").click(function() {
                var $url = '{{ url("/".$lang."/curso") }}?id_tema=' + $("select#id_tema").val();
                window.location.href = $url;                
            });
        });
    })(jQuery);
</script>
@stop

@section('content')
<!--======= RODUCTS / ITEMS =========-->
<section id="products" class="products">
    <div class="container"> 
        <!--======= TITTLE =========-->
        <div class="tittle">
            <h3>دوراتنا</h3>
          	<hr>
            <p> الحصول على المعلم في أي وقت في أي الرياضيات والعلوم والدراسات الاجتماعية وموضوع اللغة الإنجليزية. نحن تغطي العديد من الموضوعات. دورات متقدمة واختبار الإعدادية أيضا. </p>
            <hr>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <select id="id_tema">
                    <option value="">جميع المواد</option>
                    @foreach($temas as $t)
                    <option value="{{ $t->id }}" @if(!empty($idTema) && $t->id == $idTema) selected @endif>{{ $t->nombre_ar }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-sm-2">
                <button class="btn" id="btn_filter">مرشح</button>
            </div>
        </div>
        <hr />
        @include($lang.'.curso.listaCursos', array("cursos" => $cursos))
    </div>
</section>
@stop