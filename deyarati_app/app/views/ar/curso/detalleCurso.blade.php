@extends($lang.'.master')

@section('js_header')
<script>
    (function($) {
        $(document).ready(function() {
            $("#btn_rate").click(function(e) {
                e.preventDefault();
                var $url = "{{ url('/'.$lang.'/curso/rate') }}?id_curso={{ $curso->id }}&rate_profesor=" + $("#rate_profesor").val() + "&rate_curso=" + $("#rate_curso").val();
                window.location.href = $url;
            });
        });
    })(jQuery);
</script>
@stop

@section('content')                    
<div class="container">    
    <div class="contact-info">
        <div class="row">            
            <div class="col-sm-6">
                <h3>معلومات الحال</h3>
                <ul class="row lista_espacio">
                    @if (!Auth::check())
                    <li class="col-md-12"><span class="azul">إذا كنت تريد معرفة المزيد من المعلومات أو الحجز دراستك، يرجى<a href="{{ url('/'.$lang.'/sesion/formLogin') }}"> تسجيل</a></span></li>
                    @endif
                    <li class="col-md-12">
                        <strong>مدرس: </strong>{{ $curso->profesor->nombre }} {{ $curso->profesor->apellido }}
                        @include("rate", array("valor" => $curso->profesor->obtenerCalificacion()))
                    </li>
                    <li class="col-md-6">
                        <strong>موضوع:</strong> {{ $curso->curso->tema->nombre_ar }}
                    </li>                        
                    <li class="col-md-6">
                        <strong>اسم الدورة:</strong> {{ $curso->curso->nombre_ar }}
                        @include("rate", array("valor" => $curso->obtenerCalificacion()))
                    </li>
                    <li class="col-md-12">
                        <strong>اسم الدورة:</strong>
                        <br />
                        @if (Auth::check())
                        {{ $curso->descripcion }}
                        @else
                        {{ $curso->descripcion_corta }}
                        @endif
                        <hr />
                    </li>
                    <li class="col-md-6">                            
                        <strong>المستوى:</strong>
                        @if($curso->nivel == "B") أدنى @endif
                        @if($curso->nivel == "M") متوسط @endif
                        @if($curso->nivel == "A") ارتفاع @endif                           
                    </li>
                    <li class="col-md-6">
                        <strong>مدينة:</strong>{{ $curso->ciudadObj->nombre_ar }}
                    </li>
                    <li class="col-md-6">
                        <strong>مختلطة:</strong>                                
                        @if($curso->modalidad == "P") حجرة الدراسة @endif
                        @if($curso->modalidad == "V") افتراضية @endif
                        @if($curso->modalidad == "M") مدموج @endif
                        </select>
                    </li>
                    <li class="col-md-6">
                        <strong>اللغات:</strong> {{ $curso->idioma }}
                    </li>
                    @if (Auth::check())
                    <li class="col-md-12 box">
                        <h4>جدول:</h4>
                        <ul class="row">
                            <li class="col-md-12">الإثنين:
                                @if (!empty($curso->h_lunes) && trim($curso->h_lunes)) 
                                {{ $curso->h_lunes }}
                                @else
                                Off
                                @endif
                            </li>
                            <li class="col-md-12">الثلاثاء:
                                @if (!empty($curso->h_martes) && trim($curso->h_martes)) 
                                {{ $curso->h_martes }}
                                @else
                                Off
                                @endif
                            </li>
                            <li class="col-md-12">الاربعاء:
                                @if (!empty($curso->h_miercoles) && trim($curso->h_miercoles)) 
                                {{ $curso->h_miercoles }}
                                @else
                                Off
                                @endif
                            </li>
                            <li class="col-md-12">الخميس:
                                @if (!empty($curso->h_jueves) && trim($curso->h_jueves)) 
                                {{ $curso->h_jueves }}
                                @else
                                Off
                                @endif
                            </li>
                            <li class="col-md-12">الجمعة:
                                @if (!empty($curso->h_viernes) && trim($curso->h_viernes)) 
                                {{ $curso->h_viernes }}
                                @else
                                Off
                                @endif
                            </li>
                            <li class="col-md-12">السبت:
                                @if (!empty($curso->h_sabado) && trim($curso->h_sabado)) 
                                {{ $curso->h_sabado }}
                                @else
                                Off
                                @endif
                            </li>
                            <li class="col-md-12">الأحد:
                                @if (!empty($curso->h_domingo) && trim($curso->h_domingo)) 
                                {{ $curso->h_domingo }}
                                @else
                                Off
                                @endif
                            </li>
                        </ul>
                    </li>
                    @endif
                    <li class="col-sm-12 clearfix" style="height: 1px; margin: 0;">&nbsp;</li>
                    @if (Auth::check())
                    <li class="col-md-6">
                        @if (is_file(public_path("cursos")."/".$curso->id_profesor."/".$curso->id.".".$curso->certificado))
                        <a href="{{ url('/'.$lang.'/curso/certificado/'.$curso->id) }}"><i class="fa fa-download"></i>&nbsp; تحميل الشهادة</a>
                        @endif
                    </li>
                    @endif
                    <li class="col-md-6">
                        <strong>التكلفة لكل ساعة:</strong> {{ Lang::get("messages.moneda") }} {{ $curso->costo }}
                    </li>
                    @if (Auth::check())
                    <li class="col-md-12">
                        <p><strong>التعليق:</strong></p>
                        {{ $curso->notas }}                            
                    </li>
                    @endif
                </ul>
            </div>
            @if (Auth::check() && Auth::user()->tipo == "E" && $inscrito == "Y")
            <div class="col-md-3 col-sm-12">
                <h4>  قيم هذا بالطبع (1-5) </h4>
                <select id="rate_curso">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                </select>
                <h4>  تقييم المعلم (1-5)  </h4>
                <select id="rate_profesor">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                </select>
                <br />
                <br />
                <button class="btn pull-right" id="btn_rate">معدل </button>
            </div>
            @endif
        </div>
    </div>
</div>
@stop