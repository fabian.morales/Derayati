<div class="row">
    <div class="col-md-12">
        <ul class="row">
            @foreach($cursos as $c)
            <!--======= ITEM 1 =========-->
            <li class="col-sm-6 col-md-3">
                <div class="prodct"> 

                    <!--======= IMAGE =========-->                                 
                    <div class="pro-info"> 

                        <!--======= ITEM NAME / RATING =========-->
                        <div class="cate-name"> <span class="pull-left">{{ $c->curso->tema->nombre_ar }}</span>
                            @include("rate", array("valor" => $c->obtenerCalificacion()))
                        </div>

                        <!--======= ITEM Details =========--> 
                        <a href="{{ url('/'.$lang.'/curso/detalle/'.$c->id) }}">{{ $c->curso->nombre_ar }}</a>
                        <hr>
                        <span class="azul">مدرس:</span><span> {{ $c->profesor->nombre }} {{ $c->profesor->apellido }}</span>
                        <hr />
                        <span class="price">{{ Lang::get("messages.moneda") }} {{ $c->costo }}</span> 
                        @if(Auth::check())<a href="{{ url('/'.$lang.'/curso/inscribirse/'.$c->id) }}" class="btn">احجز الآن</a>@endif 
                        <a href="{{ url('/'.$lang.'/curso/detalle/'.$c->id) }}" class="btn">تفاصيل</a> </div>
                </div>
            </li>
            @endforeach
        </ul>
    </div>
</div>