@extends($lang.'.master')

@section('js_header')
<script>
(function ($){
    $(document).ready(function() {        
        
    });
})(jQuery);
</script>
@stop

@section('content')                    
<div class="container">    
    <div class="contact-info">
        <div class="row">            
            <div class="col-sm-6">
                <h3>معلومات الحال</h3>               
                <form role="form" id="curso_form" enctype="multipart/form-data" method="post" action="{{ url('/'.$lang.'/curso/guardarInscripcion') }}">
                    <input type="hidden" name="id_curso" value="{{ $curso->id }}" />
                    <input type="hidden" name="id_profesor_curso" value="{{ $curso->id_profesor }}" />
                    <ul class="row">
                        <li class="col-md-12">
                            <strong>مدرس: </strong>{{ $curso->profesor->nombre }} {{ $curso->profesor->apellido }}
                        </li>
                        <li class="col-md-6">
                            <strong>موضوع:</strong> {{ $curso->curso->tema->nombre_ar }}
                        </li>                        
                        <li class="col-md-6">
                            <strong>اسم الدورة:</strong> {{ $curso->curso->nombre_ar }}
                        </li>
                        <li class="col-md-12">
                            <strong>اسم الدورة:</strong>
                            <br />
                            {{ $curso->descripcion }}
                            <hr />
                        </li>                        
                        <li class="col-md-6">                            
                            <strong>المستوى:</strong>
                            @if($curso->nivel == "B") أدنى @endif
                            @if($curso->nivel == "M") متوسط @endif
                            @if($curso->nivel == "A") ارتفاع @endif                           
                        </li>
                        <li class="col-md-6">
                            <strong>مدينة:</strong> {{ $curso->ciudad }}
                        </li>
                        <li class="col-md-6">
                            <strong>مختلطة:</strong>                                
                            @if($curso->modalidad == "P") حجرة الدراسة @endif
                            @if($curso->modalidad == "V") افتراضية @endif
                            @if($curso->modalidad == "M") مدموج @endif
                            </select>
                        </li>
                        <li class="col-md-6">
                            <strong>اللغات:</strong> {{ $curso->idioma }}
                        </li>
                        <li class="col-md-12 box">
                             <h4>جدول:</h4>
                            <ul class="row">
                                <li class="col-md-9"><label for="h_lunes">الإثنين: </label> {{ $curso->h_lunes }}</li>
                                <li class="col-md-3">
                                    @if (!empty($curso->h_lunes) && trim($curso->h_lunes))
                                    <input type="checkbox" name="h_lunes" id="h_lunes" value="Y" />
                                    @else
                                    Off
                                    @endif
                                </li>
                                <li class="col-sm-12 clearfix" style="height: 1px; margin: 0;">&nbsp;</li>
                                <li class="col-md-9"><label for="h_martes">الثلاثاء: </label> {{ $curso->h_martes }}</li>
                                <li class="col-md-3">
                                    @if (!empty($curso->h_martes) && trim($curso->h_martes))
                                    <input type="checkbox" name="h_martes" id="h_martes" value="Y" />
                                    @else
                                    Off
                                    @endif
                                </li>
                                <li class="col-sm-12 clearfix" style="height: 1px; margin: 0;">&nbsp;</li>
                                <li class="col-md-9"><label for="h_martes">الاربعاء: </label> {{ $curso->h_miercoles }}</li>
                                <li class="col-md-3">
                                    @if (!empty($curso->h_miercoles) && trim($curso->h_miercoles))
                                    <input type="checkbox" name="h_miercoles" id="h_miercoles" value="Y" />
                                    @else
                                    Off
                                    @endif
                                </li>
                                <li class="col-sm-12 clearfix" style="height: 1px; margin: 0;">&nbsp;</li>
                                <li class="col-md-9"><label for="h_martes">الخميس: </label> {{ $curso->h_jueves }}</li>
                                <li class="col-md-3">
                                    @if (!empty($curso->h_jueves) && trim($curso->h_jueves))
                                    <input type="checkbox" name="h_jueves" id="h_jueves" value="Y" />
                                    @else
                                    Off
                                    @endif
                                </li>
                                <li class="col-sm-12 clearfix" style="height: 1px; margin: 0;">&nbsp;</li>
                                <li class="col-md-9"><label for="h_martes">الجمعة: </label> {{ $curso->h_viernes }}</li>
                                <li class="col-md-3">
                                    @if (!empty($curso->h_viernes) && trim($curso->h_viernes))
                                    <input type="checkbox" name="h_viernes" id="h_viernes" value="Y" />
                                    @else
                                    Off
                                    @endif
                                </li>
                                <li class="col-sm-12 clearfix" style="height: 1px; margin: 0;">&nbsp;</li>
                                <li class="col-md-9"><label for="h_martes">السبت: </label> {{ $curso->h_sabado }}</li>
                                <li class="col-md-3">
                                    @if (!empty($curso->h_sabado) && trim($curso->h_sabado))
                                    <input type="checkbox" name="h_sabado" id="h_sabado" value="Y" />
                                    @else
                                    Off
                                    @endif
                                </li>
                                <li class="col-sm-12 clearfix" style="height: 1px; margin: 0;">&nbsp;</li>
                                <li class="col-md-9"><label for="h_martes">الأحد: </label> {{ $curso->h_domingo }}</li>
                                <li class="col-md-3">
                                    @if (!empty($curso->h_domingo) && trim($curso->h_domingo))
                                    <input type="checkbox" name="h_domingo" id="h_domingo" value="Y" />
                                    @else
                                    Off
                                    @endif
                                </li>
                            </ul>
                        </li>
                        <li class="col-md-2">
                            <strong>ساعات العمل:</strong>
                        </li>
                        <li class="col-md-3">
                            <input type="text" class="form-control" id="horas" name="horas" />
                        </li>
                        <li class="col-sm-12 clearfix" style="height: 1px; margin: 0;">&nbsp;</li>
                        <li class="col-md-6">
                            @if (is_file(public_path("cursos")."/".$curso->id_profesor."/".$curso->id.".".$curso->certificado))
                            <a href="{{ url('/'.$lang.'/curso/certificado/'.$curso->id) }}"><i class="fa fa-download"></i>&nbsp; تحميل الشهادة</a>
                            @endif
                        </li>
                        <li class="col-md-6">
                            <strong>التكلفة لكل ساعة:</strong> {{ Lang::get("messages.moneda") }} {{ $curso->costo }}
                        </li>
                        <li class="col-md-12">
                            <p><strong>التعليق:</strong></p>
                            {{ $curso->notas }}                            
                        </li>
                        <li class="col-md-12">
                            <p><strong>تعليق ل المعلم:</strong></p>
                            <textarea class="form-control"
                                name="notas" id="notas" placeholder="Notes"></textarea>
                        </li>
                        
                        <li>
                            <button type="submit" value="submit" class="btn f_right" id="btn_inscribirse">كتاب</button>
                        </li>
                    </ul>
                </form>
            </div>
        </div>
    </div>
</div>
@stop