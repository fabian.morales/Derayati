@extends($lang.'.master')

@section('js_header')
<script>
    (function($) {
        $(document).ready(function() {
            $("#btn_rate").click(function(e) {
                e.preventDefault();
                var $url = "{{ url('/'.$lang.'/curso/rate') }}?id_curso={{ $curso->id }}&rate_profesor=" + $("#rate_profesor").val() + "&rate_curso=" + $("#rate_curso").val();
                window.location.href = $url;
            });
        });
    })(jQuery);
</script>
@stop

@section('content')                    
<div class="container">    
    <div class="contact-info">
        <div class="row">            
            <div class="col-sm-6">
                <h3>معلومات الحال</h3>
                <ul class="row lista_espacio">
                    <li class="col-md-6">
                        <strong>موضوع:</strong> {{ $curso->curso->tema->nombre_ar }}
                    </li>                        
                    <li class="col-md-6">
                        <strong>اسم الدورة:</strong> {{ $curso->curso->nombre_ar }}
                    </li>
                    <li class="col-md-12">
                        <strong>الوصف:</strong>
                        <br />
                        {{ $curso->descripcion }}
                        <hr />
                    </li>
                    <li class="col-md-6">                            
                        <strong>المستوى:</strong>
                        @if($curso->nivel == "B") أدنى @endif
                        @if($curso->nivel == "M") متوسط @endif
                        @if($curso->nivel == "A") ارتفاع @endif                           
                    </li>
                    <li class="col-md-6">
                        <strong>مدينة:</strong> {{ $curso->ciudadObj->nombre_ar }}
                    </li>
                    <li class="col-md-6">
                        <strong>مختلطة:</strong>                                 
                        @if($curso->modalidad == "P") حجرة الدراسة @endif
                        @if($curso->modalidad == "V") افتراضية @endif
                        @if($curso->modalidad == "M") مدموج @endif
                        </select>
                    </li>
                    <li class="col-md-6">
                        <strong>اللغات:</strong> {{ $curso->idioma }}
                    </li>
                    <li class="col-md-12 box">
                        <h4>Schedule</h4>
                        <ul class="row">
                            <li class="col-md-12">الإثنين:
                                @if (!empty($curso->h_lunes) && trim($curso->h_lunes) && $inscripcion->h_lunes == "Y") 
                                {{ $curso->h_lunes }}
                                @else
                                Off
                                @endif
                            </li>
                            <li class="col-md-12">الثلاثاء:
                                @if (!empty($curso->h_martes) && trim($curso->h_martes) && $inscripcion->h_martes == "Y") 
                                {{ $curso->h_martes }}
                                @else
                                Off
                                @endif
                            </li>
                            <li class="col-md-12">الاربعاء:
                                @if (!empty($curso->h_miercoles) && trim($curso->h_miercoles) && $inscripcion->h_miercoles == "Y") 
                                {{ $curso->h_miercoles }}
                                @else
                                Off
                                @endif
                            </li>
                            <li class="col-md-12">الخميس:
                                @if (!empty($curso->h_jueves) && trim($curso->h_jueves) && $inscripcion->h_jueves == "Y" && $inscripcion->h_jueves == "Y") 
                                {{ $curso->h_jueves }}
                                @else
                                Off
                                @endif
                            </li>
                            <li class="col-md-12">الجمعة:
                                @if (!empty($curso->h_viernes) && trim($curso->h_viernes) && $inscripcion->h_viernes == "Y") 
                                {{ $curso->h_viernes }}
                                @else
                                Off
                                @endif
                            </li>
                            <li class="col-md-12">السبت:
                                @if (!empty($curso->h_sabado) && trim($curso->h_sabado) && $inscripcion->h_sabado == "Y") 
                                {{ $curso->h_sabado }}
                                @else
                                Off
                                @endif
                            </li>
                            <li class="col-md-12">الأحد:
                                @if (!empty($curso->h_domingo) && trim($curso->h_domingo) && $inscripcion->h_domingo == "Y") 
                                {{ $curso->h_domingo }}
                                @else
                                Off
                                @endif
                            </li>
                        </ul>
                    </li>
                    <li class="col-md-6">
                        @if (is_file(public_path("cursos")."/".$curso->id_profesor."/".$curso->id.".".$curso->certificado))
                        <a href="{{ url('/'.$lang.'/curso/certificado/'.$curso->id) }}"><i class="fa fa-download"></i>&nbsp; تحميل الشهادة</a>
                        @endif
                    </li>            
                    <li class="col-md-6">
                        <strong>التكلفة لكل ساعة:</strong> {{ Lang::get("messages.moneda") }} {{ $curso->costo }}
                    </li>            
                    <li class="col-md-12">
                         <p><strong>التعليق:</strong></p>
                        {{ $curso->notas }}                            
                    </li>
                    <li class="col-md-12">
                        <p><strong>رسالة من الطالب:</strong></p>
                        {{ $inscripcion->notas }}                            
                    </li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-12">
                <h4>  قيم هذا بالطبع (1-5) </h4>
                <select id="rate_curso">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                </select>
                <h4>  تقييم المعلم (1-5)  </h4>
                <select id="rate_profesor">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                </select>
                <br />
                <br />
                <button class="btn pull-right" id="btn_rate"> معدل </button>
            </div>
        </div>
    </div>
</div>
@stop