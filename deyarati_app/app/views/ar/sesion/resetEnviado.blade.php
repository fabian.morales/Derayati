@extends($lang.'.master')

@section('js_header')
@stop

@section('content')                    
<div class="container">
    <div class="contact-info">
        <div class="row">استرجاع كلمة السر</h3>
            <div style="clear: both"></div>
            <br />
            <p>لقد أرسلنا رسالة إلى البريد الإلكتروني الخاص بك مع تعليمات لاستعادة كلمة السر الخاصة بك</p>            
        </div>
    </div>
</div>
@stop