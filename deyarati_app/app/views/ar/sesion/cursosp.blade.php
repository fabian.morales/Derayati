@extends($lang.'.master')

@section('js_header')
@stop

@section('content')                    
<div class="container">
    <div class="contact-info">
        <div class="row">
            <div class="col-md-12">
                <h3>موادي</h3>
                <br />
                <a class="btn" href="{{ url('/'.$lang.'/sesion/crearCurso') }}">إنشاء مسار جديد</a>
                <br />
                <br />
                <br />
            </div>
        </div>
        <div class="row">
            <div class="col-md-5"><strong>التخصص</strong></div>
            <div class="col-md-3"><strong>اسم المواد</strong></div>
            <div class="col-md-2"><strong>تحرير</strong></div>
            <div class="col-md-2"><strong>حذف</strong></div>
            <br />
            <br />
        </div>
        @foreach($cursos as $c)
        <div class="row">
            <div class="col-md-5">{{ $c->curso->tema->nombre_ar }}</div>
            <div class="col-md-3">{{ $c->curso->nombre_ar }}</div>
            <div class="col-md-2"><a href="{{ url('/'.$lang.'/sesion/editarCurso/'.$c->id) }}"><i class="fa fa-pencil"></i></a></div>
            <div class="col-md-2"><a href="{{ url('/'.$lang.'/sesion/borrarCurso/'.$c->id) }}"><i class="fa fa-remove"></i></a></div>
            <br />
            <br />
        </div>
        @endforeach
    </div>
</div>
@stop