@extends($lang.'.master')

@section('js_header')
<script>
(function ($){
    $(document).ready(function() {        
        $("select#pais_residencia").change(function(e){
            $.ajax({
                url: $(this).attr("data-url") + "/" + $(this).val(),
                success: function (json) {                    
                    var $select = $("select#ciudad");
                    $select.empty();
                    $select.append("<option>Select your city</option>");
                    $.each(json, function(i, o) {
                        $select.append("<option value='" + o.id + "'>" + o.nombre_ar + "</div>");
                    });
                }
            });
        });
        
        /*$("select#estado").change(function(e){
            $.ajax({
                url: $(this).attr("data-url") + "/" + $(this).val(),
                success: function (json) {                    
                    var $select = $("select#ciudad");
                    $select.empty();
                    $select.append("<option>Select your city</option>");
                    $.each(json, function(i, o) {
                        $select.append("<option value='" + o.id + "'>" + o.nombre_ar + "</div>");
                    });
                }
            });
        });*/
        
        $("select#tipo").change(function(e){
            if ($(this).val() === 'P'){
                $("#col_sexo").removeClass("hide");
                $("#col_nacionalidad").removeClass("hide");
                $("#col_profesion").removeClass("hide");
            }
            else{
                $("#col_sexo").addClass("hide");
                $("#col_nacionalidad").addClass("hide");
                $("#col_profesion").addClass("hide");
            }
        });
        
        $("#login_form").validate({
            submitHandler: function(form) {
                form.submit();
            }
        });
        
        $("#registro_form").validate({
            submitHandler: function(form) {
                form.submit();
            }
        });
    });
})(jQuery);
</script>
@stop

@section('content')                    
<div class="container">
    <!--======= CONTACT INFORMATION =========-->
    <div class="contact-info">
        <div class="row"> 

            <!--======= CONTACT FORM =========-->
            <div class="col-sm-6">
                <h3>تسجيل الدخول</h3>                
                <form role="form" id="login_form" method="post" action="{{ url('/'.$lang.'/sesion/login') }}">
                    <ul class="row">
                        <li class="col-md-6">
                            <input type="text" class="form-control"
                                   name="_login" id="_login" placeholder="اسم المستخدم"
                                   data-toggle="tooltip" title="مطلوب اسم المستخدم" required
                                   />
                        </li>
                        <li class="col-md-6">
                            <input type="password" class="form-control"
                                   name="_password" id="_password" placeholder="كلمة السر"
                                   data-toggle="tooltip" title="كلمة المرور مطلوبة" required
                                   />
                        </li>
                        <li class="col-md-12"><a href="{{ url('/'.$lang.'/sesion/password/requestReset') }}">نسيت كلمة السر؟</a></li>
                        <li class="col-md-12">
                            <button type="submit" value="submit" class="btn f_right" id="btn_submit_login">تسجيل الدخول</button>
                        </li>
                    </ul>
                </form>
            </div>
            <div class="col-sm-6">
                <h3>سجل</h3>                
                <form role="form" id="registro_form" method="post" action="{{ url('/'.$lang.'/sesion/registrar') }}">
                    <ul class="row">
                        <li class="col-md-12">
                            <select id="tipo" name="tipo" required>
                                <option value="">نوع المستخدم</option>
                                <option value="E">طالب</option>
                                <option value="P">مدرس</option>
                            </select>
                        </li>                        
                        <li class="col-md-6">
                            <input type="text" class="form-control"
                                   name="nombre" id="nombre" placeholder="الاسم الأول"
                                   data-toggle="tooltip" title="مطلوب اسم" required
                                   />
                        </li>
                        <li class="col-md-6">
                            <input type="text" class="form-control"
                                   name="apellido" id="apellido" placeholder="اسم العائلة"
                                   data-toggle="tooltip" title="مطلوب اسم العائلة" required
                                   />
                        </li>
                        <li class="col-md-6">
                            <input type="text" class="form-control"
                                   name="login" id="login" placeholder="اسم المستخدم"
                                   data-toggle="tooltip" title="مطلوب اسم المستخدم" required
                                   />
                        </li>
                        <li class="col-md-6">
                            <input type="password" class="form-control"
                                   name="password" id="clave" placeholder="كلمة السر"
                                   data-toggle="tooltip" title="كلمة المرور مطلوبة" required
                                   />
                        </li>
                        <li class="col-md-6">
                            <input type="text" class="form-control"
                                   name="email" id="email" placeholder="البريد الإلكتروني"
                                   data-toggle="tooltip" title="مطلوب البريد الإلكتروني" required
                                   />
                        </li>
                        <li class="col-md-6">
                            <input type="text" class="form-control"
                                   name="telefono" id="telefono" placeholder="رقم الهاتف"
                                   />
                        </li>
                        <li class="col-md-6">
                            <select name='pais_residencia' id='pais_residencia' data-url='{{ url('/ciudades') }}' required>
                                <option value="">اختر بلدك</option>
                                @foreach($paises as $p)
                                <option value='{{ $p->id }}'>{{ $p->nombre_ar }}</option>
                                @endforeach
                            </select>
                        </li>
                        <!--li class="col-md-6">
                            <select name='estado' id='estado' data-url='{{ url('/ciudades') }}'>
                                <option>اختر المدينة</option>                                
                            </select>
                        </li-->
                        <li class="col-md-6">
                            <select name='ciudad' id='ciudad' required>
                                <option value="">اختر المدينة</option>                                
                            </select>
                        </li>
                        <li class="col-md-6 hide" id='col_sexo'>
                            <select id='sexo' name='sexo' required>
                                <option value="">جنس</option>
                                <option value='M'>ذكر</option>
                                <option value='F'>أنثى</option>
                            </select>
                        </li>
                        <li class="col-md-6 hide" id='col_nacionalidad'>
                            <input type="text" class="form-control"
                                   name="nacionalidad" id="nacionalidad" placeholder="جنسية" required
                                   />
                        </li>
                        <li class="col-md-6 hide" id='col_profesion'>
                            <input type="text" class="form-control"
                                   name="profesion" id="profesion" placeholder="مهنة" required
                                   />
                        </li>
                        <li>
                            <button type="submit" value="submit" class="btn f_right" id="btn_registrar">سجل</button>
                        </li>
                    </ul>
                </form>
            </div>
        </div>
    </div>
</div>
@stop