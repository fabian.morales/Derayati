@extends($lang.'.master')

@section('js_header')
@stop

@section('content')                    
<div class="container">
    <div class="contact-info">
        <div class="row">
            <div class="col-md-6">
                <h3>Change your password</h3>
                <div style="clear: both"></div>
                <br />
                <p>Fill out the form below to change your password</p>
                <form method="post" action="{{ URL::to('/'.$lang.'/sesion/password/guardar', array($token)) }}">
                    <input type="hidden" name="token" value="{{ $token }}" />
                    <ul class="row">
                        <li class="col-md-3"><label class="inline" for="email">Email</label></li>
                        <li class="col-md-9"><input type="text" name="email" value="" class="form-control" /></li>
                        <li class="col-md-3"><label class="inline" for="password_confirmation">New password</label></li>
                        <li class="col-md-9"><input type="password" name="password" value="" class="form-control" /></li>
                        <li class="col-md-3"><label class="inline" for="password_confirmation">Confirm password</label></li>
                        <li class="col-md-9"><input type="password" name="password_confirmation" value="" class="form-control" /></li>
                        <li class="col-md-12"><input type="submit" value="Save" class="btn default" /></li>
                    </ul>
                </form>
            </div>
        </div>
    </div>
</div>
@stop