@extends($lang.'.master')

@section('js_header')
<script>
(function ($){
    $(document).ready(function() {        
        $("#btn_submit_lang").click(function(e) {
            e.preventDefault();
            var $idiomaEn = $('#idiomas option:selected').attr('data-en');
            var $idiomaAr = $('#idiomas option:selected').attr('data-ar');
            $.ajax({
                url: "{{ url($lang.'/sesion/adicionarIdioma') }}",
                method: 'post',
                data: {'idioma_ar': $idiomaEn, 'idioma_ar': $idiomaAr, 'id_profesor': '{{ $usuario->id }}'},
                success: function(){
                    window.location.reload();
                }
            });
        });
        
        $("#perfil_form, #clave_form, #form_certificado").validate({
            submitHandler: function(form) {
                form.submit();
            }
        });
    });
})(jQuery);
</script>
@stop

@section('content')

@if ($usuario->tipo == "E")
<div class="container">
    <div class="contact-info">
        <div class="row"> 
            <div class="col-sm-6">
                <h3>ملفي الشخصي </h3>
                <p class="azul"> {{ $usuario->nombre }} {{ $usuario->apellido }}</p>
                <p> </p>
                <p>تغيير كلمة المرور الخاصة بك</p>                
                <form role="form" id="clave_form" method="post" action="{{ url('/'.$lang.'/sesion/cambiarClave') }}">
                    <ul class="row">
                        <li class="col-md-6">
                            <input type="password" class="form-control"
                                   name="_password" id="_password" placeholder="كلمة السر"
                                   data-toggle="tooltip" title="كلمة المرور مطلوبة" required
                                   />
                        </li>                        
                        <li class="col-md-12">
                            <button type="submit" value="submit" class="btn f_right" id="btn_submit_clave">تغيير</button>
                        </li>
                    </ul>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6"><h3>الدروس</h3></div>
            @foreach($usuario->inscripciones as $i)
            <div class="col-md-6 box">
                <strong>{{ $i->curso->curso->tema->nombre_ar }}</strong> 
                <br /> 
                <a href="{{ url('/'.$lang.'/curso/inscripcion/'.$i->id) }}">{{ $i->curso->curso->nombre_ar }}</a>
                <br />
                مدرس:{{ $i->curso->profesor->nombre }} {{ $i->curso->profesor->apellido }}
            </div>
            @endforeach
        </div>
    </div>
</div>
@endif

@if ($usuario->tipo != 'E')
<div class="container">
    <div class="contact-info">
        <div class="row"> 
            <div class="col-sm-12 col-md-6">
                <h3>ملفي الشخصي </h3>
                <p class="azul"> {{ $usuario->nombre }} {{ $usuario->apellido }}</p>
                <p> </p>
                <form role="form" id="perfil_form" method="post" enctype="multipart/form-data" action="{{ url('/'.$lang.'/sesion/actualizar') }}">
                    <input type="hidden" name="login" value="{{ $usuario->login }}" />
                    <input type="hidden" name="ciudad" value="{{ $usuario->ciudad }}" />
                    <input type="hidden" name="tipo" value="{{ $usuario->tipo }}" />
                    <ul class="row">                        
                        <li class='col-md-5'>
                            @if (is_file(public_path('avatares/'.$usuario->id.'.jpg')))
                            <img class='img-responsive' src='{{ asset('avatares/'.$usuario->id.'.jpg') }}' />
                            @else
                            
                            @if ($usuario->sexo == 'M')
                            <img class='img-responsive' src='{{ asset('images/avatar_male.png') }}' />
                            @else
                            <img class='img-responsive' src='{{ asset('images/avatar_female.png') }}' />
                            @endif
                            
                            @endif
                        </li>
                        <li class='col-md-7'>
                            <p>تغيير الصورة</p>
                            <input type='file' name='avatar' value='Select your avatar' />
                        </li>
                    </ul>
                    <ul class="row">
                        <li class="col-md-6">
                            <p>الاسم الأول</p>
                            <input type="text" class="form-control"
                                   name="nombre" id="nombre" placeholder="الاسم الأول"
                                   data-toggle="tooltip" title="Name is required" value='{{ $usuario->nombre }}' required
                                   />
                        </li>
                        <li class="col-md-6">
                            <p>الاسم الأخير</p>
                            <input type="text" class="form-control"
                                   name="apellido" id="apellido" placeholder="الاسم الأخير"
                                   data-toggle="tooltip" title="Last Name is required" value='{{ $usuario->apellido }}' required
                                   />
                        </li>                        
                        <li class="col-md-6">
                            <p>كلمة السر</p>
                            <input type="password" class="form-control"
                                   name="password" id="clave" placeholder="كلمة السر"
                                   data-toggle="tooltip" title="Password is required" required
                                   />
                        </li>
                        <li class="col-md-6">
                            <p>البريد الإلكتروني</p>
                            <input type="email" class="form-control"
                                   name="email" id="email" placeholder="البريد الإلكتروني" value='{{ $usuario->email }}' required
                                   />
                        </li>
                        <li class="col-md-6">
                            <p>رقم الجوال</p>
                            <input type="text" class="form-control"
                                   name="telefono" id="telefono" placeholder="رقم الجوال" value='{{ $usuario->telefono }}' required
                                   />
                        </li>
                        <li class="col-md-12">
                            <p>Country</p>
                            <select name='pais_residencia' id='pais_residencia' required>
                                <option value="">مكان الإقامة</option>
                                @foreach($paises as $p)
                                <option value='{{ $p->id }}' @if($p->id == $usuario->pais_residencia) selected @endif>{{ $p->nombre_ar }}</option>
                                @endforeach
                            </select>
                        </li>                        
                        <li class="col-md-6" id='col_sexo'>
                            <p>جنس</p>
                            <select id='sexo' name='sexo' required>
                                <option value="">الجنس</option>
                                <option value='M' @if($usuario->sexo == "M") selected @endif>  ذكر </option>
                                <option value='F' @if($usuario->sexo == "F") selected @endif> أنثى </option>
                            </select>
                        </li>
                        <li class="col-md-6" id='col_nacionalidad'>
                            <p>الجنسية</p>
                            <input type="text" class="form-control"
                                   name="nacionalidad" id="nacionalidad" placeholder="الجنسية" value='{{ $usuario->nacionalidad }}' required
                                   />
                        </li>
                        <li class="col-md-12" id='col_profesion'>
                            <input type="text" class="form-control"
                                   name="profesion" id="profesion" placeholder="الخيرات" value='{{ $usuario->profesion }}' required
                                   />
                        </li>                        
                        <li class="col-md-12">
                            <p>تجربة</p>
                            <textarea class="form-control"
                                   name="experiencia" id="experiencia" placeholder="تجربة" required
                                   >{{ $usuario->experiencia }}</textarea>
                        </li>
                        <li class="col-md-12">
                            <button type="submit" value="submit" class="btn f_right" id="btn_submit_update">تحديث</button>
                        </li>
                    </ul>
                </form>
            </div>
            <div class="col-md-6 col-sm-12">                
                <a href="{{ url('/'.$lang.'/sesion/cursosp') }}" class="btn">موادي</a>
            </div>
        </div>
        <div class='row'>
            <div class="col-sm-12 col-md-6 box">
                <h3>اللغات</h3>
                <form>
                    <ul>
                        @foreach ($usuario->idiomas()->get() as $i)
                        <li>
                            <a href='{{ url($lang.'/sesion/quitarIdioma/'.$i->id) }}' title="Delete"><i class='fa fa-remove'></i><a>&nbsp;{{ $i->idioma_ar }}
                        </li>
                        @endforeach
                        <li>
                            <p>Add a new language</p>
                            <select name='idiomas' id='idiomas'>
                                <option data-en='English' data-ar='Ingles'>الإنجليزية</option>
                                <option data-en='Arabic' data-ar='Arabe'>العربية</option>
                            </select>
                        </li>
                        <li>
                            <button type="button" class="btn f_right" id="btn_submit_lang">إضافة</button>
                        </li>
                    </ul>
                </form>
            </div>
        </div>
        <hr />
        <div class='row'>
            <div class="col-sm-12 col-md-6 box">
                <h3>شهاداتي</h3>                
                <form id="form_certficado" name="form_certificado" enctype="multipart/form-data" method="post" action="{{ url($lang.'/sesion/adicionarCertificado') }}">
                    <ul>
                        @foreach ($usuario->certificados()->get() as $c)
                        <li>
                            <a href="{{ url($lang.'/sesion/certificado/'.$c->id) }}" title="Download"><i class='fa fa-download'></i>&nbsp;{{ $c->descripcion }}</a>
                        </li>
                        @endforeach
                        <li>
                            <p>Add a new certificate</p>
                            <input type='text' id='certificado' name='certificado' class="form-control" placeholder="اسم الشهادة" required />
                        </li>
                        <li>
                            <input type='file' id='archivo_cert' name='archivo_cert' required />
                        </li>                        
                        <li>
                            <input type="submit" class="btn f_right" id="btn_submit_cert" value="Add" />
                        </li>
                    </ul>
                </form>
            </div>
        </div>
    </div>
</div>
@endif
@stop