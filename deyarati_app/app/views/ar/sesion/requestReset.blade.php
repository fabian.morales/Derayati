@extends($lang.'.master')

@section('js_header')
@stop

@section('content')                    
<div class="container">
    <div class="contact-info">
        <div class="row">
            <div class="col-md-6">
                <h3>استرجاع كلمة السر</h3>
                <br />
                <p>ادخل بريدك الالكتروني</p>
                <form method="post" action="{{ url('/'.$lang.'/sesion/password/envioReset') }}">
                    <ul class="row">
                        <li class="col-md-3"><label class="inline" for="email">البريد الإلكتروني</label></li>
                        <li class="col-md-9"><input type="text" name="email" value="" class="form-control" /></li>
                        <li class="col-md-12"><input type="submit" value="Send" class="btn default" /></li>
                    </ul>
                </form>
            </div>
        </div>
    </div>
</div>
@stop