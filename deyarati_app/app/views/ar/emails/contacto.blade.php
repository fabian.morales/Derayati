<!DOCTYPE html>
<html lang="ar">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <table>
            <tr>
                <td><img src="{{ asset('/images/logo_home.png') }}" /></td>
                <td><h1>Derayati</h1></td>
            </tr>
        </table>
        <h2>Contact request</h2>
        <ul>
            <li><strong>اسم: </strong>{{ $nombre }}</li>
            <li><strong>موضوع: </strong>{{ $tema }}</li>
            <li><strong>البريد الإلكتروني: </strong>{{ $email }}</li>
            <li><strong>التعليقات:</strong>{{ $notas }}</li>
        </ul>
    </body>
</html>