<!DOCTYPE html>
<html lang="ar">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <table>
            <tr>
                <td><img src="{{ asset('/images/logo_home.png') }}" /></td>
                <td><h1>Derayati</h1></td>
            </tr>
        </table>
        <h2>إعادة ضبط كلمة المرور</h2>
        <div>
            لإعادة تعيين كلمة المرور الخاصة بك، واستكمال هذا النموذج: 
            <a href="{{ URL::to('/'.BaseController::$lang.'/sesion/password/reset', array($token)) }}">{{ URL::to('/'.BaseController::$lang.'/sesion/password/reset', array($token)) }}</a><br/>
            وهذا الرابط تنتهي في {{ Config::get('auth.reminder.expire', 60) }}  دقيقة.
        </div>
    </body>
</html>
