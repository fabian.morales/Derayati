<!DOCTYPE html>
<html lang="ar">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <table>
            <tr>
                <td><img src="{{ asset('/images/logo_home.png') }}" /></td>
                <td><h1>Derayati</h1></td>
            </tr>
        </table>
        <h2>حسابك الجديد</h2>
        <p>أهلا بك {{ $usuario->nombre }} في Derayati. توثيق البيانات الخاصة بك:
        <ul>
            <li><strong>اسم المستخدم:</strong>{{ $usuario->login }}</li>
            <li><strong>كلمة السر: </strong>{{ $clave }}</li>
        </ul>
    </body>
</html>