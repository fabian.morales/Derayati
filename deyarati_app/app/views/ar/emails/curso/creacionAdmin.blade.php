<!DOCTYPE html>
<html lang="ar">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <table>
            <tr>
                <td><img src="{{ asset('/images/logo_home.png') }}" /></td>
                <td><h1>Derayati</h1></td>
            </tr>
        </table>
        <h2>مسار جديد</h2>
        <p>المعلم <strong>{{ $curso->profesor->nombre }} {{ $curso->profesor->apellido }}</strong>
        خلق مسار جديد <strong>{{ $curso->curso->tema->nombre_ar }} - {{ $curso->curso->nombre_ar }}</strong>.
        من فضلك، التحقق من ذلك للموافقة عليه.</p>
    </body>
</html>