@extends($lang.'.master')

@section('js_header')
@stop

@section('content')                    
<div class="container">
    <div class="contact-info">
        <div class="row">
            <div class="col-md-12">
                <h3>Courses</h3>
                <a class="btn" href="{{ url('/'.$lang.'/admin/curso/crear') }}">Create new course</a>
                &nbsp;
                <a href="{{ url('/'.$lang.'/admin/curso/crear') }}" class="btn">New <i class="fa fa-plus"></i></a>
                <br />
                <br />
            </div>
        </div>
        <div class="row">
            <div class="col-md-3"><strong>Subject</strong></div>
            <div class="col-md-3"><strong>Course name</strong></div>
            <div class="col-md-3"><strong>Teacher</strong></div>
            <div class="col-md-3"><strong>Actions</strong></div>
            <br />
            <br />
        </div>
        @foreach($cursos as $c)
        <div class="row">
            <div class="col-md-3">{{ $c->curso->tema->nombre_ar }}</div>
            <div class="col-md-3">{{ $c->curso->nombre_ar }}</div>
            <div class="col-md-3">{{ $c->profesor->nombre }} {{ $c->profesor->apellido }}</div>
            <div class="col-md-3">
                <!-- Single button -->
                <div class="btn-group">
                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Actions <span class="caret"></span>
                  </button>
                  <ul class="dropdown-menu">
                    <li><a href="{{ url('/'.$lang.'/admin/curso/editar/'.$c->id) }}"><i class="fa fa-search"></i>&nbsp;View details</a></li>
                    @if (is_file(public_path("cursos")."/".$c->id_profesor."/".$c->id.".".$c->certificado))
                    <li><a href="{{ url('/'.$lang.'/curso/certificado/'.$c->id) }}"><i class="fa fa-download"></i>&nbsp;Download certificate</a></li>
                    @endif
                    <li><a href="{{ url('/'.$lang.'/admin/curso/aprobar/'.$c->id) }}"><i class="fa fa-check"></i>&nbsp;Approve</a></li>                    
                  </ul>
                </div>
            </div>
            <br />
            <br />
        </div>
        @endforeach
        <div class="row">
            <div class="col-sm-12 text-center">
                {{ $cursos->links() }}
            </div>
        </div>
    </div>
</div>
@stop