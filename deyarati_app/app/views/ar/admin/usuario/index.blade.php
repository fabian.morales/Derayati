@extends($lang.'.master')

@section('content')
@include($lang.'.admin.usuario.lista', array("usuarios" => $usuarios, "tipo" => $tipo, "lang" => $lang))
@stop