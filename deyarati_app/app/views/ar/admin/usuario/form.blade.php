@extends($lang.'.master')

@section('js_header')
<script>
(function ($){
    $(document).ready(function() {
        $("select#pais_residencia").change(function(e){
            $.ajax({
                url: $(this).attr("data-url") + "/" + $(this).val(),
                success: function (json) {                    
                    var $select = $("select#ciudad");
                    $select.empty();
                    $select.append("<option>Select the city</option>");
                    $.each(json, function(i, o) {
                        $select.append("<option value='" + o.id + "'>" + o.nombre_ar + "</div>");
                    });
                }
            });
        });
        
        $("#registro_form").validate({
            submitHandler: function(form) {
                form.submit();
            }
        });
        
        /*$("select#estado").change(function(e){
            $.ajax({
                url: $(this).attr("data-url") + "/" + $(this).val(),
                success: function (json) {                    
                    var $select = $("select#ciudad");
                    $select.empty();
                    $select.append("<option>Select your city</option>");
                    $.each(json, function(i, o) {
                        $select.append("<option value='" + o.id + "'>" + o.nombre_ar + "</div>");
                    });
                }
            });
        });*/
    });
})(jQuery);
</script>
@stop

@section('content')
{{--*/ $tiposE = array("A" => "administradores", "P" => "profesores", "E" => "estudiantes") /*--}}
<div class="container">
    <!--======= CONTACT INFORMATION =========-->
    <div class="contact-info">
        <div class="row"> 
            <div class="col-sm-6">
                <h3>User information</h3>
                <form role="form" id="registro_form" method="post" action="{{ url('/'.$lang.'/administradores/guardar') }}">
                    <input type="hidden" name="id" id="id" value="{{ $usuario->id }}" />
                    <input type="hidden" name="tipo" id="tipo" value="{{ $tipo }}" />
                    <ul class="row">                        
                        <li class="col-md-6">
                            <input type="text" class="form-control"
                                   name="nombre" id="nombre" placeholder="Name"
                                   data-toggle="tooltip" title="Name is required" value="{{ $usuario->nombre }}" required
                                   />
                        </li>
                        <li class="col-md-6">
                            <input type="text" class="form-control"
                                   name="apellido" id="apellido" placeholder="Last Name"
                                   data-toggle="tooltip" title="Last Name is required" value="{{ $usuario->apellido }}" required
                                   />
                        </li>
                        <li class="col-md-6">
                            <input type="text" class="form-control"
                                   name="login" id="login" placeholder="User Name"
                                   data-toggle="tooltip" title="User Name is required" value="{{ $usuario->login }}" required
                                   />
                        </li>
                        <li class="col-md-6">
                            <input type="password" class="form-control"
                                   name="password" id="clave" placeholder="Password"
                                   data-toggle="tooltip" title="Password is required"  required
                                   />
                        </li>
                        <li class="col-md-6">
                            <input type="email" class="form-control"
                                   name="email" id="email" placeholder="Email"
                                   data-toggle="tooltip" title="Email is required" value="{{ $usuario->email }}" required
                                   />
                        </li>
                        @if ($tipo != "A")
                        <li class="col-md-6">
                            <input type="text" class="form-control"
                                   name="telefono" id="telefono" placeholder="Phone number" value="{{ $usuario->telefono }}" required
                                   />
                        </li>
                        <li class="col-md-6">
                            <select name='pais_residencia' id='pais_residencia' data-url='{{ url('/ciudades') }}' required>
                                <option value="">Select the country</option>
                                @foreach($paises as $p)
                                <option value='{{ $p->id }}' @if($p->id == $usuario->pais_residencia) selected @endif>{{ $p->nombre_ar }}</option>
                                @endforeach
                            </select>
                        </li>
                        @if ($tipo == "E")
                        <li class="col-md-6">
                            <select name='ciudad' id='ciudad' required>
                                <option value="">Select the city</option>                                
                            </select>
                        </li>
                        @endif
                        
                        @endif
                        
                        @if ($tipo == "P")
                        <li class="col-md-6" id='col_sexo'>
                            <select id='sexo' name='sexo required'>
                                <option value="">Gender</option>
                                <option value='M'>Male</option>
                                <option value='F'>Female</option>
                            </select>
                        </li>
                        <li class="col-md-6" id='col_nacionalidad'>
                            <input type="text" class="form-control"
                                   name="nacionalidad" id="nacionalidad" placeholder="Nacionality" value="{{ $usuario->nacionalidad }}" required
                                   />
                        </li>
                        <li class="col-md-6" id='col_profesion'>
                            <input type="text" class="form-control"
                                   name="profesion" id="profesion" placeholder="Profession" value="{{ $usuario->profesion }}" required
                                   />
                        </li>
                        <li class="col-md-12">
                            <textarea class="form-control"
                                      name="experiencia" id="experiencia" placeholder="Experience" required>{{ $usuario->experiencia }}</textarea>
                        </li>
                        @endif
                        <li class="col-md-12">
                            <a href="{{ url('/'.$lang.'/'.$tiposE[$tipo]) }}" class="btn f_right">Cancel</a>
                            <button type="submit" value="submit" class="btn f_right" id="btn_registrar">Save</button>
                        </li>
                    </ul>
                </form>
            </div>
        </div>
    </div>
</div>
@stop