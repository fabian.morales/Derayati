{{--*/ $tipos = array("A" => "administrators", "P" => "teachers", "E" => "students") /*--}}
{{--*/ $tiposE = array("A" => "administradores", "P" => "profesores", "E" => "estudiantes") /*--}}
<div class="container">    
    <div class="contact-info">
        <div class="row">
            <div class="col-md-7">        
                <div class="row">
                    <div class="col-sm-12">
                        <a href="{{ url('/'.$lang.'/admin') }}" class="btn"><i class="fa fa-arrow-left"></i> Back</a>
                        &nbsp;
                        <a href="{{ url('/'.$lang.'/'.$tiposE[$tipo].'/crear') }}" class="btn">New <i class="fa fa-plus"></i></a>
                        <br />
                        <br />
                    </div>
                </div>
                <div class="row titulo lista">
                    <div class="col-sm-12"><h3>{{ ucfirst($tipos[$tipo]) }}</h3></div>
                </div>
                <div class="row item lista">
                    <div class="col-sm-2"><strong>#</strong></div>
                    <div class="col-sm-4"><strong>Name</strong></div>            
                    <div class="col-sm-4"><strong>Email</strong></div>
                    <div class="col-sm-2"><strong>Edit</strong></div>
                </div>
                @foreach($usuarios as $u)
                <div class="row item lista">
                    <div class="col-sm-2">{{ $u->id }}</div>
                    <div class="col-sm-4">{{ $u->nombre }}</div>            
                    <div class="col-sm-4">{{ $u->email }}</div>
                    <div class="col-sm-2"><a href="{{ url('/'.$lang.'/'.$tiposE[$tipo].'/editar/'.$u->id) }}"><i class="fa fa-pencil"></i></a></div>
                </div>                
                <!--a href="{{ url('/usuarios/permisos/'.$u->id) }}"><i class="fi-checkbox"></i></a-->
                @endforeach
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 text-center">
                {{ $usuarios->links() }}
            </div>
        </div>
    </div>
</div>