@extends($lang.'.master')

@section('js_header')
<script>
(function ($){
    function scrollTo(target) {
        var wheight = $(window).height() / 2;
        var ooo = $(target).offset().top - wheight;
        $('html, body').animate({scrollTop:ooo}, 600);
    }
    
    $(document).ready(function() {
        $("select#id_tema1, select#id_tema2").change(function(e){
            var $target = $(this).attr("data-target");
            $.ajax({
                url: $(this).attr("data-url") + "/" + $(this).val(),
                success: function (json) {                    
                    var $select = $("select" + $target);
                    $select.empty();
                    $select.append("<option>Course name</option>");
                    $.each(json, function(i, o) {
                        $select.append("<option value='" + o.id + "'>" + o.nombre_ar + "</div>");
                    });
                }
            });
        });
        
        $("#btn_buscar1, #btn_buscar2").click(function(e) {
            e.preventDefault();
            $('.work-in-progress').fadeIn(100);
            var $form = $($(this).attr("data-form"));
            $.ajax({
                url: $form.attr("action"),
                method: "post",
                data: $form.serialize(),
                success: function($html) {
                    $("#lista_cursos").html($html);
                    $('.work-in-progress').fadeOut(100);
                    scrollTo("#lista_cursos");
                }
            });
        });
    });
})(jQuery);
</script>
@stop

@section('content')
<!--======= BANNER =========-->
<div id="banner">
    <div class="flexslider">
        <ul class="slides">

            <!--======= SLIDE 1 =========-->
            <li>
                <img draggable="false" src="images/slide-1.jpg" alt="">
                <div class="container">
                    <div class="text-slider">
                        <div class="col-sm-7">
                            <h3> احفظ مالك واحجز درس <i class="fa fa-long-arrow-left"></i></h3>                            
                            <a class="btn" href="{{ url('/'.$lang.'/curso') }}">احجز درسك</a>
                        </div>
                        <div class="col-sm-5 col-md-4 pull-right hidden-xs">
                            <div class="find-drive sec-form">
                                <h5>البحث عن مادة <i class="fa fa-leanpub"></i></h5>
                                <div class="drive-form">
                                    <div class="intres-lesson"> 

                                        <!--======= FORM =========-->
                                        <form id="form_buscar1" method="post" action="{{ url('/'.$lang.'/buscar') }}">
                                            <ul class="row">
                                                <!--======= INPUT SELECT =========-->
                                                <li class="col-sm-6">
                                                    <div class="form-group">
                                                        <select id="tipo_curso" name="tipo_curso">
                                                            <option value="">نوع الدرس</option>
                                                            <option value="P"> قاعة دراسية </option>
                                                            <option value="V">قاعة افتراضية</option>
                                                            <option value="M">كلاهما</option>
                                                        </select>
                                                        <span class="fa fa-file-text-o"></span> 
                                                    </div>
                                                </li>
                                                
                                                <!--======= INPUT SELECT =========-->
                                                <li class="col-sm-6">
                                                    <div class="form-group">
                                                        <select name="id_ciudad">
                                                            <option value="">المدينة</option>
                                                            @foreach($ciudades as $c)
                                                            <option value="{{ $c->id }}">{{ $c->nombre_ar }}</option>
                                                            @endforeach
                                                        </select>
                                                        <span class="fa fa-building"></span> 
                                                    </div>
                                                </li>
                                                
                                                <!--======= INPUT SELECT =========-->
                                                <li class="col-sm-6">
                                                    <div class="form-group">
                                                        <select name="idioma">
                                                            <option value="">اللغة</option>
                                                            <option value="english">English</option>
                                                            <option value="arabic">العربية</option>
                                                        </select>
                                                        <span class="fa fa-language"></span> 
                                                    </div>
                                                </li>
                                                
                                                <!--======= INPUT SELECT =========-->
                                                <li class="col-sm-6">
                                                    <div class="form-group">
                                                        <select id="id_tema1" name="id_tema" data-target="#id_curso1" data-url="{{ url('/'.$lang.'/sesion/obtenerCursos') }}">
                                                            <option value="">تخصص المادة</option>
                                                            @foreach($temas as $t)
                                                            <option value="{{ $t->id }}">{{ $t->nombre_ar }}</option>
                                                            @endforeach
                                                        </select>
                                                        <span class="fa fa-file-text-o"></span> </div>
                                                </li>
                                                
                                                <li class="col-sm-12">
                                                    <div class="form-group">
                                                        <select id="id_curso1" name="id_curso">
                                                            <option value="">اسم الدورة التدريبية</option>
                                                        </select>
                                                        <span class="fa fa-file-text-o"></span> 
                                                    </div>
                                                </li>
                                            </ul>

                                            <!--======= BUTTON =========-->
                                            <div class="text-center">
                                                <button class="btn" id="btn_buscar1" data-form="#form_buscar1">ابحث عن درسي</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</div>
<div class="content">
    <!--======= RESPONSIVE FORM =========-->
    <div class="visible-xs" id="find-course">
        <div class="container">
            <div class="col-xs-12">
                <div class="find-drive">
                    <h5>البحث عن دورة<i class="fa fa-road"></i></h5>
                    <div class="drive-form">
                        <div class="intres-lesson"> 

                            <!--======= FORM =========-->
                            <form role="form" id="find_course" method="post" action="{{ url('/'.$lang.'/buscar') }}">
                                <ul class="row">
                                    <!--======= INPUT SELECT =========-->
                                    <li class="col-sm-6">
                                        <div class="form-group">
                                            <select id="tipo_curso" name="tipo_curso">
                                                <option value="">نوع الدورة</option>
                                                <option value="P"> حجرة الدراسة </option>
                                                <option value="V">افتراضية</option>
                                                <!--option value="M">سواء</option-->
                                            </select>
                                            <span class="fa fa-file-text-o"></span> 
                                        </div>                                        
                                    </li>
                                    
                                    <!--======= INPUT SELECT =========-->
                                    <li class="col-sm-6">
                                        <div class="form-group">
                                            <select name="id_ciudad">
                                                <option value="">مدينة</option>
                                                @foreach($ciudades as $c)
                                                <option value="{{ $c->id }}">{{ $c->nombre_ar }}</option>
                                                @endforeach
                                            </select>
                                            <span class="fa fa-building"></span> 
                                        </div>
                                    </li>
                                    
                                    <!--======= INPUT SELECT =========-->
                                    <li class="col-sm-6">
                                        <div class="form-group">
                                            <select name="idioma">
                                                <option value="">لغة</option>
                                                <option value="english">English</option>
                                                <option value="arabic">العربية</option>
                                            </select>
                                            <span class="fa fa-language"></span> 
                                        </div>
                                    </li>
                                    
                                    <li class="col-sm-6">
                                        <div class="form-group">
                                            <select id="id_tema2" name="id_tema" data-target="#id_curso2" data-url="{{ url('/'.$lang.'/sesion/obtenerCursos') }}">
                                                <option value="">بالطبع الموضوع</option>
                                                @foreach($temas as $t)
                                                <option value="{{ $t->id }}">{{ $t->nombre_ar }}</option>
                                                @endforeach
                                            </select>
                                            <span class="fa fa-file-text-o"></span> 
                                        </div>
                                    </li>
                                    
                                    <li class="col-sm-12">
                                        <div class="form-group">
                                            <select id="id_curso2" name="id_curso">
                                                <option value="">اسم الدورة التدريبية</option>
                                            </select>
                                            <span class="fa fa-file-text-o"></span> 
                                        </div>
                                    </li>
                                </ul>

                                <!--======= BUTTON =========-->
                                <div class="text-center">
                                    <button class="btn" id="btn_buscar1" data-form="#find_course">البحث عن ملعب بلدي</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!--======= RODUCTS / ITEMS =========-->
    <section id="products" class="products">
        <div class="container"> 
            <!--======= TITTLE =========-->
            <div class="tittle">
                <h3> موادنا </h3>
                <hr>
            </div>

            <!--======= PRODUCTS ROW =========-->
            <div id="lista_cursos">
                @include($lang.'.curso.listaCursos', array("cursos" => $cursos))            
                <div class="text-center margin-t-40"> <a href="{{ url('/'.$lang.'/curso') }}" class="btn"> شاهد جميع المواد</a> 
                </div>
            </div>
        </div>
    </section>    

    <!--======= NEWS / FAQS =========-->
    <section class="news faqs-with-bg" data-stellar-background-ratio="0.5">
        <div class="container"> 

            <!--======= TITTLE =========-->
            <div class="tittle">
                <h3> أسئلة شائعة</h3>
                <hr>
            </div>
            <div class="row"> 
                <!--======= ACCORDING =========-->
                <div class="col-sm-6">                    
                    <div class="panel-group" id="accordion">
                        <!--======= ACCORDING 1=========-->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"> كيف يعمل موقع درايتي؟ </a> </h4>
                            </div>

                            <!--======= ADD INFO HERE =========-->
                            <div id="collapseOne" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <p>  درايتي تعمل كوسيط بين الأستاذ الخصوصي والطالب. يستطيع الطالب حجز موعد مع أستاذ خصوصي لأي مادة من مواد التعليم العالي. </p>
                                </div>
                            </div>
                        </div>

                        <!--======= ACCORDING 2 =========-->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="collapsed"> بماذا ستتم خدمتي؟ </a> </h4>
                            </div>

                            <!--======= ADD INFO HERE =========-->
                            <div id="collapseTwo" class="panel-collapse collapse">
                                <div class="panel-body">
                                <p>  يقدم الأساتذة الخصوصيون المساعدة في الدراسة، التحضير لاختبار، والمساعدة في الواجبات. اطلع على <a href="{{ url('/'.$lang.'/curso') }}"> القائمة الكاملة للمواد المتاحة. </a> </p>
                                </div>
                            </div>
                        </div>

                        <!--======= ACCORDING 3 =========-->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="collapsed"> كيف يمكنني الحصول على مدرس خصوصي؟ </a> </h4>
                            </div>

                            <!--======= ADD INFO HERE =========-->
                            <div id="collapseThree" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p> يجب أولا أن يكون لديك حساب مفعل في موقع درايتي للاتصال بمدرس خصوصي. إذا كنت لا تملك حساب <a href="{{ url('/'.$lang.'/sesion/formLogin') }}"> اضغط هنا </a>  للبدء. إذا كان لديك حساب فعال، قم بتسجيل الدخول وحجز موعد. </p>
                                </div>
                            </div>
                        </div>

                        <!--======= ACCORDING 4 =========-->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapsefour" class="collapsed"> متى يكون الموقع متاح؟ </a> </h4>
                            </div>

                            <!--======= ADD INFO HERE =========-->
                            <div id="collapsefour" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p> الموقع متاح 24 ساعة في اليوم، 7 أيام في الأسبوع، 361 يوم في السنة.</p>
                                </div>
                            </div>
                        </div>

                                                            <!--======= ACCORDING 5 =========-->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse5" class="collapsed"> هل بإمكاني تقييم أستاذي الخصوصي؟ </a> </h4>
                            </div>

                            <!--======= ADD INFO HERE =========-->
                            <div id="collapse5" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p> نعم، بعد انتهاء الدرس يمكنك تقييم الأستاذ باختيار عدد النجوم المستحقة. </a></p>
                                </div>
                            </div>
                        </div>


                                                            <!--======= ACCORDING 6 =========-->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse6" class="collapsed"> ماذا لو لم أجد مدرس خصوصي لمادة ما؟ </a> </h4>
                            </div>

                            <!--======= ADD INFO HERE =========-->
                            <div id="collapse6" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p> إذا لم تجد مدرس لمادة معينة، نرجو التواصل معنا وسنعمل جاهدين لإضافة المادة ضمن <a href="{{ url('/'.$lang.'/curso') }}"> قائمة المواد </a> الخاصة بموقع درايتي وإيجاد مدرس خصوصي للمادة لإرضاء توقعاتك.</a></p>
                                </div>
                            </div>
                        </div>

                                                    <!--======= ACCORDING 7 =========-->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse7" class="collapsed"> هل بإمكاني جدولة جلسة مع أستاذ خصوصي؟ </a> </h4>
                            </div>

                            <!--======= ADD INFO HERE =========-->
                            <div id="collapse7" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p> بإمكان الطلبة جدولة موعد مع أستاذ معين. لإيجاد مدرس خصوصي لمادة ما:  <a href="{{ url('/'.$lang.'/sesion/formLogin') }}"> قم بتسجيل الدخول </a> ثم اكتب اسم المادة وقم باختيار الأستاذ المناسب من نتائج البحث. </a></p>
                                </div>
                            </div>
                        </div>

                        <!--======= ACCORDING 5 =========-->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse8" class="collapsed"> كيف يمكنني الاتصال بخدمة العملاء؟ </a> </h4>
                            </div>

                            <!--======= ADD INFO HERE =========-->
                            <div id="collapse8" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p> تفضل بإرسال بريد إلكتروني لـ(info@Derayati.com) سنجيب خلال فترة العمل الأحد- الخميس: 5م-10م الجمعة-السبت: 9ص-6م بتوقيت السعودية </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!--======= NEWS ARTICALS =========-->
                <div class="col-md-6"> </div>
            </div>
        </div>
    </section>
</div>
@stop