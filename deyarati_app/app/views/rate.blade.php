<ul class="my stars pull-right">
@for ($i = 1; $i <= $valor; $i++)
    <li><i class="fa fa-star"></i></li>                                
@endfor

@if ($valor < 5)
@for ($i = 1; $i <= 5 - $valor; $i++)
<li class="no-rate"><i class="fa fa-star"></i></li>
@endfor
@endif
</ul>