<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function() {
    return Redirect::to("/en/");
});

$appRoutes = function(){
    Route::get('/', 'HomeController@mostrarIndex');    
    Route::get('/acerca', 'HomeController@mostrarAcerca');
    Route::get('/contacto', 'HomeController@mostrarContacto');
    Route::post('/contacto/enviar', 'HomeController@enviarContacto');
    Route::post('/buscar', 'HomeController@realizarBusqueda');
    
    Route::get('/sesion/formLogin', 'SesionController@mostrarIndex');
    Route::post('/sesion/login', 'SesionController@hacerLogin');    
    Route::get('/sesion/logout', 'SesionController@hacerLogout');    
    Route::post('/sesion/registrar', 'SesionController@guardarUsuario');    
    Route::get('/sesion/perfil', 'SesionController@mostrarPerfil');    
    Route::post('/sesion/actualizar', 'SesionController@actualizarPerfil');    
    Route::post('/sesion/cambiarClave', 'SesionController@cambiarClave');    
    Route::post('/sesion/adicionarIdioma', 'SesionController@adicionarIdioma');
    Route::get('/sesion/quitarIdioma/{id}', 'SesionController@quitarIdioma');
    Route::post('/sesion/adicionarCertificado', 'SesionController@adicionarCertificado');
    Route::get('/sesion/certificado/{id}', 'SesionController@verCertificado');
    Route::get('/sesion/cursosp', 'SesionController@mostrarCursosProfesor');
    Route::get('/sesion/crearCurso', 'SesionController@crearCurso');
    Route::get('/sesion/editarCurso/{id}', 'SesionController@editarCurso');
    Route::get('/sesion/obtenerCursos/{idTema}', 'SesionController@obtenerCursos');
    Route::post('/sesion/guardarCurso', 'SesionController@guardarCurso');
    Route::get('/sesion/password/requestReset', 'SesionController@requestReset');
    Route::post('/sesion/password/envioReset', 'SesionController@envioReset');
    Route::get('/sesion/password/reset/{token}', 'SesionController@passwordReset');
    Route::post('/sesion/password/guardar/{token}', 'SesionController@guardarReset');
    
    Route::get('/curso', 'CursoController@mostrarIndex');    
    Route::get('/curso/detalle/{idCurso}', 'CursoController@verDetalle');    
    Route::get('/curso/inscribirse/{idCurso}', 'CursoController@mostrarFormInscripcion');    
    Route::get('/curso/inscripcion/{id}', 'CursoController@verDetalleInscripcion');    
    Route::get('/curso/certificado/{id}', 'CursoController@verCertificado');    
    Route::post('/curso/guardarInscripcion', 'CursoController@guardarInscripcion');
    Route::get('/curso/rate', 'CursoController@guardarRate');
    
    Route::get('/mails/{idCurso}', 'HomeController@probarMails');
        
    Route::get('/profesor', 'ProfesorController@mostrarIndex');
    Route::get('/profesor/detalle/{id}', 'ProfesorController@verDetalle');
    Route::get('/profesor/certificado/{id}', 'ProfesorController@verCertificado');
    
    Route::get('/admin', 'UsuarioController@mostrarIndex');
    Route::get('/administradores', 'UsuarioController@mostrarListaAdmin');
    Route::get('/administradores/crear', 'UsuarioController@crearAdmin');
    Route::get('/administradores/editar/{id}', 'UsuarioController@editarAdmin');
    Route::post('/administradores/guardar', 'UsuarioController@guardarAdmin');
    
    Route::get('/profesores', 'UsuarioController@mostrarListaProfesor');
    Route::get('/profesores/crear', 'UsuarioController@crearProfesor');
    Route::get('/profesores/editar/{id}', 'UsuarioController@editarProfesor');
    Route::post('/profesores/guardar', 'UsuarioController@guardarProfesor');
    
    Route::get('/estudiantes', 'UsuarioController@mostrarListaEstudiante');
    Route::get('/estudiantes/crear', 'UsuarioController@crearEstudiante');
    Route::get('/estudiantes/editar/{id}', 'UsuarioController@editarEstudiante');
    Route::post('/estudiantes/guardar', 'UsuarioController@guardarEstudiante');
    
    Route::get('/admin/curso', 'AdminCursoController@mostrarListaCursos');
    Route::get('/admin/curso/crear', 'AdminCursoController@crearCurso');
    Route::get('/admin/curso/editar/{id}', 'AdminCursoController@editarCurso');
    Route::post('/admin/curso/guardar', 'AdminCursoController@guardarCurso');
    Route::get('/admin/curso/pendientes', 'AdminCursoController@mostrarNoAprobados');
    Route::get('/admin/curso/aprobar/{id}', 'AdminCursoController@aprobarCurso');
    Route::get('/admin/curso/desaprobar/{id}', 'AdminCursoController@desaprobarCurso');
};

Route::group(array('prefix' => 'en', 'where' ), function() use($appRoutes)
{        
    $appRoutes();
});

Route::group(array('prefix' => 'ar'), function() use($appRoutes)
{    
    $appRoutes();
});

Route::get('/estados/{idPais}', 'BaseController@obtenerEstados');
Route::get('/ciudades/{idEstado}', 'BaseController@obtenerCiudades');

/*Route::get('/productos/', 'ProductoController@mostrarIndex');
Route::get('/productos/crear', 'ProductoController@crearProducto');
Route::get('/productos/editar/{id}', 'ProductoController@editarProducto');*/

Route::get('/crearAdmin', function() { 
    $usuario = new User();
    $usuario->nombre = "Admin";
    $usuario->login = "admin";
    $usuario->password = Hash::make("Ev4nerv");
    $usuario->tipo = "A";
    $usuario->save();
});

Route::get('/emails/{id}', function($id) {
        $insc = CursoAlumno::find($id);
        $curso = CursoProfesor::where("id", $insc->id_curso)->with(array("curso.tema", "profesor"))->first();
        $alumno = User::find($insc->id_alumno);
        $lang = !empty(BaseController::$lang) ? BaseController::$lang : "en";
        App::setLocale($lang);
        $datos = array("curso" => $curso, "alumno" => $alumno, "inscripcion" => $insc, "lang" => $lang);

        //Mail al alumno
        Mail::send($lang.'.emails.curso.inscripcionEstudiante', $datos, function($message) use ($alumno){
            if (Config::get("app.activarCopiaAdmin") == "Y"){
                $message->bcc(Config::get("app.correoAdmin"), "Administrator");   
            }
            $message->bcc("desarrollo@encubo.ws", "Desarrollo");
            $message->to($alumno->email, $alumno->nombre." ".$alumno->apellido)->subject(Lang::get("messages.asuntoMailInscripcionCurso"));
        });

        //Mail al profesor
        Mail::send($lang.'.emails.curso.inscripcionProfesor', $datos, function($message) use ($curso){
            if (Config::get("app.activarCopiaAdmin") == "Y"){
                $message->bcc(Config::get("app.correoAdmin"), "Administrator");   
            }
            $message->bcc("desarrollo@encubo.ws", "Desarrollo");
            $message->to($curso->profesor->email, $curso->profesor->nombre." ".$curso->profesor->apellido)->subject(Lang::get("messages.asuntoMailInscripcionCurso"));
        });

        //Mail al adinistrador
        Mail::send($lang.'.emails.curso.inscripcionAdmin', $datos, function($message){
            if (Config::get("app.activarCopiaAdmin") == "Y"){
                $message->bcc(Config::get("app.correoAdmin"), "Administrator");   
            }
            $message->bcc("desarrollo@encubo.ws", "Desarrollo");
            $message->to("desarrollo@encubo.ws", "Administrator")->subject(Lang::get("messages.asuntoMailInscripcionCurso"));
        });
});

