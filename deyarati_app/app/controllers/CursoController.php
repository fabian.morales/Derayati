<?php

class CursoController extends BaseController {

    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |	Route::get('/', 'HomeController@showWelcome');
    |
    */

    public function mostrarIndex(){
        $idTema = Input::get("id_tema");
        $cursos = CursoProfesor::whereHas("curso.tema", function($query) use ($idTema) {
            if (!empty($idTema)){
                $query->where("id", $idTema);
            }
        })->with(array("curso.tema", "profesor"))->orderBy("created_at", "desc")->get();
        $temas = Tema::all();
        return $this->make('curso.index', array("cursos" => $cursos, "temas"=>$temas, "idTema" => $idTema));
    }
    
    public function verDetalle($idCurso){
        $curso = CursoProfesor::where("id", $idCurso)->with(array("curso.tema", "profesor", "ciudadObj"))->first();
        
        if (!sizeof($curso)){
            return $this->redirectTo("/")->with("mensajeError", Lang::get('messages.errorNoEncontrado'));    
        }
        
        if ($curso->estado != "A"){
            return $this->redirectTo("/")->with("mensajeError", Lang::get('messages.errorCursoNoAutorizado'));
        }
        
        $inscrito = "N";
        if (Auth::check() && Auth::user()->tipo == "E"){
            $cnt = CursoAlumno::where("id_curso", $idCurso)->where("id_alumno", Auth::user()->id)->count();
            if ($cnt > 0){
                $inscrito = "Y";
            }
        }
        
        return $this->make('curso.detalleCurso', array("curso" => $curso, "inscrito" => $inscrito));
    }    
    
    public function mostrarFormInscripcion($idCurso){
        if (!Auth::check()){
            return $this->redirectTo("/sesion/formLogin")->with("mensajeError", Lang::get('messages.errorSesionNoIniciada'));
        }
        
        if (Auth::user()->tipo != "E"){
            return $this->redirectTo("/")->with("mensajeError", Lang::get('messages.errorNoAutorizado'));
        }
        
        $curso = CursoProfesor::where("id", $idCurso)->with(array("curso.tema", "profesor", "ciudadObj"))->first();
        
        if (!sizeof($curso)){
            return $this->redirectTo("/")->with("mensajeError", Lang::get('messages.errorNoEncontrado'));    
        }
        
        if ($curso->estado != "A"){
            return $this->redirectTo("/")->with("mensajeError", Lang::get('messages.errorCursoNoAutorizado'));    
        }
        
        return $this->make('curso.inscripcion', array("curso" => $curso));
    }
    
    public function verCertificado($id){
        if (!Auth::check()){
            return $this->redirectTo("/sesion/formLogin")->with("mensajeError", Lang::get('messages.errorSesionNoIniciada'));
        }      
        
        $curso = CursoProfesor::where("id", $id)->with("curso.tema")->first();
        
        if (!sizeof($curso)){
            return $this->redirectTo("/sesion/formLogin")->with("mensajeError", Lang::get('messages.errorNoEncontrado'));
        }
        
        $dir = public_path('cursos/'.$curso->id_profesor);
        $nombreArchivo = $curso->id.".".$curso->certificado;        
        return Response::download($dir.'/'.$nombreArchivo, urlencode($curso->curso->tema->nombre_en.".".$curso->certificado));
    }
    
    public function guardarInscripcion(){
        if (!Auth::check()){
            return $this->redirectTo("/sesion/formLogin")->with("mensajeError", Lang::get('messages.errorSesionNoIniciada'));
        }
        
        if (Auth::user()->tipo != "E"){
            return $this->redirectTo("/")->with("mensajeError", Lang::get('messages.errorNoAutorizado'));
        }
        
        $idCurso = Input::get("id_curso");
        $inscCnt = CursoAlumno::where("id_alumno", Auth::user()->id)->where("id_curso", $idCurso)->count();
        if ($inscCnt > 0){
            return $this->redirectTo("/")->with("mensajeError", Lang::get('messages.errorCursoYaInscrito'));
        }
        
        $insc = new CursoAlumno();
        $insc->fill(Input::all());
        
        $insc->id_alumno = Auth::user()->id;
        $insc->h_lunes = !empty($insc->h_lunes) && trim($insc->h_lunes) ? "Y" : "N";
        $insc->h_martes = !empty($insc->h_martes) && trim($insc->h_martes) ? "Y" : "N";
        $insc->h_miercoles = !empty($insc->h_miercoles) && trim($insc->h_miercoles) ? "Y" : "N";
        $insc->h_jueves = !empty($insc->h_jueves) && trim($insc->h_jueves) ? "Y" : "N";
        $insc->h_viernes = !empty($insc->h_viernes) && trim($insc->h_viernes) ? "Y" : "N";
        $insc->h_sabado = !empty($insc->h_sabado) && trim($insc->h_sabado) ? "Y" : "N";
        $insc->h_domingo = !empty($insc->h_domingo) && trim($insc->h_domingo) ? "Y" : "N";
        
        if ($insc->save()){
            $curso = CursoProfesor::where("id", $idCurso)->with(array("curso.tema", "profesor", "ciudadObj"))->first();
            $alumno = Auth::user();
            $datos = array("curso" => $curso, "alumno" => $alumno, "inscripcion" => $insc, "lang" => BaseController::$lang);
            
            //Mail al alumno
            Mail::send(BaseController::$lang.'.emails.curso.inscripcionEstudiante', $datos, function($message) use ($alumno){
                if (Config::get("app.activarCopiaAdmin") === "Y"){
                    $message->bcc(Config::get("app.correoAdmin"), "Administrator");   
                }
                $message->bcc("desarrollo@encubo.ws", "Desarrollo");
                $message->to($alumno->email, $alumno->nombre." ".$alumno->apellido)->subject(Lang::get("messages.asuntoMailInscripcionCurso"));
            });
            
            //Mail al profesor
            Mail::send(BaseController::$lang.'.emails.curso.inscripcionProfesor', $datos, function($message) use ($curso){
                if (Config::get("app.activarCopiaAdmin") === "Y"){
                    $message->bcc(Config::get("app.correoAdmin"), "Administrator");   
                }
                $message->bcc("desarrollo@encubo.ws", "Desarrollo");
                $message->to($curso->profesor->email, $curso->profesor->nombre." ".$curso->profesor->apellido)->subject(Lang::get("messages.asuntoMailInscripcionCurso"));
            });
            
            //Mail al adinistrador
            Mail::send(BaseController::$lang.'.emails.curso.inscripcionAdmin', $datos, function($message){
                if (Config::get("app.activarCopiaAdmin") === "Y"){
                    $message->bcc(Config::get("app.correoAdmin"), "Administrator");   
                }
                $message->bcc("desarrollo@encubo.ws", "Desarrollo");
                $message->to("desarrollo@encubo.ws", "Administrator")->subject(Lang::get("messages.asuntoMailInscripcionCurso"));
            });
            
            return $this->redirectTo("/sesion/perfil")->with("mensaje", Lang::get('messages.inscritoCurso'));
        }
        else{
            return $this->redirectTo("/sesion/perfil")->with("mensajeError", Lang::get('messages.errorInscritoCurso'));
        }
    }
    
    public function verDetalleInscripcion($id){
        if (!Auth::check()){
            return $this->redirectTo("/sesion/formLogin")->with("mensajeError", Lang::get('messages.errorSesionNoIniciada'));
        }
        
        if (Auth::user()->tipo != "E"){
            return $this->redirectTo("/")->with("mensajeError", Lang::get('messages.errorNoAutorizado'));
        }
        
        $inscripcion = CursoAlumno::find($id);
        if (!sizeof($inscripcion)){
            return $this->redirectTo("/")->with("mensajeError", Lang::get('messages.errorNoEncontrado'));
        }
        
        if ($inscripcion->id_alumno != Auth::user()->id){
            return $this->redirectTo("/")->with("mensajeError", Lang::get('messages.errorUsuarioNoCorresp'));
        }
        
        $curso = CursoProfesor::where("id", $inscripcion->id_curso)->with(array("curso.tema", "profesor", "ciudadObj"))->first();
        if (!sizeof($curso)){
            return $this->redirectTo("/")->with("mensajeError", Lang::get('messages.errorNoEncontrado'));    
        }
        
        if ($curso->estado != "A"){
            return $this->redirectTo("/")->with("mensajeError", Lang::get('messages.errorCursoNoAutorizado'));
        }        
        
        return $this->make('curso.detalleInscripcion', array("curso" => $curso, "inscripcion" => $inscripcion));
    } 
    
    public function guardarRate(){
        $idCurso = Input::get("id_curso");
        $rateProfesor = Input::get("rate_profesor");
        $rateCurso = Input::get("rate_curso");
        
        if (!Auth::check()){
            return $this->redirectTo("/sesion/formLogin")->with("mensajeError", Lang::get('messages.errorSesionNoIniciada'));
        }
        
        if (Auth::user()->tipo != "E"){
            return $this->redirectTo("/")->with("mensajeError", Lang::get('messages.errorNoAutorizado'));
        }
        
        $curso = CursoProfesor::find($idCurso);
        if (!sizeof($curso)){
            return $this->redirectTo("/")->with("mensajeError", Lang::get('messages.errorNoEncontrado'));
        }
        
        $inscr = CursoAlumno::where("id_curso", $idCurso)->where("id_alumno", Auth::user()->id)->first();
        if (!sizeof($inscr)){
            return $this->redirectTo("/")->with("mensajeError", Lang::get('messages.errorNoInscrito'));
        }
        
        $rate = Calificacion::where("id_alumno",  Auth::user()->id)->where("id_profesor", $curso->id_profesor)->first();
        if (!sizeof($rate)){
            $rate = new Calificacion();
            $rate->id_profesor = $curso->id_profesor;
            $rate->id_alumno = Auth::user()->id;
        }
        
        $rate->calificacion = $rateProfesor;
        $rate->save();
        
        $inscr->calificacion = $rateCurso;
        $inscr->save();
        
        return $this->redirectTo("/curso/detalle/".$idCurso);
    }
}
