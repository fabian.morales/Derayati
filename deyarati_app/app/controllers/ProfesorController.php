<?php

class ProfesorController extends BaseController {

    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |	Route::get('/', 'HomeController@showWelcome');
    |
    */

    public function mostrarIndex(){
        $profesores = User::where("tipo", "P");
        $idTema = Input::get("id_tema");
        $nombre = Input::get("nombre");
        $apellido = Input::get("apellido");
        
        if ($idTema){
            $profesores = $profesores->whereHas("cursos.curso.tema", function($query) use ($idTema) {
               $query->where("id", $idTema);
            });
        }
        
        if ($nombre){
            $profesores = $profesores->where("nombre", "like", "%".$nombre."%");
        }
        
        if ($apellido){
            $profesores = $profesores->where("apellido", "like", "%".$apellido."%");
        }
        
        $profesores = $profesores->paginate(20);
        
        $temas = Tema::all();
        
        return $this->make('profesor.lista', array("profesores" => $profesores, "temas" => $temas, "idTema" => $idTema, "nombre" => $nombre, "apellido" => $apellido));
    }
    
    public function verDetalle($id){
        $profesor = User::where("id", $id)->where("tipo", "P")->with(array("pais", "certificados", "cursos.profesor", "cursos.curso.tema"))->first();
        
        if (!sizeof($profesor)){
            return $this->redirectTo("/profesor")->with("mensajeError", Lang::get('messages.errorNoEncontrado'));    
        }        
        
        return $this->make('profesor.detalle', array("profesor" => $profesor));
    }
    
    public function verCertificado($id){
        if (!Auth::check()){
            return $this->redirectTo("/profesor")->with("mensajeError", Lang::get('messages.errorSesionNoIniciada'));
        }
        
        $certificado = Certificado::find($id);
        
        if (!sizeof($certificado)){
            return $this->redirectTo("/profesor")->with("mensajeError", Lang::get('messages.errorNoEncontrado'));
        }
        
        $dir = public_path('certificados/'.Auth::user()->id);
        $nombreArchivo = $certificado->id.".".$certificado->extension;        
        return Response::download($dir.'/'.$nombreArchivo, urlencode($certificado->descripcion.".".$certificado->extension));
    }
}
