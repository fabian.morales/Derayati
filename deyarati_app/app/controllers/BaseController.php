<?php

class BaseController extends Controller {

    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    public static $lang;
    
    function __construct() {
        $this->beforeFilter(function() {
            BaseController::$lang = Route::current()->getPrefix();
            App::setLocale(Route::current()->getPrefix());
        });
    }
    
    public function make($template, $valores = array()){
        $valores["lang"] = BaseController::$lang;
        return View::make(BaseController::$lang.'.'.$template, $valores);
    }
    
    public function redirectTo($ruta){        
        return Redirect::to('/'.BaseController::$lang.$ruta);
    }
    
    public function obtenerEstados($idPais){
        $pais = Pais::find($idPais);
        $estados = array();
        if (sizeof($pais)){
            $estados = $pais->estados()->get();
        }
        
        return Response::json($estados);
    }
    
    public function obtenerCiudades($idEstado){
        $pais = Pais::find($idEstado);
        $ciudades = array();
        if (sizeof($pais)){
            $ciudades = $pais->ciudades()->get();
        }
        
        return Response::json($ciudades);
    }
    
    public function obtenerCursos($idTema){
        $tema = Tema::find($idTema);
        $cursos = array();
        if (sizeof($tema)){
            $cursos = $tema->cursos()->get();
        }
        
        return Response::json($cursos);
    }
    
    protected function setupLayout()
    {
        if ( ! is_null($this->layout))
        {
            $this->layout = View::make($this->layout);
        }
    }

}
