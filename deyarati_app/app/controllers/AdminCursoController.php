<?php

class AdminCursoController extends AdminController {   
    public function mostrarListaCursos(){
        $cursos = CursoProfesor::with(array("curso.tema", "profesor"))->paginate(50);
        return $this->make('admin.curso.lista', array("cursos" => $cursos));
    }
    
    public function mostrarFormCurso($curso){       
        if (!sizeof($curso)){
            $curso = new CursoProfesor();
        }
        
        $profesores = User::where("tipo", "P")->get();
        $ciudades = Ciudad::with("pais")->get();
        $temas = Tema::all();
        $tema = array();        
        if ($curso->id_curso){
            $_curso = Curso::find($curso->id_curso);
            $tema = Tema::find($_curso->id_tema);
        }
        
        return $this->make("admin.curso.form", array("curso" => $curso, "profesores" => $profesores, "temas" => $temas, "tema" => $tema, "ciudades" => $ciudades));
    }
    
    public function crearCurso(){       
        return $this->mostrarFormCurso(new CursoProfesor());
    }
    
    public function editarCurso($id){        
        $curso = CursoProfesor::find($id);
        if (!sizeof($curso)){
            return $this->redirectTo('/admin/curso/')->with("mensajeError", Lang::get("messages.errorNoEncontrado"));
        }
        
        return $this->mostrarFormCurso($curso);
    }
    
    public function guardarCurso(){               
        
        $id = Input::get("id");
        $curso = CursoProfesor::find($id);
        
        if (!sizeof($curso)){
            $curso = new CursoProfesor();
        }
        
        $curso->fill(Input::all());
        $curso->id_profesor = Input::get("id_profesor");
        
        $f = $_FILES["certificado"];
        $info = null;
        if (is_array($f) && is_uploaded_file($f["tmp_name"]) && !$f['error']){
            $info = pathinfo($f['name']);            
        }
        
        $curso->certificado = $info["extension"];
        
        if ($curso->save()){
            if (is_array($f) && is_uploaded_file($f["tmp_name"]) && !$f['error']){
                $dir = public_path('cursos');

                if(!is_dir($dir)){
                    @mkdir($dir);
                }
                
                $dir .= '/'.$curso->id_profesor;
                 if(!is_dir($dir)){
                    @mkdir($dir);
                }
                
                $nombreArchivo = $curso->id.".".$info["extension"];
                move_uploaded_file($f['tmp_name'], $dir.'/'.$nombreArchivo);
            }
            
            return $this->redirectTo("/admin/curso")->with("mensaje", Lang::get('messages.cursoCreado'));
        }
        else{
            return $this->redirectTo("/admin/curso")->with("mensajeError", Lang::get('messages.errorCursoCreado'));
        }
    }
    
    public function mostrarNoAprobados(){     
        $cursos = CursoProfesor::with(array("curso.tema", "profesor"))->where("estado", "<>", "A")->paginate(50);
        return $this->make('admin.curso.listaAprobar', array("cursos" => $cursos));
    }
    
    public function aprobarCurso($id){        
        $curso = CursoProfesor::find($id);
        if (!sizeof($curso)){
            return $this->redirectTo('/admin/curso/')->with("mensajeError", Lang::get("messages.errorNoEncontrado"));
        }
        
        $curso->estado = "A";
        if ($curso->save()){
            return $this->redirectTo("/admin/curso/pendientes")->with("mensaje", Lang::get('messages.cursoAprobado'));
        }
        else{
            return $this->redirectTo("/admin/curso/pendientes")->with("mensajeError", Lang::get('messages.errorCursoAprobado'));
        }
        return $this->mostrarFormCurso($curso);
    }
    
    public function desaprobarCurso($id){        
        $curso = CursoProfesor::find($id);
        if (!sizeof($curso)){
            return $this->redirectTo('/admin/curso/')->with("mensajeError", Lang::get("messages.errorNoEncontrado"));
        }
        
        $curso->estado = "D";
        if ($curso->save()){
            return $this->redirectTo("/admin/curso/")->with("mensaje", Lang::get('messages.cursoDesprobado'));
        }
        else{
            return $this->redirectTo("/admin/curso/")->with("mensajeError", Lang::get('messages.errorCursoDesaprobado'));
        }
        return $this->mostrarFormCurso($curso);
    }
}