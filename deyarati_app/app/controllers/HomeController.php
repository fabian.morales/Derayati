<?php

class HomeController extends BaseController {

    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |	Route::get('/', 'HomeController@showWelcome');
    |
    */

    public function mostrarIndex(){
        $cursos = CursoProfesor::with(array("curso.tema", "profesor"))->orderBy("created_at", "desc")->take(4)->get();
        $temas = Tema::all();
        $ciudades = Ciudad::with("pais")->get();
        return $this->make('home', array("cursos" => $cursos, "temas" => $temas, "ciudades" => $ciudades, "mensajeBusqueda" => ""));
    }
    
    public function mostrarAcerca(){
        return $this->make('whyus');
    }
    
    public function mostrarContacto(){
        return $this->make('contacto');
    }
    
    public function enviarContacto(){
        $datos = array(
            "email" => Input::get("email"),
            "nombre" => Input::get("nombre"),
            "tema" => Input::get("tema"),
            "notas" => Input::get("notas")
        );
        
        Mail::send(BaseController::$lang.'.emails.contacto', $datos, function($message){
            if (Config::get("app.activarCopiaAdmin") === "Y"){
                $message->bcc(Config::get("app.correoAdmin"), "Administrator");   
            }            
            $message->bcc("desarrollo@encubo.ws", "Desarrollo");
            $message->to(Config::get("app.correoContacto"), "Contact")->subject('Contact request');
        });
        
        return $this->redirectTo("/contacto")->with("mensaje", Lang::get('messages.gracias'));    
    }
    
    public function realizarBusqueda(){
        $idTema = Input::get("id_tema");
        $idCurso = Input::get("id_curso");
        $idCiudad = Input::get("id_ciudad");
        $tipoCurso = Input::get("tipo_curso");
        $idioma = Input::get("idioma");
        
        //$ciudad = Ciudad::find($idCiudad);
        
        $cursos1 = CursoProfesor::with(array("curso.tema", "profesor"));
        $cursos2 = CursoProfesor::with(array("curso.tema", "profesor"));
        
        if ($idTema){
            $cursos1 = $cursos1->whereHas("curso.tema", function($query) use ($idTema) {            
                $query->where("id", $idTema);
            });
            
            $cursos2 = $cursos2->whereHas("curso.tema", function($query) use ($idTema) {            
                $query->where("id", $idTema);
            });
        }
        
        if ($idCurso){
            $cursos1 = $cursos1->where("id_curso", $idCurso);
        }
        
        if ($idCiudad){
            $cursos1 = $cursos1->where("ciudad", $idCiudad);
            $cursos2 = $cursos2->where("ciudad", $idCiudad);
        }
        
        if ($tipoCurso){
            $cursos1 = $cursos1->where("modalidad", $tipoCurso);
            $cursos2 = $cursos2->where("modalidad", $tipoCurso);            
        }
        
        if ($idioma){
            $cursos1 = $cursos1->where(DB::raw("lower(idioma)"), "like", "%".strtolower($idioma)."%");
            //$cursos2 = $cursos2->where(DB::raw("lower(idioma)"), "like", "%".strtolower($idioma)."%");
        }
        
        $cursos = array();
        foreach($cursos1->get() as $c){
            if (!array_key_exists($c->id, $cursos)){
                $cursos[$c->id] = $c;
            }
        }
        
        foreach($cursos2->get() as $c){
            if (!array_key_exists($c->id, $cursos)){
                $cursos[$c->id] = $c;
            }
        }        
        
        $mensaje = "";
        if (!sizeof($cursos)){
            $cursos = CursoProfesor::all();
            $mensaje = Lang::get("messages.noResultados");
        }
        
        return $this->make("curso.listaCursos", array("cursos" => $cursos, "mensajeBusqueda" => $mensaje));            
    }
}
