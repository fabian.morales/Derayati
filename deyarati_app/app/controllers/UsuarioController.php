<?php

class UsuarioController extends AdminController {
    
    var $slug = array("A" => "administradores", "P" => "profesores", "E" => "estudiantes");
    
    public function mostrarIndex(){
        return $this->make('admin.index');
    }

    public function mostrarListaUsuarios($tipo){
        $usuarios = User::where("tipo", $tipo)->paginate(20);
        return $this->make('admin.usuario.index', array("usuarios" => $usuarios, "tipo" => $tipo));
    }
    
    public function mostrarFormUsuario($usuario, $tipo){       
        if (!sizeof($usuario)){
            $usuario = new User();
        }
        
        $paises = Pais::all();
        $ciudades = array();
        
        if ($usuario->pais_residencia){
            $ciudades = Ciudad::where("id_pais", $usuario->pais_residencia)->get();
        }
        
        return $this->make("admin.usuario.form", array("usuario" => $usuario, "tipo" => $tipo, "paises" => $paises));
    }
    
    public function crearUsuario($tipo){       
        return $this->mostrarFormUsuario(new User(), $tipo);
    }
    
    public function editarUsuario($id, $tipo){        
        $usuario = User::find($id);
        if (!sizeof($usuario)){
            return $this->redirectTo('/'.$this->slug[$tipo])->with("mensajeError", Lang::get("messages.errorNoEncontrado"));
        }
        
        return $this->mostrarFormUsuario($usuario, $tipo);
    }
    
    public function guardarUsuario($tipo){        
        $id = Input::get("id");
        $clave = Input::get("password");
        
        $usuario = User::find($id);
        if (!sizeof($usuario)){
            $usuario = new User();
        }
        
        if (empty($id) && empty($clave)){
            Session::flash("mensajeError", Lang::get("messages.errorClaveVacia"));
            return $this->mostrarFormUsuario($usuario, $tipo);
        }
        else if (!empty($clave)){
            $clave = Hash::make($clave);
        }
        else{
            $clave = $usuario->password;
        }
        
        $usuario->fill(Input::all());
        $usuario->password = $clave;
        $usuario->tipo = $tipo;
        
        $cntLogin = User::where("login", $usuario->login)->where("id", "!=", $usuario->id)->count();
        if ($cntLogin > 0){
            Session::flash("mensajeError", Lang::get("messages.errorUsernameExiste"));
            return $this->mostrarFormUsuario($usuario, $tipo);
        }
        
        $cntEmail = User::where("email", $usuario->email)->count();
        if ($cntEmail > 0){
            Session::flash("mensajeError", Lang::get("messages.errorEmailUsado"));            
            return $this->mostrarFormUsuario($usuario, $tipo);
        }
        
        if ($usuario->save()){
            return $this->redirectTo('/'.$this->slug[$tipo])->with("mensaje", Lang::get("messages.registroGuardado"));
        }
        else{
            return $this->redirectTo('/'.$this->slug[$tipo])->with("mensajeError", Lang::get("messages.errorRegistroGuardado"));
        }
    }
    
    public function mostrarListaAdmin(){
        return $this->mostrarListaUsuarios("A");
    }
    
    public function crearAdmin(){
        return $this->crearUsuario("A");
    }
    
    public function editarAdmin($id){
        return $this->editarUsuario($id, "A");
    }
    
    public function guardarAdmin(){
        return $this->guardarUsuario("A");
    }
    
    public function mostrarListaProfesor(){
        return $this->mostrarListaUsuarios("P");
    }
    
    public function crearProfesor(){
        return $this->crearUsuario("P");
    }
    
    public function editarProfesor($id){
        return $this->editarUsuario($id, "P");
    }
    
    public function guardarProfesor(){
        return $this->guardarUsuario("P");
    }
    
    public function mostrarListaEstudiante(){
        return $this->mostrarListaUsuarios("E");
    }
    
    public function crearEstudiante(){
        return $this->crearUsuario("E");
    }
    
    public function editarEstudiante($id){
        return $this->editarUsuario($id, "E");
    }
    
    public function guardarEstudiante(){
        return $this->guardarUsuario("E");
    }    
}