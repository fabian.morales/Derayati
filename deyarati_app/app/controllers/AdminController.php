<?php
class AdminController extends BaseController {
    private $validarPermiso = false;
    
    function __construct() {
        $this->beforeFilter(function() {
            BaseController::$lang = Route::current()->getPrefix();
            App::setLocale(Route::current()->getPrefix());
            
            if (!Auth::check()){
                return $this->redirectTo("/sesion/formLogin")->with("mensajeError", Lang::get("messages.errorNoAutorizado"));
            }
            
            $usuario = Auth::user();
            if ($usuario->tipo != "A"){
                return $this->redirectTo("/")->with("mensajeError", Lang::get("messages.errorNoAutorizado"));
            }            
            
            /*list($clase, $accion) = explode("@", Route::currentRouteAction());
            
            $controlador = Controlador::where("nombre_clase", $clase)->first();
            if (sizeof($controlador) && $controlador->validar_permiso == "Y"){
                $usuario = Auth::user();
                
                $c_permiso = $usuario->controladores()->where("nombre_clase", $clase)->count();
                if ($usuario->admin != "Y" && $c_permiso == 0){
                    return Redirect::to("/")->with("mensajeError", "No tiene permisos para ingresar a esta secci&oacute;n");
                }                
            }*/
        });
    }
}