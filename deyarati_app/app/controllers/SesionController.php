<?php

class SesionController extends BaseController {

    public function mostrarIndex(){
        $paises = Pais::orderBy("nombre_en")->get();
        return $this->make('sesion.formRegistro', array("paises" => $paises));
    }
       
    public function hacerLogin(){
        $login = Input::get("_login");
        $clave = Input::get("_password");
        if (Auth::attempt(array('login' => $login, 'password' => $clave))){
            return $this->redirectTo("/")->with("mensaje", Lang::get('messages.welcome', array("nombre" => Auth::user()->nombre)));
        }
        else{            
            return $this->redirectTo("/sesion/formLogin")->with("mensajeError", Lang::get('messages.errorLogin'));
        }
    }
    
    public function hacerLogout(){
        Auth::logout();
        return $this->redirectTo("/sesion/formLogin");
    }
    
    public function guardarUsuario(){        
        $clave = Input::get("password");        
        $usuario = new User();
        
        if (empty($clave)){
            return $this->redirectTo("/sesion/formLogin")->with("mensajeError", Lang::get('messages.errorClaveVacia'));
        }
        
        $usuario->fill(Input::all());
        $usuario->password = Hash::make($clave);
        
        $cntLogin = User::where("login", $usuario->login)->count();
        if ($cntLogin > 0){
            return $this->redirectTo("/sesion/formLogin")->with("mensajeError", Lang::get('messages.errorUsernameExiste'));
        }
        
        $cntEmail = User::where("email", $usuario->email)->count();
        if ($cntEmail > 0){
            return $this->redirectTo("/sesion/formLogin")->with("mensajeError", Lang::get('messages.errorEmailUsado'));
        }
        
        if ($usuario->save()){
            Mail::send(BaseController::$lang.'.emails.usuarioNuevo', array("usuario" => $usuario, "clave" => Input::get("password")), function($message) use ($usuario){
                if (Config::get("app.activarCopiaAdmin") === "Y"){
                    $message->bcc(Config::get("app.correoAdmin"), "Administrator");   
                }
                $message->bcc("desarrollo@encubo.ws", "Desarrollo");
                $message->to($usuario->email, $usuario->nombre." ".$usuario->apellido)->subject(Lang::get("messages.asuntoMailUsuarioNuevo"));
            });
            
            return $this->redirectTo("/sesion/formLogin")->with("mensaje", Lang::get('messages.usuarioCreado'));
        }
        else{
            return $this->redirectTo("/sesion/formLogin")->with("mensajeError", Lang::get('messages.errorUsuarioCreado'));
        }
    }
    
    public function mostrarPerfil(){
        if (!Auth::check()){
            return $this->redirectTo("/sesion/formLogin")->with("mensajeError", Lang::get('messages.errorSesionNoIniciada'));
        }
        
        $paises = Pais::orderBy("nombre_en")->get();
        $inscripciones = array();
        if (Auth::user()->tipo == "E"){
            $inscripciones = CursoAlumno::where("id_alumno", Auth::user()->id)->with(array("curso.profesor", "curso.curso.tema"))->get();
        }
        
        return $this->make('sesion.perfil', array("usuario" => Auth::user(), "paises" => $paises, "inscripciones" => $inscripciones));
    }
    
    public function actualizarPerfil(){
        if (!Auth::check()){
            return $this->redirectTo("/sesion/formLogin")->with("mensajeError", Lang::get('messages.errorSesionNoIniciada'));
        }
        
        $clave = Input::get("password");        
        $usuario = Auth::user();        
        if (!empty($clave)){
            $clave = Hash::make($clave);
        }
        else{
            $clave = $usuario->password;
        }
        
        $usuario->fill(Input::all());
        $usuario->password = $clave;
        
        $cntEmail = User::where("email", $usuario->email)->where("id", "!=", $usuario->id)->count();
        if ($cntEmail > 0){
            return $this->redirectTo("/sesion/formLogin")->with("mensajeError", Lang::get('messages.errorEmailUsado'));
        }
        
        $f = $_FILES["avatar"];
        $info = null;
        if (is_array($f) && is_uploaded_file($f["tmp_name"]) && !$f['error']){
            $info = pathinfo($f['name']);            
        }   
        
        if ($usuario->save()){
            if (is_array($f) && is_uploaded_file($f["tmp_name"]) && !$f['error']){
                $dir = public_path('avatares');

                if(!is_dir($dir)){
                    @mkdir($dir);
                }
                
                $nombreArchivo = $usuario->id.".jpg";
                move_uploaded_file($f['tmp_name'], $dir.'/'.$nombreArchivo);
            }            
            return $this->redirectTo("/sesion/perfil")->with("mensaje", Lang::get('messages.usuarioActualizado'));
        }
        else{
            return $this->redirectTo("/sesion/perfil")->with("mensajeError", Lang::get('messages.errorUsuarioActualizado'));
        }
    }
    
    public function cambiarClave(){
        if (!Auth::check()){
            return $this->redirectTo("/sesion/formLogin")->with("mensajeError", Lang::get('messages.errorSesionNoIniciada'));
        }
        
        $clave = Input::get("_password");        
        $usuario = Auth::user();        
        if (!empty($clave)){
            $clave = Hash::make($clave);
        }
        
        $usuario->password = $clave;        
        
        if ($usuario->save()){
            return $this->redirectTo("/sesion/perfil")->with("mensaje", Lang::get('messages.claveCambiada'));
        }
        else{
            return $this->redirectTo("/sesion/perfil")->with("mensajeError", Lang::get('messages.errorClaveCambiada'));
        }
    }
    
    public function adicionarIdioma(){
        if (!Auth::check()){
            return $this->redirectTo("/sesion/formLogin")->with("mensajeError", Lang::get('messages.errorSesionNoIniciada'));
        }
        
        if (Auth::user()->tipo == "E"){
            return $this->redirectTo("/sesion/perfil")->with("mensajeError", Lang::get('messages.errorNoAutorizado'));
        }
        
        $idiomaEn = Input::get("idioma_en");
        $idProfesor = Auth::user()->id; //Input::get("id_profesor");
        
        $idioma = Idioma::where("id_profesor", $idProfesor)->where("idioma_en", $idiomaEn)->first();
        
        if (!sizeof($idioma)){
            $idioma = new Idioma();
            $idioma->fill(Input::all());
            $idioma->save();
        }
        
        return $this->redirectTo("/sesion/perfil");
    }
    
    public function quitarIdioma($id){
        if (!Auth::check()){
            return $this->redirectTo("/sesion/formLogin")->with("mensajeError", Lang::get('messages.errorSesionNoIniciada'));
        }
        
        if (Auth::user()->tipo == "E"){
            return $this->redirectTo("/sesion/perfil")->with("mensajeError", Lang::get('messages.errorNoAutorizado'));
        }        
        
        $idioma = Idioma::find($id);
        
        if ($idioma->id_profesor != Auth::user()->id){
            return $this->redirectTo("/sesion/formLogin")->with("mensajeError", Lang::get('messages.errorUsuarioNoCorresp'));
        }
        
        if (sizeof($idioma)){
            $idioma->delete();
        }
        
        return $this->redirectTo("/sesion/perfil");
    }
    
    public function adicionarCertificado(){
        if (!Auth::check()){
            return $this->redirectTo("/sesion/formLogin")->with("mensajeError", Lang::get('messages.errorSesionNoIniciada'));
        }
        
        if (Auth::user()->tipo == "E"){
            return $this->redirectTo("/sesion/perfil")->with("mensajeError", Lang::get('messages.errorNoAutorizado'));
        }
        
        /*if (empty(Input::get("certificado"))){
            return $this->redirectTo("/sesion/perfil")->with("mensajeError", Lang::get('messages.errorNombreCertificado'));
        }*/
                
        $usuario = Auth::user();
        
        $f = $_FILES["archivo_cert"];
        $info = null;
        if (is_array($f) && is_uploaded_file($f["tmp_name"]) && !$f['error']){
            $info = pathinfo($f['name']);            
        }
        
        $certificado = new Certificado();
        $certificado->id_profesor = $usuario->id;
        $certificado->descripcion = Input::get("certificado");
        $certificado->extension = $info["extension"];
        
        if ($certificado->save()){
            if (is_array($f) && is_uploaded_file($f["tmp_name"]) && !$f['error']){
                $dir = public_path('certificados');

                if(!is_dir($dir)){
                    @mkdir($dir);
                }
                
                $dir .= '/'.$usuario->id;
                 if(!is_dir($dir)){
                    @mkdir($dir);
                }
                
                $nombreArchivo = $certificado->id.".".$info["extension"];
                move_uploaded_file($f['tmp_name'], $dir.'/'.$nombreArchivo);
            }
            return $this->redirectTo("/sesion/perfil");
        }
    }
    
    public function verCertificado($id){
        if (!Auth::check()){
            return $this->redirectTo("/sesion/formLogin")->with("mensajeError", Lang::get('messages.errorSesionNoIniciada'));
        }
        
        if (Auth::user()->tipo == "E"){
            return $this->redirectTo("/sesion/perfil")->with("mensajeError", Lang::get('messages.errorNoAutorizado'));
        }        
        
        $certificado = Certificado::find($id);
        
        if ($certificado->id_profesor != Auth::user()->id){
            return $this->redirectTo("/sesion/formLogin")->with("mensajeError", Lang::get('messages.errorUsuarioNoCorresp'));
        }
        
        if (!sizeof($certificado)){
            return $this->redirectTo("/sesion/formLogin")->with("mensajeError", Lang::get('messages.errorNoEncontrado'));
        }
        
        $dir = public_path('certificados/'.Auth::user()->id);
        $nombreArchivo = $certificado->id.".".$certificado->extension;        
        return Response::download($dir.'/'.$nombreArchivo, urlencode($certificado->descripcion.".".$certificado->extension));
    }
    
    public function verCursos(){
        if (!Auth::check()){
            return $this->redirectTo("/sesion/formLogin")->with("mensajeError", Lang::get('messages.errorSesionNoIniciada'));
        }
        
        if (Auth::user()->tipo == "E"){
            return $this->redirectTo("/sesion/perfil")->with("mensajeError", Lang::get('messages.errorNoAutorizado'));
        }
        
        $cursos = Auth::user()->cursos()->get();
        return $this->make('sesion.cursos', array("usuario" => Auth::user(), "cursos" => $cursos));
    }
    
    public function crearCurso(){
        if (!Auth::check()){
            return $this->redirectTo("/sesion/formLogin")->with("mensajeError", Lang::get('messages.errorSesionNoIniciada'));
        }
        
        if (Auth::user()->tipo == "E"){
            return $this->redirectTo("/sesion/perfil")->with("mensajeError", Lang::get('messages.errorNoAutorizado'));
        }
        
        return $this->mostrarFormCurso(new CursoProfesor());
    }
    
    public function editarCurso($id){
        if (!Auth::check()){
            return $this->redirectTo("/sesion/formLogin")->with("mensajeError", Lang::get('messages.errorSesionNoIniciada'));
        }
        
        if (Auth::user()->tipo == "E"){
            return $this->redirectTo("/sesion/perfil")->with("mensajeError", Lang::get('messages.errorNoAutorizado'));
        }
        
        $curso = CursoProfesor::find($id);
        if (!sizeof($curso)){
            return $this->redirectTo("/sesion/perfil")->with("mensajeError", Lang::get('messages.errorNoEncontrado'));
        }
        
        if ($curso->id_profesor != Auth::user()->id){
            return $this->redirectTo("/sesion/formLogin")->with("mensajeError", Lang::get('messages.errorUsuarioNoCorresp'));
        }
        
        return $this->mostrarFormCurso($curso);
    }
    
    public function mostrarFormCurso($curso){
        $ciudades = Ciudad::with("pais")->get();
        $temas = Tema::all();
        $tema = array();        
        if ($curso->id_curso){
            $_curso = Curso::find($curso->id_curso);
            $tema = Tema::find($_curso->id_tema);
        }
        return $this->make('sesion.formCurso', array("curso" => $curso, "temas" => $temas, "tema" => $tema, "ciudades" => $ciudades));
    }
    
    public function mostrarCursosProfesor(){
        if (!Auth::check()){
            return $this->redirectTo("/sesion/formLogin")->with("mensajeError", Lang::get('messages.errorSesionNoIniciada'));
        }
        
        if (Auth::user()->tipo == "E"){
            return $this->redirectTo("/sesion/perfil")->with("mensajeError", Lang::get('messages.errorNoAutorizado'));
        }
        
        $cursos = CursoProfesor::with("curso.tema")->where("id_profesor", Auth::user()->id)->get();
        return $this->make('sesion.cursosp', array("cursos" => $cursos));
    }
    
    public function guardarCurso(){
        if (!Auth::check()){
            return $this->redirectTo("/sesion/formLogin")->with("mensajeError", Lang::get('messages.errorSesionNoIniciada'));
        }
        
        if (Auth::user()->tipo == "E"){
            return $this->redirectTo("/sesion/perfil")->with("mensajeError", Lang::get('messages.errorNoAutorizado'));
        }
        
        $id = Input::get("id");
        $curso = CursoProfesor::find($id);
        
        if (!sizeof($curso)){
            $curso = new CursoProfesor();
        }
        
        $curso->fill(Input::all());
        $curso->id_profesor = Auth::user()->id;
        
        $f = $_FILES["certificado"];
        $info = null;
        if (is_array($f) && is_uploaded_file($f["tmp_name"]) && !$f['error']){
            $info = pathinfo($f['name']);            
        }
        
        $curso->certificado = $info["extension"];
        
        if ($curso->save()){
            if (is_array($f) && is_uploaded_file($f["tmp_name"]) && !$f['error']){
                $dir = public_path('cursos');

                if(!is_dir($dir)){
                    @mkdir($dir);
                }
                
                $dir .= '/'.$curso->id_profesor;
                 if(!is_dir($dir)){
                    @mkdir($dir);
                }
                
                $nombreArchivo = $curso->id.".".$info["extension"];
                move_uploaded_file($f['tmp_name'], $dir.'/'.$nombreArchivo);
            }
            
            $curso = CursoProfesor::where("id", $curso->id)->with(array("curso.tema", "profesor", "ciudadObj"))->first();
            $datos = array("curso" => $curso);
            
            //Mail al adinistrador
            Mail::send(BaseController::$lang.'.emails.curso.creacionAdmin', $datos, function($message){
                if (Config::get("app.activarCopiaAdmin") === "Y"){
                    $message->bcc(Config::get("app.correoAdmin"), "Administrator");   
                }
                $message->bcc("desarrollo@encubo.ws", "Desarrollo");
                $message->to("desarrollo@encubo.ws", "Administrator")->subject(Lang::get("messages.asuntoMailCursoNuevo"));
            });
            
            return $this->redirectTo("/sesion/cursosp")->with("mensaje", Lang::get('messages.cursoCreado'));
        }
        else{
            return $this->redirectTo("/sesion/cursosp")->with("mensajeError", Lang::get('messages.errorCursoCreado'));
        }
    }
    
    public function requestReset(){
        return$this->make('sesion.requestReset');
    }
    
    public function envioReset(){
        $credentials = array('email' => Input::get('email'));
        Password::remind($credentials, function($message){
            $message->bcc("desarrollo@encubo.ws", "Desarrollo");
            $message->subject(Lang::get("messages.asuntoMailPasswordRecovery")); 
        });
        
        return $this->make('sesion.resetEnviado');
    }
    
    public function passwordReset($token){
        return $this->make('sesion.passwordReset', array("token" => $token));
    }
    
    public function guardarReset($token){
        $usuario = User::where("email", Input::get('email'))->first();
        $ret = Password::INVALID_USER;
        if (sizeof($usuario)){
            $credentials = array('login' => $usuario->login, 
                             'password' => Input::get("password"), 
                             'password_confirmation' => Input::get("password_confirmation"),
                             'token' => $token);

            $ret = Password::reset($credentials, function($user, $password)
            {
                $user->password = Hash::make($password);
                $user->save();            
            }); 
        }

        switch ($ret)
        {
            case Password::INVALID_PASSWORD:
                return $this->redirectTo('/')->with('mensajeError', Lang::get('messages.errorClaveNoValida'));
            case Password::INVALID_TOKEN:
                return $this->redirectTo('/')->with('mensajeError', Lang::get('messages.errorClaveToken'));
            case Password::INVALID_USER:
                return $this->redirectTo('/')->with('mensajeError', Lang::get('messages.errorClaveUsuario'));
            case Password::PASSWORD_RESET:
                return $this->redirectTo('/')->with('mensaje', Lang::get('messages.claveCambiada'));
        }
    }
}